<?php
header("Content-type: text/html; charset=utf-8");
require("../class/config.php");

if(isset($_POST) && !empty($_POST)){

    //Limpa os posts recebidos
    $CodAssinatura = strip_tags(trim(addslashes($_POST['data']['subscription_id'])));
    $CodId = strip_tags(trim(addslashes($_POST['data']['id'])));
    $Status = strip_tags(trim(addslashes($_POST['data']['status'])));

    $ReadCompras = new Read();
    $ReadPlanos = new Read();

    //Mudança de status da fatura
    if($_POST['event'] == "invoice.status_changed" || $_POST['event'] == "invoice.refund"){

        $ReadCompras->ExeRead("compras", "WHERE cod_fatura = :cod_fatura", "cod_fatura={$CodId}");
        if($ReadCompras->GetResult()){

            //Atualiza o status
            $UpdateStatus = new Update();
            $UpdateStatus->ExeUpdate("compras", array("status_pgto" => $Status), "WHERE cod_fatura = :cod_fatura", "cod_fatura={$CodId}");
            
            //Verifica se o status é o PAID = Aprovado
            if($Status == "paid"){

                //Atualiza o status assinatura
                $UpdateStatus = new Update();
                $UpdateStatus->ExeUpdate("compras", array("status_ass" => "2"), "WHERE cod_fatura = :cod_fatura", "cod_fatura={$CodId}");
                
                //Pega informações do cliente
                $ReadCliente = new Read();
                $ReadCliente->ExeRead("clientes", "WHERE cod_cliente = :cod_cliente", "cod_cliente={$ReadCompras->GetResult()[0]['cod_cliente']}");

                //Pega informações do sistema
                $ReadSistema = new Read();
                $ReadSistema->ExeRead("sistema");

                //Atualiza etapa da otimização 
                $Alterar = false;
                switch ($ReadCompras->GetResult()[0]['plano']) {
                    case 'otimizar_seo'   :
                    case 'otimizar_combo' :
                        $Alterar = true;
                        break;
                    
                    default:
                        $Alterar = false;
                        break;
                }

                if ($Alterar) {

                    $readSeo = new Read();
                    $readSeo->ExeRead("etapas", "WHERE cod_compra = :cod", "cod={$ReadCompras->GetResult()[0]['id']}");

                    if ($readSeo->GetResult()) {

                        if ($readSeo->GetResult()[0]['seo'] < 2) {

                            $updtSeo = new Update();
                            $updtSeo->ExeUpdate("etapas", array("seo" => 2),"WHERE cod_compra =:cod","cod={$ReadCompras->GetResult()[0]['id']}");
                            
                        }

                    }else{

                        $createSeo = new Create();
                        $createSeo->ExeCreate("etapas", 
                            array(
                                "cod_compra"    =>  $ReadCompras->GetResult()[0]['id'],
                                "seo"           =>  2
                            )
                        );

                    }
                }

                //Verifica se preencheu nome completo, se sim pega o primeiro nome
                $PrimeiroNome = explode(" ", $ReadCliente->GetResult()[0]['nome']);
                if($PrimeiroNome[0]){
                    $PrimeiroNome = $PrimeiroNome[0];
                }else{
                    $PrimeiroNome = $ReadCliente->GetResult()[0]['nome'];
                }

                //Envia e-mail de pagamento realizado com link para o preenchimento do briefing
                $MsgEmail = file_get_contents(__DIR__."/../Emails/pagamento_realizado.html");
                $MsgEmail = str_replace ('%NomeUsuario', $PrimeiroNome, $MsgEmail);
                $MsgEmail = str_replace ('%idenAss', Valida::Base3($CodAssinatura), $MsgEmail);
                Valida::EnviarEmail("Finalizar cadastro", $MsgEmail, $ReadSistema->GetResult()[0]['email_resposta'], NOME_PROJETO, $ReadCliente->GetResult()[0]['email'], $ReadCliente->GetResult()[0]['nome']);
            
            }
        }

    }

    //Assinatura renovada - Passo 1 - Criar fatura
    if($_POST['event'] == "invoice.created" && $CodAssinatura <> ""){

        //Instancia class de registro
        $InsereCompra = new Create();

        $ReadCompras->ExeRead("compras", "WHERE cod_assinatura = :cod_assinatura AND cod_fatura <> :cod_fatura ORDER BY id DESC", "cod_assinatura={$CodAssinatura}&cod_fatura={$CodId}");
        if($ReadCompras->GetResult() && $ReadCompras->GetResult()[0]['status_pgto'] == "paid"){ //Renovação
            //Verificar registro para não ocorrer duplicação
            $Read = new Read();
            $Read->ExeRead("compras", "WHERE cod_assinatura = :cod_assinatura AND cod_cliente = :cod_cliente AND cod_fatura = :cod_fatura", "cod_assinatura={$CodAssinatura}&cod_cliente={$ReadCompras->GetResult()[0]['cod_cliente']}&cod_fatura={$CodId}");

            if (!$Read->GetResult()) {
                $ReadPlanos->ExeRead("planos", "WHERE identificador = :identificador", "identificador={$ReadCompras->GetResult()[0]['plano']}");
                $ValorAssinatura = ($ReadPlanos->GetResult()) ? $ReadPlanos->GetResult()[0]['assinatura'] : $ReadCompras->GetResult()[0]['valor'] ;
                $array_data = array(
                    "cod_assinatura" => $CodAssinatura, 
                    "cod_cliente" => $ReadCompras->GetResult()[0]['cod_cliente'], 
                    "cod_fatura" => $CodId, 
                    "plano" => $ReadCompras->GetResult()[0]['plano'], 
                    "metodo" => $ReadCompras->GetResult()[0]['metodo'], 
                    "valor" => $ValorAssinatura, 
                    "renovacao" => "2", 
                    "status_pgto" => $Status, 
                    "status_ass" => $ReadCompras->GetResult()[0]['status_ass'], 
                    "revendedor" => $ReadCompras->GetResult()[0]['revendedor'], 
                    "cod_vendedor" => 0,
                    "origem_criacao" => "gatilho iugu", 
                    "dias_adicionados" => 1, 
                    "data" => date("Y-m-d H:i:s")
                );
    
                //Insere nova compra
                $InsereCompra->ExeCreate("compras", $array_data);
                
                //Cadastra informações no banco de dados SOUNET através do Gatilho
                $array_data['id_compra'] = $InsereCompra->GetResult();
                $array_data['type_action'] = COMPRAS;
                $array_data['origem_compra'] = ORIGEM_COMPRA;
                $Gatilho = new Gatilho();
                $Gatilho->Request('post', $array_data);
    
                //Cadastra informações do cliente no banco de dados SOUNET através do gatilho
                $Cliente = new Read();
                $Cliente->ExeRead("clientes", "WHERE cod_cliente = :cod_cliente", "cod_cliente={$ReadCompras->GetResult()[0]['cod_cliente']}");
                
                if ($Cliente->GetResult()) {
                    $array_data = $Cliente->GetResult()[0];
    
                    Iugu::setApiKey(TOKEN_IUGU_SOUNET);
                    try {
                        $Cliente_Iugu = Iugu_Customer::fetch($ReadCompras->GetResult()[0]['cod_cliente']);
                        $array_data['iugu'] = 1;
                    } catch (Exception $e) {
                        $array_data['iugu'] = 0;
                    }
                                        
                    $array_data['type_action'] = CLIENTES;
                    $array_data['origem_cliente'] = ORIGEM_COMPRA;
                    $array_data['origem_criacao'] = 'gatilho iugu';
                    unset($array_data['id']);
                    $Gatilho->Request('post', $array_data);
                }
            }

        }elseif($ReadCompras->GetResult() && ($ReadCompras->GetResult()[0]['status_pgto'] == "canceled" || $ReadCompras->GetResult()[0]['status_pgto'] == "expired")){ //Segunda via
            //Verificar registro para não ocorrer duplicação
            $Read = new Read();
            $Read->ExeRead("compras", "WHERE cod_assinatura = :cod_assinatura AND cod_cliente = :cod_cliente AND cod_fatura = :cod_fatura", "cod_assinatura={$CodAssinatura}&cod_cliente={$ReadCompras->GetResult()[0]['cod_cliente']}&cod_fatura={$CodId}");

            if (!$Read->GetResult()) {
                $ReadPlanos->ExeRead("planos", "WHERE identificador = :identificador", "identificador={$ReadCompras->GetResult()[0]['plano']}");
                $ValorAssinatura = ($ReadPlanos->GetResult()) ? $ReadPlanos->GetResult()[0]['assinatura'] : $ReadCompras->GetResult()[0]['valor'] ;
                $array_data = array(
                    "cod_assinatura" => $CodAssinatura, 
                    "cod_cliente" => $ReadCompras->GetResult()[0]['cod_cliente'], 
                    "cod_fatura" => $CodId, 
                    "plano" => $ReadCompras->GetResult()[0]['plano'], 
                    "metodo" => $ReadCompras->GetResult()[0]['metodo'], 
                    "valor" => $ValorAssinatura, 
                    "renovacao" => "1", 
                    "status_pgto" => $Status, 
                    "status_ass" => $ReadCompras->GetResult()[0]['status_ass'], 
                    "revendedor" => $ReadCompras->GetResult()[0]['revendedor'], 
                    "cod_vendedor" => 0,
                    "origem_criacao" => "gatilho iugu", 
                    "dias_adicionados" => 2, 
                    "data" => date("Y-m-d H:i:s")
                );
    
                //Insere nova compra
                $InsereCompra->ExeCreate("compras", $array_data);
    
                //Cadastra informações no banco de dados SOUNET através do Gatilho
                $array_data['id_compra'] = $InsereCompra->GetResult();
                $array_data['type_action'] = COMPRAS;
                $array_data['origem_compra'] = ORIGEM_COMPRA;
                $Gatilho = new Gatilho();
                $Gatilho->Request('post', $array_data);
    
                //Cadastra informações do cliente no banco de dados SOUNET através do gatilho
                $Cliente = new Read();
                $Cliente->ExeRead("clientes", "WHERE cod_cliente = :cod_cliente", "cod_cliente={$ReadCompras->GetResult()[0]['cod_cliente']}");
    
                if ($Cliente->GetResult()) {
                    $array_data = $Cliente->GetResult()[0];
    
                    Iugu::setApiKey(TOKEN_IUGU_SOUNET);
                    try {
                        $Cliente_Iugu = Iugu_Customer::fetch($ReadCompras->GetResult()[0]['cod_cliente']);
                        $array_data['iugu'] = 1;
                    } catch (Exception $e) {
                        $array_data['iugu'] = 0;
                    }
                                        
                    $array_data['type_action'] = CLIENTES;
                    $array_data['origem_cliente'] = ORIGEM_COMPRA;
                    $array_data['origem_criacao'] = 'gatilho iugu';
                    unset($array_data['id']);
                    $Gatilho->Request('post', $array_data);
                }
            }
        }
    }

    //Assinatura renovada - Passo 2 - Alterar dizendo que é uma renovação
    // if($_POST['event'] == "subscription.renewed" && $CodId <> ""){

    //     $ReadCompras->ExeRead("compras", "WHERE cod_assinatura = :cod_assinatura ORDER BY id DESC LIMIT 1", "cod_assinatura={$CodId}");
    //     if($ReadCompras->GetResult()){

    //         //Atualiza a compra
    //         $UpdateStatus = new Update();
    //         $UpdateStatus->ExeUpdate("compras", array("renovacao" => "2"), "WHERE cod_assinatura = :cod_assinatura", "cod_assinatura={$ReadCompras->GetResult()[0]['cod_assinatura']}");
            
    //     }

    // }

    //Assinatura suspensa
    if($_POST['event'] == "subscription.suspended" && $CodId <> ""){

        $ReadCompras->ExeRead("compras", "WHERE cod_assinatura = :cod_assinatura ORDER BY id DESC LIMIT 1", "cod_assinatura={$CodId}");
        if($ReadCompras->GetResult()){

            //Atualiza a compra
            $UpdateStatus = new Update();
            $UpdateStatus->ExeUpdate("compras", array("status_ass" => "1"), "WHERE cod_assinatura = :cod_assinatura", "cod_assinatura={$ReadCompras->GetResult()[0]['cod_assinatura']}");
            
        }

    }

    //Assinatura ativada
    if($_POST['event'] == "subscription.activated" && $CodId <> ""){

        $ReadCompras->ExeRead("compras", "WHERE cod_assinatura = :cod_assinatura ORDER BY id DESC LIMIT 1", "cod_assinatura={$CodId}");
        if($ReadCompras->GetResult()){

            //Atualiza a compra
            $UpdateStatus = new Update();
            $UpdateStatus->ExeUpdate("compras", array("status_ass" => "2"), "WHERE cod_assinatura = :cod_assinatura", "cod_assinatura={$ReadCompras->GetResult()[0]['cod_assinatura']}");
            
        }

    }

}
?>