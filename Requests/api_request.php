<?php

header('Content-type: text/html; charset=utf-8');
require('../class/config.php');

function array_map_recursive($callback, $array) {
    foreach ($array as $key => $value) {
        if (is_array($array[$key])) {
            $array[$key] = array_map_recursive($callback, $array[$key]);
        }
        else {
            $array[$key] = call_user_func($callback, $array[$key]);
        }
    }
    return $array;
}

function respostaGatilho($success = false, $message = 'Ação indisponível!', $dados = array()) {
	echo json_encode(
    	array(
    		'status' => $success,
    		'mensagem' => $message,
    		'dados' => $dados
    	)
    );
    exit();
}

if (isset($_POST) && !empty($_POST)) {
	//Limpa e retira quaisquer meios de invasões
    $dados = array_map_recursive('strip_tags', $_POST);
    $dados = array_map_recursive('trim', $_POST);

    $status = false;
    $mensagem = 'Ação indisponível';
    $data = array();

    //Verifica se todos os campos estão preenchidos e se o type_action veio preenchido
	if ( empty($dados) || ( !isset($dados['type_action']) || empty($dados['type_action']) ) ) {
		$mensagem = 'Dados Incorretos!';

	} else {
		$acao = $dados['type_action'];
		unset($dados['type_action']);

		$Read = new Read;

		if ($acao == 'observacoes') {
			$dados['origem'] = 'Painel Franqueados';
			$dados['data'] = date('Y-m-d H:i:s');
			$Observacao = new Create;
			$Observacao->ExeCreate('observacoes', $dados);
			if ($result = $Observacao->GetResult()) {
				$status = true;
				$mensagem = 'Observação inserida com sucesso!';
				$Read->ExeRead('observacoes', 'WHERE id = :id', "id={$result}");
				$_data = $Read->GetResult();
				$data = $_data[0];
			} else {
				$mensagem = 'Erro ao inserir observação!';
			}

		} elseif ($acao == 'prospeccao') {
			if (isset($dados['cliente']) && isset($dados['pre_venda'])) {
				$cliente = array();
				$dados['cliente']['origem_criacao'] = $dados['pre_venda']['origem_criacao'] = 'painel franqueados';
				$dados['cliente']['iugu_destino'] = 'sounet';
				$ReadVerifica = new Read;
				$ReadVerifica->ExeRead("clientes", "WHERE cpf_cnpj = :CpfCnpj AND email = :email AND iugu_destino = 'sounet'", "CpfCnpj={$dados['cliente']['cpf_cnpj']}&email={$dados['cliente']['email']}");
				if (!$ReadVerifica->GetResult()) {
					$Cliente = new Create;
					$Cliente->ExeCreate('clientes', $dados['cliente']);
					
					if ($id_cliente = $Cliente->GetResult()){
						$Read->ExeRead('clientes', 'WHERE id = :id', "id={$id_cliente}");
						$cliente = $Read->GetResult()[0];
					} else {
						$mensagem = 'Erro ao inserir cliente!';
					}

				} else {
					$cliente = $ReadVerifica->GetResult()[0];
				}

				if (!empty($cliente)) {
					$Prospeccao = new Create;
					$Prospeccao->ExeCreate('prospeccao', $dados['pre_venda']);
					if ($id_prospeccao = $Prospeccao->GetResult()) {
						$Read->ExeRead('prospeccao', 'WHERE id = :id', "id={$id_prospeccao}");
						$prospeccao = $Read->GetResult()[0];
						$status = true;
						$mensagem = 'Prospecção inserida com sucesso!';
						$MsgEmail = file_get_contents(__DIR__."/../Emails/dados_cliente.html");
						$MsgEmail = str_replace ('%NomeUsuario', $cliente['nome'], $MsgEmail);
						$MsgEmail = str_replace ('%idenAss', Valida::criptografia_segura($prospeccao['id']), $MsgEmail);
						$ReadSistema = new Read;
						$ReadSistema->ExeRead('sistema');
						$mensagem = Valida::criptografia_segura($prospeccao['id']);
						//Valida::EnviarEmail("Selecionar Forma de Pagamento - OtimizarAgora", $MsgEmail, $ReadSistema->GetResult()[0]['email_resposta'], NOME_PROJETO, $cliente['email'], $cliente['nome']);
					}
				}
				
			} else {
				$mensagem = 'Parâmetros incorretos!';
			}
		} else {
			$mensagem = 'Ação inexistente!';
		}
    }

    respostaGatilho($status, $mensagem, $data);
}

?>