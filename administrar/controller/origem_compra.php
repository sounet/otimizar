<?php
//Verifica o tipo de usuário logado
if($UserLogado[0] == "revendedores"){

    $TplView->addFile("INCLUDE_PG", "view/conteudo_restrito.html");

}else{

    $TplView->addFile("INCLUDE_PG", "view/origem_compra.html");
    $TplView->page_atual_admin = $url[0];
    $ReadOrigemCompra = new Read();

    $TplView->delType = Valida::GeraAes("delOrigemCompra");

    if($url[1] == "listar"){
        
        $ReadOrigemCompra->ExeRead("origem_compra", "ORDER BY id DESC");
        if($ReadOrigemCompra->GetResult()){
                                
            foreach($ReadOrigemCompra->GetResult() as $res_list){
                $TplView->titulo = $res_list['titulo'];
                $TplView->ident = $res_list['id'];
                $TplView->block("FOREACH");
            }

            $TplView->block("VERIFICA_LISTAGEM");
        }

        $TplView->block("BLOCK_LISTAR");

    }elseif($url[1] == "inserir"){

        $TplView->tpf_form = Valida::GeraAes("cadOrigemCompra");
        $TplView->block("BLOCK_INSERIR");

    }elseif($url[1] == "editar"){

        $TplView->tpf_form = Valida::GeraAes("updOrigemCompra");
        $TplView->idfinfo = $url[2];
        $ReadOrigemCompra->ExeRead("origem_compra", "WHERE id = :id", "id={$url[2]}");
        $TplView->titulo = $ReadOrigemCompra->GetResult()[0]['titulo'];
        $TplView->block("BLOCK_EDITAR");

    }

}
?>