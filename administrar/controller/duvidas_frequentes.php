<?php
//Verifica o tipo de usuário logado
if($UserLogado[0] == "revendedores"){

    $TplView->addFile("INCLUDE_PG", "view/conteudo_restrito.html");

}else{
    
    $TplView->addFile("INCLUDE_PG", "view/duvidas_frequentes.html");
    $TplView->page_atual_admin = $url[0];
    $ReadDuvidas = new Read();

    $TplView->delType = Valida::GeraAes("delDuvidas");

    if($url[1] == "listar"){
        
        $ReadDuvidas->ExeRead("duvidas_frequentes", "ORDER BY id DESC");
        if($ReadDuvidas->GetResult()){
                                
            foreach($ReadDuvidas->GetResult() as $res_list){
                $TplView->pergunta = $res_list['pergunta'];
                $TplView->ident = $res_list['id'];
                $TplView->block("FOREACH");
            }

            $TplView->block("VERIFICA_LISTAGEM");
        }

        $TplView->block("BLOCK_LISTAR");

    }elseif($url[1] == "inserir"){

        $TplView->tpf_form = Valida::GeraAes("cadDuvidas");
        $TplView->block("BLOCK_INSERIR");

    }elseif($url[1] == "editar"){

        $TplView->tpf_form = Valida::GeraAes("updDuvidas");
        $TplView->idfinfo = $url[2];
        $ReadDuvidas->ExeRead("duvidas_frequentes", "WHERE id = :id", "id={$url[2]}");
        $TplView->pergunta = $ReadDuvidas->GetResult()[0]['pergunta'];
        $TplView->resposta = $ReadDuvidas->GetResult()[0]['resposta'];
        $TplView->idfinfo = $ReadDuvidas->GetResult()[0]['id'];
        $TplView->block("BLOCK_EDITAR");

    }

}
?>