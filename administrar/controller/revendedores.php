<?php
//Verifica o tipo de usuário logado
if($UserLogado[0] == "revendedores"){

    $TplView->addFile("INCLUDE_PG", "view/conteudo_restrito.html");

}else{
    
    $TplView->addFile("INCLUDE_PG", "view/revendedores.html");
    $TplView->page_atual_admin = $url[0];
    $ReadRevendedores = new Read();

    if($url[1] == "listar"){
        
        $ReadRevendedores->FullRead("SELECT re.id, re.nome, re.status FROM revendedores re ORDER BY re.id DESC");
        if($ReadRevendedores->GetResult()){
                                
            foreach($ReadRevendedores->GetResult() as $res_list){
                $TplView->NomeRevendedor = $res_list['nome'];
                $TplView->StatusRevendedor = ($res_list['status'] == "2" ? "Ativo" : "Inativo");
                $TplView->ident = $res_list['id'];
                $TplView->block("FOREACH");
            }

            $TplView->block("VERIFICA_LISTAGEM");
        }

        $TplView->block("BLOCK_LISTAR");

    }elseif($url[1] == "inserir"){

        $TplView->tpf_form = Valida::GeraAes("cadRevendedores");
        $TplView->block("BLOCK_INSERIR");

    }elseif($url[1] == "editar"){

        $TplView->tpf_form = Valida::GeraAes("updRevendedores");
        $TplView->idfinfo = $url[2];
        $ReadRevendedores->ExeRead("revendedores", "WHERE id = :id", "id={$url[2]}");
        $TplView->CodigoRevendedor = $ReadRevendedores->GetResult()[0]['cod_revendedor'];
        $TplView->nome = $ReadRevendedores->GetResult()[0]['nome'];
        $TplView->email = $ReadRevendedores->GetResult()[0]['email'];
        $TplView->telefone = $ReadRevendedores->GetResult()[0]['telefone'];
        $TplView->cpf = $ReadRevendedores->GetResult()[0]['cpf'];
        
        $TplView->RevenAtivo = ($ReadRevendedores->GetResult()[0]['status'] == "2" ? "selected='selected'" : "");
        $TplView->RevenInativo = ($ReadRevendedores->GetResult()[0]['status'] == "1" ? "selected='selected'" : "");

        $TplView->LevelVendas = ($ReadRevendedores->GetResult()[0]['level'] == "1" ? "selected='selected'" : "");
        $TplView->LevelPlantao = ($ReadRevendedores->GetResult()[0]['level'] == "2" ? "selected='selected'" : "");

        $TplView->block("BLOCK_EDITAR");

    }

}
?>