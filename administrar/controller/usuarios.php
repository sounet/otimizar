<?php
//Verifica o tipo de usuário logado
if($UserLogado[0] == "revendedores"){

    $TplView->addFile("INCLUDE_PG", "view/conteudo_restrito.html");

}else{
    
    $TplView->addFile("INCLUDE_PG", "view/usuarios.html");
    $TplView->page_atual_admin = $url[0];
    $ReadUsers = new Read();

    $TplView->delType = Valida::GeraAes("delUsers");

    if($url[1] == "listar"){
        
        $ReadUsers->ExeRead("admin", "ORDER BY id DESC");
        if($ReadUsers->GetResult()){
                                
            foreach($ReadUsers->GetResult() as $res_list){
                $TplView->nome = $res_list['nome'];
                $TplView->email = $res_list['email'];
                $TplView->ident = $res_list['id'];
                $TplView->block("FOREACH");
            }

            $TplView->block("VERIFICA_LISTAGEM");
        }

        $TplView->block("BLOCK_LISTAR");

    }elseif($url[1] == "inserir"){

        $TplView->tpf_form = Valida::GeraAes("cadUsers");
        $TplView->block("BLOCK_INSERIR");

    }elseif($url[1] == "editar"){

        $TplView->tpf_form = Valida::GeraAes("updUsers");
        $TplView->idfinfo = $url[2];
        $ReadUsers->ExeRead("admin", "WHERE id = :id", "id={$url[2]}");
        $TplView->nome = $ReadUsers->GetResult()[0]['nome'];
        $TplView->email = $ReadUsers->GetResult()[0]['email'];
        $TplView->ident = $ReadUsers->GetResult()[0]['id'];
        $TplView->block("BLOCK_EDITAR");

    }

}
?>