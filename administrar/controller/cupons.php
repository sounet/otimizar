<?php
//Verifica o tipo de usuário logado
if($UserLogado[0] == "revendedores"){

    $TplView->addFile("INCLUDE_PG", "view/conteudo_restrito.html");

}else{

    $TplView->addFile("INCLUDE_PG", "view/cupons.html");
    $TplView->page_atual_admin = $url[0];
    $ReadCupons = new Read();

    $TplView->delType = Valida::GeraAes("delCupons");

    if($url[1] == "listar"){
        
        $ReadCupons->ExeRead("cupons", "ORDER BY id DESC");
        if($ReadCupons->GetResult()){
                                
            foreach($ReadCupons->GetResult() as $res_list){
                $TplView->codigo = $res_list['codigo'];
                $TplView->porcentagem = $res_list['porcentagem'];
                $TplView->utilizacoes = $res_list['utilizacoes'];
                $TplView->ident = $res_list['id'];
                $TplView->block("FOREACH");
            }

            $TplView->block("VERIFICA_LISTAGEM");
        }

        $TplView->block("BLOCK_LISTAR");

    }elseif($url[1] == "inserir"){

        $TplView->tpf_form = Valida::GeraAes("cadCupons");
        $TplView->block("BLOCK_INSERIR");

    }elseif($url[1] == "editar"){

        $TplView->tpf_form = Valida::GeraAes("updCupons");
        $TplView->idfinfo = $url[2];
        $ReadCupons->ExeRead("cupons", "WHERE id = :id", "id={$url[2]}");
        $TplView->codigo = $ReadCupons->GetResult()[0]['codigo'];
        $TplView->porcentagem = $ReadCupons->GetResult()[0]['porcentagem'];
        $TplView->block("BLOCK_EDITAR");

    }

}
?>