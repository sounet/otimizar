<?php
//Verifica o tipo de usuário logado
if($UserLogado[0] == "revendedores"){

    $TplView->addFile("INCLUDE_PG", "view/conteudo_restrito.html");

}else{

    $TplView->addFile("INCLUDE_PG", "view/plano_mais_vendido.html");

    $data_de = "";
    $data_de_query = "";
    $data_ate = "";
    $data_ate_query = "";

    if(isset($_POST) && !empty($_POST)){

        if($_POST['data_de']){
            $convert_data_de = Valida::DataForSql($_POST['data_de']);
            $data_de = "AND DATE(co.data) >= :data_de";
            $data_de_query = "&data_de={$convert_data_de}";
        }

        if($_POST['data_ate']){
            $convert_data_ate = Valida::DataForSql($_POST['data_ate']);
            $data_ate = "AND DATE(co.data) <= :data_ate";
            $data_ate_query = "&data_ate={$convert_data_ate}";
        }

        $TplView->InformacoesFiltradas = "Informações de: <b>{$_POST['data_de']}</b> até <b>{$_POST['data_ate']}</b>";

    }else{

        $TplView->InformacoesFiltradas = "Informações de: <b>20/11/2017</b> até <b>".date("d/m/Y")."</b>";

    }

    $ReadMensal = new Read();
    $ReadTrimestral = new Read();
    $ReadSemestral = new Read();
    $ReadAnual = new Read();

    $ReadMensal->FullRead("SELECT COUNT(*) AS nvendas, SUM(co.valor) AS valor_vendas FROM compras co WHERE co.status_pgto = :status_pgto AND plano = :plano {$data_de} {$data_ate}", "status_pgto=paid&plano=mensal_plan{$data_de_query}{$data_ate_query}");
    $ReadTrimestral->FullRead("SELECT COUNT(*) AS nvendas, SUM(co.valor) AS valor_vendas FROM compras co WHERE co.status_pgto = :status_pgto AND plano = :plano {$data_de} {$data_ate}", "status_pgto=paid&plano=trimestral_plan{$data_de_query}{$data_ate_query}");
    $ReadSemestral->FullRead("SELECT COUNT(*) AS nvendas, SUM(co.valor) AS valor_vendas FROM compras co WHERE co.status_pgto = :status_pgto AND plano = :plano {$data_de} {$data_ate}", "status_pgto=paid&plano=semestral_plan{$data_de_query}{$data_ate_query}");
    $ReadAnual->FullRead("SELECT COUNT(*) AS nvendas, SUM(co.valor) AS valor_vendas FROM compras co WHERE co.status_pgto = :status_pgto AND plano = :plano {$data_de} {$data_ate}", "status_pgto=paid&plano=anual_plan{$data_de_query}{$data_ate_query}");

    $TplView->NVendasMensal = $ReadMensal->GetResult()[0]['nvendas'];
    $TplView->NVendasTrimestral = $ReadTrimestral->GetResult()[0]['nvendas'];
    $TplView->NVendasSemestral = $ReadSemestral->GetResult()[0]['nvendas'];
    $TplView->NVendasAnual = $ReadAnual->GetResult()[0]['nvendas'];

    $TplView->SomaMensal = number_format($ReadMensal->GetResult()[0]['valor_vendas'], 2, ',', '.');
    $TplView->SomaTrimestral = number_format($ReadTrimestral->GetResult()[0]['valor_vendas'], 2, ',', '.');
    $TplView->SomaSemestral = number_format($ReadSemestral->GetResult()[0]['valor_vendas'], 2, ',', '.');
    $TplView->SomaAnual = number_format($ReadAnual->GetResult()[0]['valor_vendas'], 2, ',', '.');

    $TplView->block("TAXA_RENOVACAO");

}
?>