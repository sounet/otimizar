<?php
//Verifica o tipo de usuário logado
if($UserLogado[0] == "revendedores"){

    $TplView->addFile("INCLUDE_PG", "view/conteudo_restrito.html");

}else{

    $TplView->addFile("INCLUDE_PG", "view/nao_finalizadas.html");
    $Read = new Read();
        
    $Read->FullRead("SELECT cl.id, cl.nome, cl.email, cl.celular, cl.cidade, cl.estado, cl.origem, cl.data FROM clientes cl LEFT JOIN compras co ON cl.cod_cliente = co.cod_cliente WHERE co.cod_cliente IS NULL GROUP BY cl.id ORDER BY cl.data DESC");
    if($Read->GetResult()){

        //Listando clientes que não finalizaram as compras
        foreach($Read->GetResult() as $res){
            $TplView->nome = $res['nome'];
            $TplView->email = $res['email'];
            $TplView->celular = $res['celular'];
            $TplView->cidade_uf = $res['cidade']." / ".$res['estado'];
            $TplView->tentativa = date("d/m/Y H:i", strtotime($res['data']));

            $TplView->block("FOREACH_NAOFINALIZARAM");
        }
        $TplView->block("NAO_FINALIZARAM");

    }

}
?>