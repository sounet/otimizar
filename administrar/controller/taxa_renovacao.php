<?php
//Verifica o tipo de usuário logado
if($UserLogado[0] == "revendedores"){

    $TplView->addFile("INCLUDE_PG", "view/conteudo_restrito.html");

}else{

    $TplView->addFile("INCLUDE_PG", "view/taxa_renovacao.html");

    $TplView->SomaVendasPeriodo = "0";
    $TplView->SomaRenovacoesPeriodo = "0";
    $TplView->NomeMesAnterior = "00/0000";
    $TplView->SomaVendasMesAnterior = "0";
    $TplView->SomaTaxaRenovacao = "0";
    $TplView->InformacoesFiltradas = "";
    $TplView->ValorVendasPeriodo = "0.00";
    $TplView->ValorRenovacoesPeriodo = "0.00";
    $TplView->ValorVendasMesAnterior = "0.00";

    if(isset($_POST) && !empty($_POST)){

        if($_POST['data_de']){
            $convert_data_de = Valida::DataForSql($_POST['data_de']);
            $data_de = "AND DATE(co.data) >= :data_de";
            $data_de_query = "&data_de={$convert_data_de}";
        }else{
            $data_de = "";
            $data_de_query = "";
        }

        if($_POST['data_ate']){
            $convert_data_ate = Valida::DataForSql($_POST['data_ate']);
            $data_ate = "AND DATE(co.data) <= :data_ate";
            $data_ate_query = "&data_ate={$convert_data_ate}";
        }else{
            $data_ate = "";
            $data_ate_query = "";
        }

        $TplView->InformacoesFiltradas = "Informações de: <b>{$_POST['data_de']}</b> até <b>{$_POST['data_ate']}</b>";

        if($_POST['plano']){
            $plano = "AND co.plano = :plano";
            $plano_query = "&plano={$_POST['plano']}";

            if($_POST['plano'] == "mensal_plan"){
                $intervalo_tempo = "AND MONTH(co.data) = MONTH(('{$convert_data_de}') - INTERVAL 1 MONTH)";
            }

            if($_POST['plano'] == "trimestral_plan"){
                $intervalo_tempo = "AND MONTH(co.data) = MONTH(('{$convert_data_de}') - INTERVAL 3 MONTH)";
            }

            if($_POST['plano'] == "semestral_plan"){
                $intervalo_tempo = "AND MONTH(co.data) = MONTH(('{$convert_data_de}') - INTERVAL 6 MONTH)";
            }

            if($_POST['plano'] == "anual_plan"){
                $intervalo_tempo = "AND YEAR(co.data) = YEAR(('{$convert_data_de}') - INTERVAL 1 YEAR)";
            }
        }else{
            $plano = "";
            $plano_query = "";
        }

        $ReadVendas = new Read();
        $ReadRenovacoes = new Read();
        $ReadVendasEm = new Read();

        //Número de vendas no período
        $ReadVendas->FullRead("SELECT COUNT(*) AS nvendas_periodo, SUM(co.valor) AS valor_vendas_periodo FROM compras co WHERE co.status_pgto = :status_pgto AND renovacao = :renovacao {$plano} {$data_de} {$data_ate}", "status_pgto=paid&renovacao=1{$plano_query}{$data_de_query}{$data_ate_query}");

        $ReadRenovacoes->FullRead("SELECT COUNT(*) AS nrenovacoes_periodo, SUM(co.valor) AS valor_renovacoes_periodo FROM compras co WHERE co.status_pgto = :status_pgto AND renovacao = :renovacao {$plano} {$data_de} {$data_ate}", "status_pgto=paid&renovacao=2{$plano_query}{$data_de_query}{$data_ate_query}");
        
        $ReadVendasEm->FullRead("SELECT COUNT(*) AS numero_vendas, co.data AS data_vendas, SUM(co.valor) AS valor_mes_anterior FROM compras co WHERE co.status_pgto = :status_pgto AND renovacao = :renovacao {$plano} {$intervalo_tempo}", "status_pgto=paid&renovacao=1{$plano_query}");
    
        if($ReadVendas->GetResult()){

            $TplView->SomaVendasPeriodo = $ReadVendas->GetResult()[0]['nvendas_periodo'];
            $TplView->SomaRenovacoesPeriodo = $ReadRenovacoes->GetResult()[0]['nrenovacoes_periodo'];
            $TplView->NomeMesAnterior = date("m/Y", strtotime($ReadVendasEm->GetResult()[0]['data_vendas']));
            $TplView->SomaVendasMesAnterior = $ReadVendasEm->GetResult()[0]['numero_vendas'];
            $TplView->SomaTaxaRenovacao = round(($ReadRenovacoes->GetResult()[0]['nrenovacoes_periodo'] / $ReadVendasEm->GetResult()[0]['numero_vendas']) * 100);

            $TplView->ValorVendasPeriodo = number_format($ReadVendas->GetResult()[0]['valor_vendas_periodo'], 2, ',', '.');
            $TplView->ValorRenovacoesPeriodo = number_format($ReadRenovacoes->GetResult()[0]['valor_renovacoes_periodo'], 2, ',', '.');
            $TplView->ValorVendasMesAnterior = number_format($ReadVendasEm->GetResult()[0]['valor_mes_anterior'], 2, ',', '.');

        }

    }

    $TplView->block("TAXA_RENOVACAO");

}
?>