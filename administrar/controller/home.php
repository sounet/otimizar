<?php
$TplView->addFile("INCLUDE_PG", "view/home.html");

// $ReadSomas = new Read();

// $ReadSomas->FullRead("SELECT SUM(comp.valor) AS total FROM compras comp WHERE DATE(comp.data) = CURDATE() AND status_pgto = :status_pgto", "status_pgto=paid");
// $TplView->SomaHoje = number_format($ReadSomas->GetResult()[0]['total'], 2, ',', '.');

// $ReadSomas->FullRead("SELECT SUM(comp.valor) AS total FROM compras comp WHERE DATE(comp.data) = CURDATE() - INTERVAL 1 DAY AND status_pgto = :status_pgto", "status_pgto=paid");
// $TplView->SomaOntem = number_format($ReadSomas->GetResult()[0]['total'], 2, ',', '.');

// $ReadSomas->FullRead("SELECT SUM(comp.valor) AS total FROM compras comp WHERE DATE(comp.data) = CURDATE() - INTERVAL 2 DAY AND status_pgto = :status_pgto", "status_pgto=paid");
// $TplView->SomaAntOntem = number_format($ReadSomas->GetResult()[0]['total'], 2, ',', '.');

// $ReadSomas->FullRead("SELECT SUM(comp.valor) AS total FROM compras comp WHERE DATE(comp.data) = CURDATE() - INTERVAL 3 DAY AND status_pgto = :status_pgto", "status_pgto=paid");
// $TplView->SomaAntAntOntem = number_format($ReadSomas->GetResult()[0]['total'], 2, ',', '.');

// $ReadSomas->FullRead("SELECT SUM(comp.valor) AS total FROM compras comp WHERE status_pgto = :status_pgto", "status_pgto=pending");
// $TplView->SomaAguardando = number_format($ReadSomas->GetResult()[0]['total'], 2, ',', '.');

// $ReadSomas->FullRead("SELECT SUM(comp.valor) AS total FROM compras comp WHERE MONTH(comp.data) = MONTH(CURDATE()) AND status_pgto = :status_pgto", "status_pgto=expired");
// $TplView->SomaExpirados = number_format($ReadSomas->GetResult()[0]['total'], 2, ',', '.');

// $ReadSomas->FullRead("SELECT SUM(comp.valor) AS total FROM compras comp WHERE status_pgto = :status_pgto AND metodo = :metodo", "status_pgto=paid&metodo=credit_card");
// $TplView->SomaCartao = number_format($ReadSomas->GetResult()[0]['total'], 2, ',', '.');

// $ReadSomas->FullRead("SELECT SUM(comp.valor) AS total FROM compras comp WHERE status_pgto = :status_pgto AND metodo = :metodo", "status_pgto=paid&metodo=bank_slip");
// $TplView->SomaBoleto = number_format($ReadSomas->GetResult()[0]['total'], 2, ',', '.');

// $ReadSomas->FullRead("SELECT SUM(comp.valor) AS total FROM compras comp WHERE MONTH(comp.data) = MONTH(CURDATE()) AND status_pgto = :status_pgto", "status_pgto=paid");
// $TplView->SomaEsteMes = number_format($ReadSomas->GetResult()[0]['total'], 2, ',', '.');

// $ReadSomas->FullRead("SELECT SUM(comp.valor) AS total FROM compras comp WHERE MONTH(comp.data) = MONTH(CURDATE() - INTERVAL 1 MONTH) AND status_pgto = :status_pgto", "status_pgto=paid");
// $TplView->SomaMesPassado = number_format($ReadSomas->GetResult()[0]['total'], 2, ',', '.');

// $ReadSomas->FullRead("SELECT comp.data AS data_recorde, SUM(comp.valor) AS total FROM compras comp WHERE status_pgto = :status_pgto GROUP BY DATE(comp.data) ORDER BY total DESC LIMIT 1", "status_pgto=paid");
// $TplView->DataRecordeDiario = date("d/m/Y", strtotime($ReadSomas->GetResult()[0]['data_recorde']));
// $TplView->SomaRecordeDiario = number_format($ReadSomas->GetResult()[0]['total'], 2, ',', '.');

// $ReadSomas->FullRead("SELECT MONTH(comp.data) AS mes_recorde, YEAR(comp.data) AS ano_recorde, SUM(comp.valor) AS total FROM compras comp WHERE status_pgto = :status_pgto GROUP BY MONTH(comp.data), YEAR(comp.data) LIMIT 1", "status_pgto=paid");
// $TplView->MesRecordeMensal = $ReadSomas->GetResult()[0]['mes_recorde'];
// $TplView->AnoRecordeMensal = $ReadSomas->GetResult()[0]['ano_recorde'];
// $TplView->SomaRecordeMensal = number_format($ReadSomas->GetResult()[0]['total'], 2, ',', '.');


//Níveis de usuários
$FinalParseQuery = "";
$FinalQuery = "";

if($UserLogado[0] == "admin"){

    $TplView->block("ADMINISTRACAO");

}elseif($UserLogado[0] == "revendedores"){

    $FinalParseQuery = "AND co.revendedor = :revendedor";
    $FinalQuery = "&revendedor={$UserLogado[1]}";

}

$ReadCompras = new Read();
if(isset($_POST['NomeCliente'])){
    $ReadCompras->FullRead("SELECT cl.nome, co.id, co.data, co.valor, co.status_pgto FROM compras co LEFT JOIN clientes cl ON co.cod_cliente = cl.cod_cliente WHERE (cl.nome LIKE '%' :serach_string '%') {$FinalParseQuery} ORDER BY co.id DESC", "serach_string={$_POST['NomeCliente']}{$FinalQuery}");
}else{
    $ReadCompras->FullRead("SELECT cl.nome, co.id, co.data, co.valor, co.status_pgto FROM compras co LEFT JOIN clientes cl ON co.cod_cliente = cl.cod_cliente WHERE co.id > :id {$FinalParseQuery} ORDER BY co.id DESC LIMIT 250", "id=0{$FinalQuery}");
}

if($ReadCompras->GetResult()){
                        
    foreach($ReadCompras->GetResult() as $res_list){
        $TplView->NomeComprador = $res_list['nome'];
        $TplView->DataCompra = date("d/m/Y H:i", strtotime($res_list['data']));
        $TplView->ValorCompra = number_format($res_list['valor'], 2, ',', '.');

        $StatusCompra = Valida::StatusCompra($res_list['status_pgto']);
        $TplView->TitleStatus = $StatusCompra[0];
        $TplView->DescStatus = $StatusCompra[1];
        $TplView->BgStatus = $StatusCompra[2];

        $TplView->IdCompra = $res_list['id'];
        $TplView->block("FOREACH");
    }

	$TplView->block("TRANSACOES");
}
?>