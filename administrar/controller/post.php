<?php
session_start();
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
require ("../../class/config.php");
$InforVal = new Valida();

$Info_post = filter_input_array(INPUT_POST, FILTER_DEFAULT);

//Verifica se todos os campos estão preenchidos e se o tpf veio preenchido
if (empty($Info_post) || (!isset($Info_post['tpf']) || empty($Info_post['tpf']))) {
    echo "<script>window.location='".URL_ADMIN."';</script>";
    exit();
}

$JsonResults = array();

if($InforVal->CheckAes($Info_post['tpf']) == "loginadmin"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de login
    $LoginAdmin = new Login();
    $LoginAdmin->ExeLogin($Info_post);
    if($LoginAdmin->GetResult()){
        $JsonResults['success'] = Mensagens::SetLogSucess();
        $JsonResults['urldir'] = URL_ADMIN;
    }else{
        $JsonResults['error'] = $LoginAdmin->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "updConfig"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de configuração do sistema
    $ConfiguracoesAdmin = new ConfiguracoesAdmin();
    $ConfiguracoesAdmin->ExeAtualizacao($Info_post);

    if($ConfiguracoesAdmin->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucess();
        $JsonResults['urldir'] = URL_ADMIN."/configuracoes";
    }else{
        $JsonResults['error'] = $ConfiguracoesAdmin->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "cadUsers"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de usuários administradores
    $UsuariosAdmin = new UsuariosAdmin();
    $UsuariosAdmin->ExeCadastro($Info_post);

    if($UsuariosAdmin->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucess();
        $JsonResults['urldir'] = URL_ADMIN."/usuarios/listar";
    }else{
        $JsonResults['error'] = $UsuariosAdmin->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "updUsers"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de usuários administradores
    $UsuariosAdmin = new UsuariosAdmin();
    $UsuariosAdmin->ExeAtualizacao($Info_post);

    if($UsuariosAdmin->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetUpdInfoSucess();
        $JsonResults['urldir'] = URL_ADMIN."/usuarios/listar";
    }else{
        $JsonResults['error'] = $UsuariosAdmin->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "delUsers"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de usuários administradores
    $UsuariosAdmin = new UsuariosAdmin();
    $UsuariosAdmin->ExeDelete($Info_post);

    if($UsuariosAdmin->GetResult()){
        $JsonResults['success'] = true;
    }else{
        $JsonResults['error'] = $UsuariosAdmin->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "cadCupons"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de cupons
    $Cupons = new Cupons();
    $Cupons->ExeCadastro($Info_post);

    if($Cupons->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucess();
        $JsonResults['urldir'] = URL_ADMIN."/cupons/listar";
    }else{
        $JsonResults['error'] = $Cupons->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "updCupons"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de cupons
    $Cupons = new Cupons();
    $Cupons->ExeAtualizacao($Info_post);

    if($Cupons->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetUpdInfoSucess();
        $JsonResults['urldir'] = URL_ADMIN."/cupons/listar";
    }else{
        $JsonResults['error'] = $Cupons->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "delCupons"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de cupons
    $Cupons = new Cupons();
    $Cupons->ExeDelete($Info_post);

    if($Cupons->GetResult()){
        $JsonResults['success'] = true;
    }else{
        $JsonResults['error'] = $Cupons->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "updTextos"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de configuração do sistema
    $TextosPaginas = new TextosPaginas();
    $TextosPaginas->ExeAtualizacao($Info_post);

    if($TextosPaginas->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucess();
        $JsonResults['urldir'] = URL_ADMIN."/textos/".Valida::Rebase3($Info_post['pag_form']);
    }else{
        $JsonResults['error'] = $TextosPaginas->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "cadDuvidas"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de duvidas frequentes
    $Duvidas = new Duvidas();
    $Duvidas->ExeCadastro($Info_post);

    if($Duvidas->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucess();
        $JsonResults['urldir'] = URL_ADMIN."/duvidas_frequentes/listar";
    }else{
        $JsonResults['error'] = $Duvidas->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "updDuvidas"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de duvidas
    $Duvidas = new Duvidas();
    $Duvidas->ExeAtualizacao($Info_post);

    if($Duvidas->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetUpdInfoSucess();
        $JsonResults['urldir'] = URL_ADMIN."/duvidas_frequentes/listar";
    }else{
        $JsonResults['error'] = $Duvidas->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "delDuvidas"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de duvidas
    $Duvidas = new Duvidas();
    $Duvidas->ExeDelete($Info_post);

    if($Duvidas->GetResult()){
        $JsonResults['success'] = true;
    }else{
        $JsonResults['error'] = $Duvidas->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "updPlanos"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de planos
    $Planos = new Planos();
    $Planos->ExeAtualizacao($Info_post);

    if($Planos->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetUpdInfoSucess();
        $JsonResults['urldir'] = URL_ADMIN."/planos/listar";
    }else{
        $JsonResults['error'] = $Planos->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "cadOrigemCompra"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de origem das compras
    $OrigemCompra = new OrigemCompra();
    $OrigemCompra->ExeCadastro($Info_post);

    if($OrigemCompra->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucess();
        $JsonResults['urldir'] = URL_ADMIN."/origem_compra/listar";
    }else{
        $JsonResults['error'] = $OrigemCompra->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "updOrigemCompra"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de origem das compras
    $OrigemCompra = new OrigemCompra();
    $OrigemCompra->ExeAtualizacao($Info_post);

    if($OrigemCompra->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetUpdInfoSucess();
        $JsonResults['urldir'] = URL_ADMIN."/origem_compra/listar";
    }else{
        $JsonResults['error'] = $OrigemCompra->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "delOrigemCompra"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de origem das compras
    $OrigemCompra = new OrigemCompra();
    $OrigemCompra->ExeDelete($Info_post);

    if($OrigemCompra->GetResult()){
        $JsonResults['success'] = true;
    }else{
        $JsonResults['error'] = $OrigemCompra->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "SendAutomacao"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de automação iniciada
    $Compras = new Compras();
    $Compras->SendAutomacao($Info_post);

    if($Compras->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucessRefresh();
        $JsonResults['urldir'] = URL_ADMIN."/compras/detalhes/".$Info_post['IdCompra'];
    }else{
        $JsonResults['error'] = $Compras->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "SendObservacoes"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de observações da compra
    $Compras = new Compras();
    $Compras->SendObservacao($Info_post);

    if($Compras->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucessRefresh();
        $JsonResults['urldir'] = URL_ADMIN."/compras/detalhes/".$Info_post['IdCompra'];
    }else{
        $JsonResults['error'] = $Compras->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "SendRenovacao"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de renovação da compra
    $Compras = new Compras();
    $Compras->SendRenovacao($Info_post);

    if($Compras->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucessRefresh();
        $JsonResults['urldir'] = URL_ADMIN."/compras/detalhes/".$Info_post['IdCompra'];
    }else{
        $JsonResults['error'] = $Compras->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "SendDiasAdicionados"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de acrescentamento de dias para a compra
    $Compras = new Compras();
    $Compras->SendDiasAdicionados($Info_post);

    if($Compras->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucessRefresh();
        $JsonResults['urldir'] = URL_ADMIN."/compras/detalhes/".$Info_post['IdCompra'];
    }else{
        $JsonResults['error'] = $Compras->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "cadRevendedores"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de revendedores
    $Revendedores = new Revendedores();
    $Revendedores->ExeCadastro($Info_post);

    if($Revendedores->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucess();
        $JsonResults['urldir'] = URL_ADMIN."/revendedores/listar";
    }else{
        $JsonResults['error'] = $Revendedores->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "updRevendedores"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de cupons
    $Revendedores = new Revendedores();
    $Revendedores->ExeAtualizacao($Info_post);

    if($Revendedores->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetUpdInfoSucess();
        $JsonResults['urldir'] = URL_ADMIN."/revendedores/listar";
    }else{
        $JsonResults['error'] = $Revendedores->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "SendRevendedores"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de revendedores
    $Compras = new Compras();
    $Compras->SendRevendedores($Info_post);

    if($Compras->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucessRefresh();
        $JsonResults['urldir'] = URL_ADMIN."/compras/detalhes/".$Info_post['IdCompra'];
    }else{
        $JsonResults['error'] = $Compras->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "SendSeo"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de revendedores
    $etapas = new Etapas();
    $etapas->setPost($Info_post);
    $etapas->nextSeo($Info_post);

    if($etapas->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucessRefresh();
        $JsonResults['urldir'] = URL_ADMIN."/compras/detalhes/".$Info_post['IdCompra'];
    }else{
        $JsonResults['error'] = $etapas->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "SendAdword"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de revendedores
    $etapas = new Etapas();
    $etapas->setPost($Info_post);
    $etapas->nextAd($Info_post);

    if($etapas->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucessRefresh();
        $JsonResults['urldir'] = URL_ADMIN."/compras/detalhes/".$Info_post['IdCompra'];
    }else{
        $JsonResults['error'] = $etapas->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "SendCompraManual"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);

    $Info_post['comprovante'] = $_FILES['comprovante'];
    $Info_post['origem_criacao'] = 'painel administrar';
    
    //Campos não obrigatórios
    $Bypass = array('cpf_cnpj', 'cep', 'endereco', 'bairro');
    
    //Executa a class de revendedores
    $CompraAvulsa = new CompraAvulsa();
    $CompraAvulsa->ExeCadastro($Info_post, $Bypass);

    if($CompraAvulsa->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucessRefresh();
        $JsonResults['urldir'] = URL_ADMIN;
    }else{
        $JsonResults['error'] = $CompraAvulsa->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

} else {
    echo "<script>window.location='".URL_ADMIN."';</script>";
    exit();
}
?>