<?php
if ($url[0] == "Inicial")                           {include "principal.php";}
elseif ($url[0] == "configuracoes")                 {include "configuracoes.php";}
elseif ($url[0] == "usuarios")                 		{include "usuarios.php";}
elseif ($url[0] == "cupons")                 		{include "cupons.php";}
elseif ($url[0] == "duvidas_frequentes")            {include "duvidas_frequentes.php";}
elseif ($url[0] == "textos")                		{include "textos.php";}
elseif ($url[0] == "planos")                		{include "planos.php";}
elseif ($url[0] == "origem_compra")                	{include "origem_compra.php";}
elseif ($url[0] == "compras")                		{include "compras.php";}
elseif ($url[0] == "revendedores")                	{include "revendedores.php";}
elseif ($url[0] == "nao_finalizadas")               {include "nao_finalizadas.php";}
elseif ($url[0] == "taxa_renovacao")                {include "taxa_renovacao.php";}
elseif ($url[0] == "plano_mais_vendido")            {include "plano_mais_vendido.php";}
else {
    echo "<script>window.location='".URL_ADMIN."';</script>";
    exit();
}
?>