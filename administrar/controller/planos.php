<?php
//Verifica o tipo de usuário logado
if($UserLogado[0] == "revendedores"){

    $TplView->addFile("INCLUDE_PG", "view/conteudo_restrito.html");

}else{

    $TplView->addFile("INCLUDE_PG", "view/planos.html");
    $TplView->page_atual_admin = $url[0];
    $ReadPlanos = new Read();

    if($url[1] == "listar"){
        
        $ReadPlanos->ExeRead("planos", "ORDER BY id DESC");
        if($ReadPlanos->GetResult()){
                                
            foreach($ReadPlanos->GetResult() as $res_list){
                $TplView->titulo = $res_list['titulo'];
                $TplView->valor = number_format($res_list['valor'], 2, ',', '.');
                $TplView->ident = $res_list['id'];
                $TplView->block("FOREACH");
            }

            $TplView->block("VERIFICA_LISTAGEM");
        }

        $TplView->block("BLOCK_LISTAR");

    }elseif($url[1] == "editar"){

        $TplView->tpf_form = Valida::GeraAes("updPlanos");
        $TplView->idfinfo = $url[2];
        $ReadPlanos->ExeRead("planos", "WHERE id = :id", "id={$url[2]}");
        $TplView->valor = $ReadPlanos->GetResult()[0]['valor'];
        $TplView->block("BLOCK_EDITAR");

    }

}
?>