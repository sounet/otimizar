<?php
$TplView->addFile("INCLUDE_PG", "view/compras.html");

$Read = new Read();

if($url[1] == "detalhes"){

    $ParseQuery = "";
    $FinalQuery = "";
    
    $Read->FullRead("SELECT cl.nome, cl.email, cl.celular, cl.cpf_cnpj, cl.endereco, cl.bairro, cl.cep, cl.numero, cl.cidade, cl.estado, cl.origem, co.id, co.cod_assinatura, co.cod_cliente, co.plano, co.metodo, co.valor, co.renovacao, co.status_pgto, co.status_ass, co.revendedor, co.dias_adicionados, co.data, co.comprovante FROM compras co LEFT JOIN clientes cl ON co.cod_cliente = cl.cod_cliente WHERE co.id = :id {$ParseQuery}", "id={$url[2]}{$FinalQuery}");
    if($Read->GetResult()){

        //Origem do comprador
        $ReadOrigem = new Read();
        $ReadOrigem->ExeRead("origem_compra", "WHERE id = :id", "id={$Read->GetResult()[0]['origem']}");

        //Dados do comprador
        $TplView->NomeComprador = $Read->GetResult()[0]['nome'];
        $TplView->EmailComprador = $Read->GetResult()[0]['email'];
        $TplView->CelularComprador = $Read->GetResult()[0]['celular'];
        $TplView->CelularLink = str_replace(array("(", ")", "-", " "), "", $Read->GetResult()[0]['celular']);
        $TplView->OrigemComprador = ($Read->GetResult()[0]['origem'] <> "0" ? $ReadOrigem->GetResult()[0]['titulo'] : "Não informado");
        $TplView->CpfCnpjComprador = $Read->GetResult()[0]['cpf_cnpj'];
        $TplView->EnderecoComprador = $Read->GetResult()[0]['endereco'].", ".$Read->GetResult()[0]['bairro']."<br>".$Read->GetResult()[0]['cep'].", ".$Read->GetResult()[0]['numero'].", ".$Read->GetResult()[0]['cidade']." / ".$Read->GetResult()[0]['estado'];
        $TplView->Comprovante = ($Read->GetResult()[0]['comprovante'] <> "" ? "<p><strong>Comprovante: </strong> <a href='{URL_BASE}/uploads/comprovantes/".$Read->GetResult()[0]['comprovante']."' target='_blank' style='color:#09f; font-size: 15px;'>Visualizar</a></p>" : "");
        $TplView->IdCompra = $Read->GetResult()[0]['id'];

        $readPlano = new Read();
        $readPlano->ExeRead("planos","WHERE identificador = :plano", "plano={$Read->GetResult()[0]['plano']}");
        $TplView->PlanoCompra = $readPlano->GetResult()[0]['titulo'];

        $TplView->RenovacaoCompra = ($Read->GetResult()[0]['renovacao'] == "1" ? "Não" : "Sim");
        if($Read->GetResult()[0]['metodo'] == "bank_slip"){
            $TplView->MetodoCompra = "Boleto bancário";
        }elseif($Read->GetResult()[0]['metodo'] == "credit_card"){
            $TplView->MetodoCompra = "Cartão de crédito";
        }elseif($Read->GetResult()[0]['metodo'] == "deposit"){
            $TplView->MetodoCompra = "Depósito";
        }elseif($Read->GetResult()[0]['metodo'] == "other_method"){
            $TplView->MetodoCompra = "Outros";
        }
        $TplView->ValorCompra = number_format($Read->GetResult()[0]['valor'], 2, ',', '.');
        $StatusCompra = Valida::StatusCompra($Read->GetResult()[0]['status_pgto']);
        $TplView->TitleStatus = $StatusCompra[0];
        $TplView->DescStatus = $StatusCompra[1];
        $TplView->BgStatus = $StatusCompra[2];
        $StatusAssinatura = Valida::StatusAssinatura($Read->GetResult()[0]['status_ass']);
        $TplView->TitleStatusAss = $StatusAssinatura[0];
        $TplView->DescStatusAss = $StatusAssinatura[1];
        $TplView->BgStatusAss = $StatusAssinatura[2];
        $TplView->DataCompra = date("d/m/Y H:i", strtotime($Read->GetResult()[0]['data']));

        //Variavel com a lista das etapas
        $readEtapa = New Read();

        if ( $readPlano->GetResult()[0]['identificador'] == "otimizar_seo"      || 
             $readPlano->GetResult()[0]['identificador'] == "assinatura_seo"    || 
             $readPlano->GetResult()[0]['identificador'] == "otimizar_combo"    ){

            //Tipo segmentação para exibição
            $TplSegmentacao = ( $readPlano->GetResult()[0]['identificador'] == 'otimizar_combo' ) ? 'SEGMENTACAO_COMBO' : 'SEGMENTACAO_SEO' ;

            //Verifica qual etapa do Seo o cliente está
            $readEtapa->ExeRead("etapas","WHERE cod_compra = :compra", "compra={$Read->GetResult()[0]['id']}");
            if($readEtapa->GetResult()){
                $seoDisable = $readEtapa->GetResult()[0]['seo'];
            }else{
                $seoDisable = 0;
            }

            //Faz listagem de todas as etapas do SEO
            $readSeo = New Read();
            $readSeo->FullRead("SELECT * FROM seo WHERE tipo = 'seo'");
            foreach($readSeo->GetResult() as $res_list) {

                if($res_list['etapa'] <= $seoDisable){
                    $TplView->seoDisable = "disabled";
                }else{
                    $TplView->seoDisable = "";
                }

                $TplView->seoValor = $res_list['etapa'];
                $TplView->seoNome = $res_list['nome'];
                $TplView->block("FOREACH_SEO");
            }
            $TplView->block("SEO");
        }

        if ( $readPlano->GetResult()[0]['identificador'] == "otimizar_adwords"      ||
             $readPlano->GetResult()[0]['identificador'] == "assinatura_adwords"    ||
             $readPlano->GetResult()[0]['identificador'] == "otimizar_combo"        ){

            //Tipo segmentação para exibição
            $TplSegmentacao = ( $readPlano->GetResult()[0]['identificador'] == 'otimizar_combo' ) ? 'SEGMENTACAO_COMBO' : 'SEGMENTACAO_ADWORDS' ;

            //Verifica qual etapa do AdWords o cliente está
            $readEtapa->ExeRead("etapas","WHERE cod_compra = :compra", "compra={$Read->GetResult()[0]['id']}");
            if($readEtapa->GetResult()){
                $adDisable = $readEtapa->GetResult()[0]['ad'];
            }else{
                $adDisable = 0;
            }

            //Faz listagem de todas as etapas do SEO
            $readAdword = New Read();
            $readAdword->FullRead("SELECT * FROM seo WHERE tipo = 'adword'");
            foreach($readAdword->GetResult() as $res_list) {
                if($res_list['etapa'] <= $adDisable){
                    $TplView->adDisable = "disabled";
                }else{
                    $TplView->adDisable = "";
                }

                $TplView->adValor = $res_list['etapa'];
                $TplView->adNome = $res_list['nome'];
                $TplView->block("FOREACH_ADWORDS");
            }
            $TplView->block("ADWORDS");
        }

        //Lista demais compras do mesmo cliente
        $ReadDmsCompras = new Read();
        $ReadDmsCompras->FullRead("SELECT co.id, co.data, co.valor, co.status_pgto FROM compras co WHERE co.cod_cliente = :cod_cliente AND co.id <> :id ORDER BY co.id DESC", "cod_cliente={$Read->GetResult()[0]['cod_cliente']}&id={$url[2]}");
        if($ReadDmsCompras->GetResult()){
                                
            foreach($ReadDmsCompras->GetResult() as $res_list){
                $TplView->DataCompraDms = date("d/m/Y H:i", strtotime($res_list['data']));
                $TplView->ValorCompraDms = number_format($res_list['valor'], 2, ',', '.');

                $StatusCompra = Valida::StatusCompra($res_list['status_pgto']);
                $TplView->TitleStatusDms = $StatusCompra[0];
                $TplView->DescStatusDms = $StatusCompra[1];
                $TplView->BgStatusDms = $StatusCompra[2];

                $TplView->IdCompraDms = $res_list['id'];
                $TplView->block("FOREACH_DMSCOMPRAS");
            }

            $TplView->block("DMSTRANSACOES");
        }

        //Verifica segmentacao
        $ReadSegmentacao = new Read();
        $ReadSegmentacao->ExeRead("segmetacao", "WHERE cod_cliente = :cod_cliente AND cod_assinatura = :cod_assinatura", "cod_cliente={$Read->GetResult()[0]['cod_cliente']}&cod_assinatura={$Read->GetResult()[0]['cod_assinatura']}");

        if($ReadSegmentacao->GetResult()){

            // Segmentação em comum
            $TplView->Site_URL = $ReadSegmentacao->GetResult()[0]['site_url'];
            $TplView->Keywords = $ReadSegmentacao->GetResult()[0]['keywords'];
            $TplView->Area_Atuacao = $ReadSegmentacao->GetResult()[0]['area_atuacao'];
            $TplView->Google_Login = $ReadSegmentacao->GetResult()[0]['google_login'];
            $TplView->Google_Senha = $ReadSegmentacao->GetResult()[0]['google_senha'];
            $TplView->DataPreenchimento = date("d/m/Y H:i", strtotime($ReadSegmentacao->GetResult()[0]['data']));
            
            if ($TplSegmentacao == 'SEGMENTACAO_SEO' || $TplSegmentacao == 'SEGMENTACAO_COMBO') {
                // Segmentação Otimizar SEO
                $TplView->Site_Titulo = $ReadSegmentacao->GetResult()[0]['site_titulo'];
                $TplView->FTP_Host = $ReadSegmentacao->GetResult()[0]['ftp_host'];
                $TplView->FTP_Login = $ReadSegmentacao->GetResult()[0]['ftp_login'];
                $TplView->FTP_Senha = $ReadSegmentacao->GetResult()[0]['ftp_senha'];
                $TplView->Plataforma_Login = $ReadSegmentacao->GetResult()[0]['plataforma_login'];
                $TplView->Plataforma_Senha = $ReadSegmentacao->GetResult()[0]['plataforma_senha'];
                $TplView->block('SEGMENTACAO_SEO');
            }

            if ($TplSegmentacao == 'SEGMENTACAO_ADWORDS' || $TplSegmentacao == 'SEGMENTACAO_COMBO') {
                // Segmentação Otimizar AdWords
                $TplView->Site_Anunciado = str_replace('//', '', $ReadSegmentacao->GetResult()[0]['site_url']);
                $TplView->Destaque = $ReadSegmentacao->GetResult()[0]['destaque'];
                $TplView->Orcamento = $ReadSegmentacao->GetResult()[0]['orcamento'];
                $TplView->Veiculacao = $ReadSegmentacao->GetResult()[0]['veiculacao'];
                $TplView->Publico_Alvo = $ReadSegmentacao->GetResult()[0]['publico_alvo'];
                $TplView->Valor_Investido = $ReadSegmentacao->GetResult()[0]['valor_investido'];
                $TplView->Destaque_Inicial = $ReadSegmentacao->GetResult()[0]['destaque_inicial'];
                $TplView->Clientes_Potencial = $ReadSegmentacao->GetResult()[0]['clientes_potencial'];
                $TplView->block('SEGMENTACAO_ADWORDS');
            }

            $TplView->block('SEGMENTACAO');

        }else{

            //Informa que a segmentação não foi enviada
            $TplView->DataPreenchimento = "Não enviada";
        }

        //Código do formulário de seo
        $TplView->tpf_form_seo = Valida::GeraAes("SendSeo");

        //Código do formulário de adword
        $TplView->tpf_form_adword = Valida::GeraAes("SendAdword");

        //Código do formulário de revendedores
        $TplView->tpf_form_revendedores = Valida::GeraAes("SendRevendedores");

        //Código do formulário de renovação: Sim ou não
        $TplView->tpf_form_renovacao = Valida::GeraAes("SendRenovacao");

        //Código do formulário de dias adicionados: Sim ou não
        $TplView->tpf_form_dias = Valida::GeraAes("SendDiasAdicionados");

        //Código do formulário de observação
        $TplView->tpf_form_observacoes = Valida::GeraAes("SendObservacoes");

        //Verifica e lista observações
        $ReadObservacoes = new Read();
        $ReadObservacoes->ExeRead("observacoes", "WHERE cod_assinatura = :cod_assinatura", "cod_assinatura={$Read->GetResult()[0]['cod_assinatura']}");
        if($ReadObservacoes->GetResult()){
            foreach($ReadObservacoes->GetResult() as $res_observacoes){
                $TplView->ObservacaoRealizada = $res_observacoes['observacao'];
                $TplView->ObsPorQuemData = $res_observacoes['nome_postador']." em ".date("d/m/Y H:i", strtotime($res_observacoes['data']));
                $TplView->block("FOREACH_OBSERVACOES");
            }
            $TplView->block("OBSERVACOES_COMPRA");
        }

        //Verifica e lista os revendedores
        $ReadRevendedores = new Read();
        $ReadRevendedores->ExeRead("revendedores", "WHERE status = :status ORDER BY nome ASC", "status=2");
        if($ReadRevendedores->GetResult()){
            foreach($ReadRevendedores->GetResult() as $res_revendedores){
                $TplView->IdRevendedor = $res_revendedores['id'];
                $TplView->NomeRevendedor = $res_revendedores['nome'];
                $TplView->block("FOREACH_REVENDEDORES");
            }
        }
        if($Read->GetResult()[0]['revendedor'] == "0"){
            $TplView->Revendedor = "";
        }else{
            $ReadRevendedores->ExeRead("revendedores", "WHERE id = :id", "id={$Read->GetResult()[0]['revendedor']}");
            $TplView->Revendedor = "<p><strong>Revendedor: </strong> {$ReadRevendedores->GetResult()[0]['nome']}</p>";
        }

        //Selecionar manualmente se é ou não uma renovação
        $TplView->RenovacaoSelectSim = ($Read->GetResult()[0]['renovacao'] == "2" ? "selected='selected'" : "");
        $TplView->RenovacaoSelectNao = ($Read->GetResult()[0]['renovacao'] == "1" ? "selected='selected'" : "");

        //Verifica se os dias foram adicionados, se sim não apresenta o campo de dias
        if($Read->GetResult()[0]['dias_adicionados'] == "1"){
            $TplView->block("DIAS_ADICIONADOS");
        }

        //Selecionar manualmente se foi ou não adicionado dias
        $TplView->DiasSelectSim = ($Read->GetResult()[0]['dias_adicionados'] == "2" ? "selected='selected'" : "");
        $TplView->DiasSelectNao = ($Read->GetResult()[0]['dias_adicionados'] == "1" ? "selected='selected'" : "");

        //Codigo criptografado para preencher informacoes
        $TplView->CodAssinatura = Valida::Base3($Read->GetResult()[0]['cod_assinatura']);

        if($UserLogado[0] == "admin"){
            $TplView->block("RESTRITO_ADMINISTRACAO");
        }elseif($UserLogado[0] == "revendedor"){
            $ParseQuery = "AND co.revendedor = :revendedor";
            $FinalQuery = "&revendedor={$UserLogado[1]}";
        }

        $TplView->block("DETALHES");

    }else{
        echo "<script>window.location='".URL_ADMIN."';</script>";
        exit();
    }


}elseif($url[1] == "inserir_compra_manual"){

    //Leitura de tabela de origens das compras
    $ReadOrigemCompra = new Read();
    $ReadOrigemCompra->ExeRead("origem_compra", "ORDER BY id DESC");
    if($ReadOrigemCompra->GetResult()){
        foreach($ReadOrigemCompra->GetResult() as $res_list){
            $TplView->titulo_origem = $res_list['titulo'];
            $TplView->ident_origem = Valida::Base3($res_list['id']);
            $TplView->block("FOREACH_ORIGENS");
        }
        $TplView->block("VERIFICA_ORIGENS");
    }

    //Leitura de tabela de planos
    $ReadPlanosList = new Read();
    $ReadPlanosList->ExeRead("planos", "ORDER BY id DESC");
    if($ReadPlanosList->GetResult()){
        foreach($ReadPlanosList->GetResult() as $res_list){
            $TplView->titulo_plano = $res_list['titulo'];
            $TplView->ident_plano = $res_list['identificador'];
            $TplView->block("FOREACH_PLANOS");
        }
        $TplView->block("VERIFICA_PLANOS");
    }

    //Código do formulário de dias adicionados: Sim ou não
    $TplView->tpf_form = Valida::GeraAes("SendCompraManual");

    $TplView->block("INSERIR_COMPRA");

}
?>