<?php
//Verifica o tipo de usuário logado
if($UserLogado[0] == "revendedores"){

    $TplView->addFile("INCLUDE_PG", "view/conteudo_restrito.html");

}else{

	$TplView->addFile("INCLUDE_PG", "view/configuracoes.html");
	$TplView->tpf_form = Valida::GeraAes("updConfig");
	$TplView->email_principal = $SqlSistema->GetResult()[0]['email_principal'];
	$TplView->email_resposta = $SqlSistema->GetResult()[0]['email_resposta'];
	$TplView->vendas = $SqlSistema->GetResult()[0]['vendas'];
	$TplView->suporte = $SqlSistema->GetResult()[0]['suporte'];
	$TplView->descrecorrenciaNao = ($SqlSistema->GetResult()[0]['desconto_recorrencias'] == 1 ? ' selected="selected"' : "");
	$TplView->descrecorrenciaSim = ($SqlSistema->GetResult()[0]['desconto_recorrencias'] == 2 ? ' selected="selected"' : "");
	$TplView->block("BLOCK_CONFIGURACOES");

}
?>