<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require ("../class/config.php");
require ("../class/Helpers/Template.php");

//Verifica se existe session do login
if(isset($_SESSION['LoginUser']) && isset($_SESSION['TipoUser'])){
	$LoginAdmin = new Login();
	$UserLogado = $LoginAdmin->CheckLogado($_SESSION['LoginUser'], $_SESSION['TipoUser']);
}else{
    echo "<script>window.location='".URL_ADMIN."/login.php';</script>";
    exit();
}

$SqlSistema = new Read;
$SqlSistema->ExeRead("sistema");

$TplView = new Template("view/index.html");

if(isset($_GET['Secao'])) {
    $Sec = $_GET['Secao'];
    $url = explode('/', $_GET['Secao']);
}else{
    $Sec = "";
    $url[0] = "";
}

if(($url[0] == "") || ($url[0] == "Inicial")) {
    require ("controller/home.php");
} else {
    require ("controller/secao.php");
}

$TplView->URL_BASE = URL_BASE;
$TplView->URL_ADMIN = URL_ADMIN;
$TplView->NOME_PROJETO = NOME_PROJETO;

$TplView->show();
?>