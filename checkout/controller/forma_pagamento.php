<?php
if (!empty($_POST)) {
    
    $Info_post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    
    //Verifica se todos os campos estão preenchidos e se o type_action veio preenchido
    if (empty($Info_post) || (!isset($Info_post['type_action']) || empty($Info_post['type_action']))){
        echo "<script>window.location='".URL_CHECKOUT."';</script>";
        exit();
    }

    $Prospeccao = new Read;
    if(isset($Info_post['identificador'])){
        $identificador = Valida::descriptografia_segura($Info_post['identificador']);
        $Prospeccao->ExeRead("prospeccao", "WHERE id = :id", "id={$identificador}");
        if (!$Prospeccao->GetResult()) {
            echo "<script>window.location='" . URL_CHECKOUT . "';</script>";
            exit();
        }
    } else {
        echo "<script>window.location='" . URL_CHECKOUT . "';</script>";
        exit();
    }

    $JsonResults = array();

    if (Valida::CheckAes($Info_post['type_action']) == "InfoPaymentSlip") {

        //Limpa e retira quaisquer meios de invasões por formulários
        $Info_post = array_map("strip_tags", $Info_post);
        $Info_post = array_map("trim", $Info_post);
       
        //Atribui informações
        $Info_post['identification_plan'] = $Prospeccao->GetResult()[0]['identificador_plano'];
        $Info_post['clientecod'] = $Prospeccao->GetResult()[0]['cod_cliente'];
        $Info_post['cod_vendedor'] = $Prospeccao->GetResult()[0]['cod_vendedor'];
        $Info_post['valor'] = $Prospeccao->GetResult()[0]['valor'];
        $Info_post['valor_assinatura'] = $Prospeccao->GetResult()[0]['valor_assinatura'];
        $Info_post['nome_plano'] = $Prospeccao->GetResult()[0]['nome_plano'];

        //Executa a class de assinaturas
        $BoletoFranquia = new BoletoFranquia();
        $BoletoFranquia->ExeAssinatura($Info_post);
        if($BoletoFranquia->GetResult()){
            $JsonResults['success'] = Mensagens::SetBoletoGerado();
            $JsonResults['urldir'] = $BoletoFranquia->UrlBoleto();
        }else{
            $JsonResults['error'] = $BoletoFranquia->GetError();
        }
        
        echo json_encode($JsonResults);
        exit();

    } elseif(Valida::CheckAes($Info_post['type_action']) == "InfoPaymentCard") {
    
        //Limpa e retira quaisquer meios de invasões por formulários
        $Info_post = array_map("strip_tags", $Info_post);
        $Info_post = array_map("trim", $Info_post);
       
        //Atribui informações
        $Info_post['identification_plan'] = $Prospeccao->GetResult()[0]['identificador_plano'];
        $Info_post['clientecod'] = $Prospeccao->GetResult()[0]['cod_cliente'];
        $Info_post['cod_vendedor'] = $Prospeccao->GetResult()[0]['cod_vendedor'];
        $Info_post['valor'] = $Prospeccao->GetResult()[0]['valor'];
        $Info_post['valor_assinatura'] = $Prospeccao->GetResult()[0]['valor_assinatura'];
        $Info_post['nome_plano'] = $Prospeccao->GetResult()[0]['nome_plano'];

        //Executa a class de assinaturas
        $CartaoFranquia = new CartaoFranquia();
        $CartaoFranquia->ExeAssinatura($Info_post);
        if ($CartaoFranquia->GetResult()) {
            $JsonResults['success'] = Mensagens::SetPagamentoCartao();
            $JsonResults['urldir'] = URL_CHECKOUT."/pagamento_realizado/".Valida::Base3($Info_post['clientecod']);
        } else {
            $JsonResults['error'] = $CartaoFranquia->GetError();
        }
        
        echo json_encode($JsonResults);
        exit();  
    } else {
        echo "<script>window.location='".URL_CHECKOUT."';</script>";
        exit();
    }
}

if (isset($url[1])) {
    $TplView->addFile("INCLUDE_PAGE", "view/forma_pagamento.html");
    
    $TplView->SELF = URL_CHECKOUT."/forma_pagamento";

    //Pega o item da URL para passar via POST
    $codigo_prospeccao = Valida::descriptografia_segura($url[1]);
    $Prospeccao = new Read;
    $Prospeccao->ExeRead("prospeccao", "WHERE id = :id", "id={$codigo_prospeccao}");

    if(!$Prospeccao->GetResult()){
        echo "<script>window.location='".URL_BASE."';</script>";
        exit();
    }

    $ValorFinal = $Prospeccao->GetResult()[0]['valor'];
    $ReadPlanos = new Read;
    $ReadPlanos->ExeRead("planos", "WHERE identificador = :identificador", "identificador={$Prospeccao->GetResult()[0]['identificador_plano']}");
        
    if(!$ReadPlanos->GetResult()){
        echo "<script>window.location='".URL_BASE."';</script>";
        exit();
    }

    $identificador_plano = $ReadPlanos->GetResult()[0]['identificador'];

    //Informações de titulo do plano e valor
    $TplView->TitleTop = $Prospeccao->GetResult()[0]['nome_plano'];
    $TplView->Identificador = $url[1];
    $TplView->type_action_card = Valida::GeraAes("InfoPaymentCard");
    $TplView->type_action_slip = Valida::GeraAes("InfoPaymentSlip");

    $TplView->ClassParcelas = "col-lg-3 col-md-9 col-sm-8 col-xs-12";

    for($i = 1; $i < (MAX_PARCELAS + 1); $i++){
        $TplView->NumeroParcelas = Valida::Base3($i);
        $TplView->ValorParcelado = $i."x de R$ ".number_format(($ValorFinal / $i), 2, ',', '.');
        $TplView->block("FOR_PARCELAMENTO");
    }
    $TplView->block("PGTO_PARCELADO");

    //Classe bootstrap com campo parcelamento
    $TplView->ClassCodSeguranca = "col-lg-6 col-md-4 col-sm-3 col-xs-12";
    $TplView->ClassParcelas = "";

    $TplView->SubTitleTop = "R$ ".number_format($ValorFinal, 2, ',', '.');

} else {
    echo "<script>window.location='".URL_CHECKOUT."';</script>";
    exit();
}