<?php
session_start();
require ("../../class/config.php");
$InforVal = new Valida();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$Info_post = filter_input_array(INPUT_POST, FILTER_DEFAULT);

//Verifica se todos os campos estão preenchidos e se o type_action veio preenchido
if (empty($Info_post) || (!isset($Info_post['type_action']) || empty($Info_post['type_action']))){
    echo "<script>window.location='".URL_CHECKOUT."';</script>";
    exit();
}

$JsonResults = array();

if($InforVal->CheckAes($Info_post['type_action']) == "InfoCadUser"){

    //Limpa e retira quaisquer meios de invasões por formulários
    $Info_post = array_map("strip_tags", $Info_post);
    $Info_post = array_map("trim", $Info_post);

    //Verifica se os termos foram aceitos
    if(!isset($Info_post['termos_accpt']) || empty($Info_post['termos_accpt']) || $Info_post['termos_accpt'] <> "sim") {
        $Info_post['termos_accpt'] = "nao";
    }else{
        $Info_post['termos_accpt'] = "sim";
    }

    //Cria a URL de pagamento
    $urldir_pgto = URL_CHECKOUT."/formas_pagamento/".$Info_post['identification_plan'];

    //Define origem do registro
    $Info_post['origem_criacao'] = 'site';

    //Cria a sessão com o cupom preenchido
    if($Info_post['cupom']){
        $_SESSION['cupom'] = Valida::Base3($Info_post['cupom']);
    }

    if($Info_post['cod_vendedor']){
        $_SESSION['cod_vendedor'] = Valida::Base3($Info_post['cod_vendedor']);
    }

    //Remove do post algumas informações
    unset($Info_post['type_action'], $Info_post['identification_plan'], $Info_post['cupom']);

    //Executa a class de cadastro de clientes
    $Clientes = new Clientes();
    $Clientes->ExeCadastro($Info_post);
    if($Clientes->GetResult()){
        $JsonResults['success'] = Mensagens::SetCadCliente();

        //Cria sessão com o código criptografado gerado pelo IUGU
        $_SESSION['clientecod'] = Valida::Base3($Clientes->GetIdIuguCliente());

        $JsonResults['urldir'] = $urldir_pgto;

        unset($Info_post);
    }else{
        $JsonResults['error'] = $Clientes->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

    
}elseif($InforVal->CheckAes($Info_post['type_action']) == "InfoPaymentSlip"){

    //Limpa e retira quaisquer meios de invasões por formulários
    $Info_post = array_map("strip_tags", $Info_post);
    $Info_post = array_map("trim", $Info_post);
   
    //Verifica se existe a sessão com o código do cliente e se o mesmo é válido
    if(isset($_SESSION) && empty($_SESSION['clientecod'])){
        echo "<script>window.location='" . URL_CHECKOUT . "';</script>";
        exit();
    }

    //Atribui informações
    $Info_post['clientecod'] = $_SESSION['clientecod'];
    $Info_post['cupom'] = (isset($_SESSION) && empty($_SESSION['cupom']) ? 0 : $_SESSION['cupom']);
    $Info_post['cod_revendedor'] = (isset($_SESSION) && empty($_SESSION['cod_revendedor']) ? 0 : $_SESSION['cod_revendedor']);
    $Info_post['cod_vendedor'] = (isset($_SESSION) && empty($_SESSION['cod_vendedor']) ? 0 : $_SESSION['cod_vendedor']);
    $Info_post['origem_criacao'] = 'site';

    //Limpa sessão de cupom de desconto caso exista
    if(!isset($_SESSION) && !empty($_SESSION['cupom'])){
        unset($_SESSION['cupom']);
    }

    //Limpa sessão de revendedor caso exista
    if(!isset($_SESSION) && !empty($_SESSION['cod_revendedor'])){
        unset($_SESSION['cod_revendedor']);
    }
    
    //Limpa sessão de vendedor caso exista
    if(!isset($_SESSION) && !empty($_SESSION['cod_vendedor'])){
        unset($_SESSION['cod_vendedor']);
    }
    
    //Executa a class de assinaturas
    $AssinaturaBoleto = new AssinaturaBoleto();
    $AssinaturaBoleto->ExeAssinatura($Info_post);
    if($AssinaturaBoleto->GetResult()){
        $JsonResults['success'] = Mensagens::SetBoletoGerado();
        $JsonResults['urldir'] = $AssinaturaBoleto->UrlBoleto();

        //Limpa sessão de identificação do cliente e todo o post
        unset($Info_post, $_SESSION['clientecod']);
    }else{
        $JsonResults['error'] = $AssinaturaBoleto->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

    
}elseif($InforVal->CheckAes($Info_post['type_action']) == "InfoPaymentCard"){

    //Limpa e retira quaisquer meios de invasões por formulários
    $Info_post = array_map("strip_tags", $Info_post);
    $Info_post = array_map("trim", $Info_post);
   
    //Verifica se existe a sessão com o código do cliente e se o mesmo é válido
    if(isset($_SESSION) && empty($_SESSION['clientecod'])){
        echo "<script>window.location='" . URL_CHECKOUT . "';</script>";
        exit();
    }

    //Atribui informações
    $Info_post['clientecod'] = $_SESSION['clientecod'];
    $Info_post['cupom'] = (isset($_SESSION) && empty($_SESSION['cupom']) ? 0 : $_SESSION['cupom']);
    $Info_post['cod_revendedor'] = (isset($_SESSION) && empty($_SESSION['cod_revendedor']) ? 0 : $_SESSION['cod_revendedor']);
    $Info_post['cod_vendedor'] = (isset($_SESSION) && empty($_SESSION['cod_vendedor']) ? 0 : $_SESSION['cod_vendedor']);
    $Info_post['origem_criacao'] = 'site';

    //Limpa sessão de cupom de desconto caso exista
    if(!isset($_SESSION) && !empty($_SESSION['cupom'])){
        unset($_SESSION['cupom']);
    }

    //Limpa sessão de revendedor caso exista
    if(!isset($_SESSION) && !empty($_SESSION['cod_revendedor'])){
        unset($_SESSION['cod_revendedor']);
    }

    //Limpa sessão de vendedor caso exista
    if(!isset($_SESSION) && !empty($_SESSION['cod_vendedor'])){
        unset($_SESSION['cod_vendedor']);
    }
    
    //Executa a class de assinaturas
    $AssinaturaCartao = new AssinaturaCartao();
    $AssinaturaCartao->ExeAssinatura($Info_post);
    if($AssinaturaCartao->GetResult()){
        $JsonResults['success'] = Mensagens::SetPagamentoCartao();
        $JsonResults['urldir'] = URL_CHECKOUT."/pagamento_realizado/".$Info_post['clientecod'];

        //Limpa sessão de identificação do cliente e todo o post
        unset($Info_post, $_SESSION['clientecod']);
    }else{
        $JsonResults['error'] = $AssinaturaCartao->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

    
}elseif($InforVal->CheckAes($Info_post['type_action']) == "PaymentSlipAvulso"){

    //Limpa e retira quaisquer meios de invasões por formulários
    $Info_post = array_map("strip_tags", $Info_post);
    $Info_post = array_map("trim", $Info_post);
   
    //Verifica se existe a sessão com o código do cliente e se o mesmo é válido
    if(isset($_SESSION) && empty($_SESSION['clientecod'])){
        echo "<script>window.location='" . URL_CHECKOUT . "';</script>";
        exit();
    }

    //Atribui informações
    $Info_post['clientecod'] = $_SESSION['clientecod'];
    $Info_post['cupom'] = (isset($_SESSION) && empty($_SESSION['cupom']) ? 0 : $_SESSION['cupom']);
    $Info_post['cod_revendedor'] = (isset($_SESSION) && empty($_SESSION['cod_revendedor']) ? 0 : $_SESSION['cod_revendedor']);
    $Info_post['cod_vendedor'] = (isset($_SESSION) && empty($_SESSION['cod_vendedor']) ? 0 : $_SESSION['cod_vendedor']);
    $Info_post['origem_criacao'] = 'site';

    //Limpa sessão de cupom de desconto caso exista
    if(!isset($_SESSION) && !empty($_SESSION['cupom'])){
        unset($_SESSION['cupom']);
    }

    //Limpa sessão de revendedor caso exista
    if(!isset($_SESSION) && !empty($_SESSION['cod_revendedor'])){
        unset($_SESSION['cod_revendedor']);
    }

    //Limpa sessão de revendedor caso exista
    if(!isset($_SESSION) && !empty($_SESSION['cod_vendedor'])){
        unset($_SESSION['cod_vendedor']);
    }
    
    //Executa a class de assinaturas
    $Fatura = new Fatura();
    $Fatura->ExeFatura($Info_post);
    if($Fatura->GetResult()){
        $JsonResults['success'] = Mensagens::SetBoletoGerado();
        $JsonResults['urldir'] = $Fatura->UrlBoleto();

        //Limpa sessão de identificação do cliente e todo o post
        unset($Info_post, $_SESSION['clientecod']);
    }else{
        $JsonResults['error'] = $Fatura->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

    
}elseif($InforVal->CheckAes($Info_post['type_action']) == "PaymentCardAvulso"){

    //Limpa e retira quaisquer meios de invasões por formulários
    $Info_post = array_map("strip_tags", $Info_post);
    $Info_post = array_map("trim", $Info_post);
   
    //Verifica se existe a sessão com o código do cliente e se o mesmo é válido
    if(isset($_SESSION) && empty($_SESSION['clientecod'])){
        echo "<script>window.location='" . URL_CHECKOUT . "';</script>";
        exit();
    }

    //Atribui informações
    $Info_post['clientecod'] = $_SESSION['clientecod'];
    $Info_post['cupom'] = (isset($_SESSION) && empty($_SESSION['cupom']) ? 0 : $_SESSION['cupom']);
    $Info_post['cod_revendedor'] = (isset($_SESSION) && empty($_SESSION['cod_revendedor']) ? 0 : $_SESSION['cod_revendedor']);
    $Info_post['cod_vendedor'] = (isset($_SESSION) && empty($_SESSION['cod_vendedor']) ? 0 : $_SESSION['cod_vendedor']);
    $Info_post['origem_criacao'] = 'site';

    //Limpa sessão de cupom de desconto caso exista
    if(!isset($_SESSION) && !empty($_SESSION['cupom'])){
        unset($_SESSION['cupom']);
    }

    //Limpa sessão de revendedor caso exista
    if(!isset($_SESSION) && !empty($_SESSION['cod_revendedor'])){
        unset($_SESSION['cod_revendedor']);
    }

    //Limpa sessão de vendedor caso exista
    if(!isset($_SESSION) && !empty($_SESSION['cod_vendedor'])){
        unset($_SESSION['cod_vendedor']);
    }
  
    //Executa a class de assinaturas
    $assinaturaCartao = new AssinaturaCartao();
    $assinaturaCartao->ExeAssinatura($Info_post);
    if($assinaturaCartao->GetResult()){
        $JsonResults['success'] = Mensagens::SetPagamentoCartao();
        $JsonResults['urldir'] = URL_CHECKOUT."/pagamento_realizado/".$Info_post['clientecod'];

        //Limpa sessão de identificação do cliente e todo o post
        unset($Info_post, $_SESSION['clientecod']);
    }else{
        $JsonResults['error'] = $assinaturaCartao->GetError();
    }
    echo json_encode($JsonResults);
    exit();

    
}elseif($InforVal->CheckAes($Info_post['type_action']) == "CompletarCadastro"){

    //Remove do post algumas informações
    unset($Info_post['type_action']);

    //Limpa e retira quaisquer meios de invasões por formulários
    $Info_post = array_map("strip_tags", $Info_post);
    $Info_post = array_map("trim", $Info_post);
    
    //Executa a class de assinaturas
    $CompletarCadastro = new CompletarCadastro();
    $CompletarCadastro->ExeAtualizacao($Info_post);
    if($CompletarCadastro->GetResult()){
        $JsonResults['success'] = Mensagens::SetSuccessCompletar();
        $JsonResults['urldir'] = URL_BASE;
    }else{
        $JsonResults['error'] = $CompletarCadastro->GetError();
    }

    //Limpa todo o post
    unset($Info_post);
    
    echo json_encode($JsonResults);
    exit();


} else {
    echo "<script>window.location='".URL_CHECKOUT."';</script>";
    exit();
}
?>