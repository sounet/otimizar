<?php
$TplView->addFile("INCLUDE_PAGE", "view/informacoes_pessoais.html");

//Pega o item da URL para passar via POST
$TplView->IdentUrl = $url[1];

//Limpa cupom da sessão
if(isset($_SESSION) && !empty($_SESSION['cupom'])) {
    unset($_SESSION['cupom']);
}

//Limpa codigo vendedor da sessão
if(isset($_SESSION) && !empty($_SESSION['cod_vendedor'])) {
    unset($_SESSION['cod_vendedor']);
}

//Verifica se o plano da URL é válido
$identificador_url = Valida::Rebase3($url[1]);
$ReadPlanos = new Read();
$ReadPlanos->ExeRead("planos", "WHERE identificador = :identificador", "identificador={$identificador_url}");
if(!$ReadPlanos->GetResult()){
    echo "<script>window.location='".URL_BASE."';</script>";
    exit();
}

//Informações de titulo do plano e valor
$TplView->TitleTop = $ReadPlanos->GetResult()[0]['titulo'];
$TplView->SubTitleTop = "R$ ".number_format($ReadPlanos->GetResult()[0]['valor'], 2, ',', '.');

//Leitura de tabela de origens das compras
$ReadOrigemCompra = new Read();
$ReadOrigemCompra->ExeRead("origem_compra", "ORDER BY id DESC");
if($ReadOrigemCompra->GetResult()){
    foreach($ReadOrigemCompra->GetResult() as $res_list){
        $TplView->titulo = $res_list['titulo'];
        $TplView->ident = Valida::Base3($res_list['id']);
        $TplView->block("FOREACH_ORIGENS");
    }
    $TplView->block("VERIFICA_ORIGENS");
}

//Direciona formulário para o arquivo post.php
$TplView->type_action = Valida::GeraAes("InfoCadUser");

//$TplView->block("INFORMACOES_PESSOAIS");
