<?php
$TplView->addFile("INCLUDE_PAGE", "view/formas_pagamento.html");

//Pega o item da URL para passar via POST
$TplView->IdentUrl = $url[1];

if(isset($_SESSION) && empty($_SESSION['clientecod'])){
    echo "<script>window.location='" . URL_CHECKOUT . "';</script>";
    exit();
}

//Verifica se o plano da URL é válido
$identificador_url = Valida::Rebase3($url[1]);
$ReadPlanos = new Read();
$ReadPlanos->ExeRead("planos", "WHERE identificador = :identificador", "identificador={$identificador_url}");
if(!$ReadPlanos->GetResult()){
    echo "<script>window.location='".URL_BASE."';</script>";
    exit();
}

//Informações de titulo do plano e valor
$TplView->TitleTop = $ReadPlanos->GetResult()[0]['titulo'];

//Verifica se existe um cupom de desconto
if($_SESSION && !empty($_SESSION['cupom'])){
    $Cupom = Valida::Rebase3($_SESSION['cupom']);
    $ReadCupom = new Read();
    $ReadCupom->ExeRead("cupons", "WHERE codigo = :codigo", "codigo={$Cupom}");
    if($ReadCupom->GetResult()){

        $ValorDesconto = ($ReadPlanos->GetResult()[0]['valor'] / 100) * $ReadCupom->GetResult()[0]['porcentagem'];
        $ValorFinal = ($ReadPlanos->GetResult()[0]['valor'] - $ValorDesconto);

    }else{

        $ValorFinal = $ReadPlanos->GetResult()[0]['valor'];

    }
}else{

    $ValorFinal = $ReadPlanos->GetResult()[0]['valor'];

}

//Verifica se o plano é ANUAL
//Caso seja anual direciona o formulário para um determinado bloco do post.php
//Caso não seja anual direciona para outro bloco do post.php
if($identificador_url == "anual_plan"){
    
    $TplView->type_action_card = Valida::GeraAes("PaymentCardAvulso");
    $TplView->type_action_slip = Valida::GeraAes("PaymentSlipAvulso");

    //Classe bottstrap com campo parcelamento
    $TplView->ClassCodSeguranca = "col-lg-3 col-md-9 col-sm-8 col-xs-12";
    $TplView->ClassParcelas = "col-lg-3 col-md-9 col-sm-8 col-xs-12";

    //Laço de parcelas
    for($i = 1; $i < (MAX_PARCELAS + 1); $i++){
        $TplView->NumeroParcelas = Valida::Base3($i);
        $TplView->ValorParcelado = $i."x de R$ ".number_format(($ValorFinal / $i), 2, ',', '.');
        $TplView->block("FOR_PARCELAMENTO");
    }
    $TplView->block("PGTO_PARCELADO");

}elseif($identificador_url == "semestral_plan"){

    $TplView->type_action_card = Valida::GeraAes("PaymentCardAvulso");
    $TplView->type_action_slip = Valida::GeraAes("PaymentSlipAvulso");

    //Classe bottstrap com campo parcelamento
    $TplView->ClassCodSeguranca = "col-lg-3 col-md-9 col-sm-8 col-xs-12";
    $TplView->ClassParcelas = "col-lg-3 col-md-9 col-sm-8 col-xs-12";

    //Laço de parcelas
    for($i = 1; $i < (MAX_PARCELAS + 1); $i++){
        $TplView->NumeroParcelas = Valida::Base3($i);
        $TplView->ValorParcelado = $i."x de R$ ".number_format(($ValorFinal / $i), 2, ',', '.');
        $TplView->block("FOR_PARCELAMENTO");
    }
    $TplView->block("PGTO_PARCELADO");

}else{

    $TplView->type_action_card = Valida::GeraAes("InfoPaymentCard");
    $TplView->type_action_slip = Valida::GeraAes("InfoPaymentSlip");

    $TplView->ClassParcelas = "col-lg-3 col-md-9 col-sm-8 col-xs-12";

    for($i = 1; $i < (MAX_PARCELAS + 1); $i++){
        $TplView->NumeroParcelas = Valida::Base3($i);
        $TplView->ValorParcelado = $i."x de R$ ".number_format(($ValorFinal / $i), 2, ',', '.');
        $TplView->block("FOR_PARCELAMENTO");
    }
    $TplView->block("PGTO_PARCELADO");

    //Classe bottstrap com campo parcelamento
    $TplView->ClassCodSeguranca = "col-lg-6 col-md-4 col-sm-3 col-xs-12";
    $TplView->ClassParcelas = "";

}

$TplView->SubTitleTop = "R$ ".number_format($ValorFinal, 2, ',', '.');
//$TplView->block("FORMAS_PAGAMENTO");