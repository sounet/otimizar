<?php
$TplView->addFile("INCLUDE_PAGE", "view/pagamento_realizado.html");
//Verifica se o cliente da URL é válido
$IdClienteUrl = Valida::Rebase3($url[1]);
$ReadCliente = new Read();
$ReadCliente->ExeRead("clientes", "WHERE cod_cliente = :cod_cliente", "cod_cliente={$IdClienteUrl}");

//Informações do cliente para o topo
$TplView->TitleTop = "Olá,";
$TplView->SubTitleTop = $ReadCliente->GetResult()[0]['nome'];

$TplView->block("PAGAMENTO_REALIZADO");