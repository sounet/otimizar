<?php
//Verifica se o cliente da URL é válido
$identificador_url = Valida::Rebase3($url[1]);
$ReadVerificacao = new Read();
$ReadVerificacao->FullRead("SELECT cli.nome, comp.plano FROM compras comp LEFT JOIN clientes cli ON comp.cod_cliente = cli.cod_cliente WHERE comp.cod_assinatura = :cod_assinatura", "cod_assinatura={$identificador_url}");
if(!$ReadVerificacao->GetResult()){
    echo "<script>window.location='".URL_BASE."';</script>";
    exit();
}

//Informações do cliente para o topo
$TplView->TitleTop = "Olá,";
$TplView->SubTitleTop = $ReadVerificacao->GetResult()[0]['nome'];

//Direciona formulário para o arquivo post.php
$TplView->type_action = Valida::GeraAes("CompletarCadastro");

//Carrega formulário de acordo com plano
switch ($ReadVerificacao->GetResult()[0]['plano']) {
    case 'otimizar_seo'     :
    case 'assinatura_seo'   :
        $include = 'cadastro_seo.html';
        break;
    
    case 'otimizar_adwords'     :
    case 'assinatura_adwords'   :
        $include = 'cadastro_adwords.html';
        break;
    
    case 'otimizar_combo'     :
    case 'assinatura_combo'   :
        $include = 'cadastro_combo.html';
        break;

    default:
        $include = "";
        break;
}
if ($include)
    $TplView->addFile("INCLUDE_PAGE", "view/{$include}");