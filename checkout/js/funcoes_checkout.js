$(document).ready(function(){
    //Submit informações pessoais
    $("#form_informacoes").submit(function(){
        var form = $(this);
        var data = $(this).serialize();
        var url_post = $(this).attr('action');

        $.ajax({
            url: url_post,
            data: data,
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                form.find('.return_form').fadeIn('fast').html("<img src='imagens/loader.gif' width='25' height='25' />");
            },
            success: function (retorno) {
                if (retorno.error) {
                    form.find('.return_form').html('<div class="error_form">' + retorno.error + '</div>');
                } else if (retorno.success) {
                    form.find('.return_form').html('<div class="success_form">' + retorno.success + '</div>');
                    setTimeout(function(){
                        window.location=retorno.urldir;
                    }, 2000); 
                }else{
                    alert("Houve algum problema!");
                }
            }
        });

        return false;
    });

    //Submit boleto
    $("#form_slip").submit(function(){
        if(confirm("Gerar boleto?")){
            var Form = $(this);
            var Data = Form.serialize();
            var url_post = $(this).attr('action');

            console.log(url_post);

            $.ajax({
                url: url_post,
                data: Data,
                type: 'POST',
                dataType: 'json',
                beforeSend: function(){
                    $('#send_boleto').attr('disabled', true);
                    $('.tab_boleto_show .return_form').fadeIn('fast').html("<img src='imagens/loader.gif' width='25' height='25' />");
                    console.log("passou");
                },
                success: function (retorno){
                    if (retorno.success){
                        console.log("sucesso");
                        $('#send_boleto').attr('disabled', true);
                        Form.find('.return_form').fadeIn('fast').html('<div class="success_form">' + retorno.success + '</div>');
                        setTimeout(function(){
                            window.location=retorno.urldir;
                        }, 2000);
                    }else if(retorno.error){
                        console.log("falha");
                        $('#send_boleto').attr('disabled', false);
                        Form.find('.return_form').fadeIn('fast').html('<div class="error_form">' + retorno.error + '</div>');
                    }else{
                        alert("Houve algum problema!");
                    }
                }
            });
        }
        return false;
    });

    //Submit cartão crédito
    $("#form_card").submit(function(){
        var Form = $(this);
        var Data = Form.serialize();
        var url_post = $(this).attr('action');

        console.log(url_post);
        console.log(Data);

        $.ajax({
            url: url_post,
            data: Data,
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {

                $('#send_card').attr('disabled', true);
                $('.tab_cartao_show .return_form').fadeIn().html("<img src='imagens/loader.gif' width='25' height='25' />");
            },
            success: function (data) {
                if(data.success){
                    $('#send_card').attr('disabled', true);
                    Form.find('.return_form').fadeIn('fast').html('<div class="success_form">' + data.success + '</div>');
                    setTimeout(function(){
                        window.location=data.urldir;
                    }, 2000);
                }else if(data.error){
                    $('#send_card').attr('disabled', false);
                    $('.tab_cartao_show .return_form').fadeIn().html('<div class="error_form">' + data.error + '</div>');
                }else{
                    $('#send_card').attr('disabled', false);
                    $('.tab_cartao_show .return_form').fadeIn().html('<div class="error_form">&#10008; Erro ao processar pagamento!<br>Você pode tentar novamente. Ou entre em contato conosco!</div>');
                }
            }
        });
        return false;
    });

    //Tab cartão
    $('.tab_cartao').click(function(){
        $('.tab_boleto').removeClass("ativa");
        $(this).addClass("ativa");
        $('.tab_boleto_show').fadeOut('fast', function(){
            $('.tab_cartao_show').fadeIn('fast');
        });
    });
    
    //Tab boleto
    $('.tab_boleto').click(function(){
        $('.tab_cartao').removeClass("ativa");
        $(this).addClass("ativa");
        $('.tab_cartao_show').fadeOut('fast', function(){
            $('.tab_boleto_show').fadeIn('fast');
        });
    });

    //Máscaras formulários
    $('#celular').mask("(99) 9999-9999?9");
    $("#cep").mask("99999-999");
    $("#cartao").mask("999999999999999?9");
    $("#data").mask("99/99/9999");
});

//Função para obter informações de acordo com o cep
function getEndereco() {
    if($.trim($("#cep").val()) != ""){
        var cep = $("#cep").val();
        $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
            if (!("erro" in dados)) {
                $("#endereco").val(dados.logradouro);
                $("#bairro").val(dados.bairro);
                $("#cidade").val(dados.localidade);
                $("#estado").val(dados.uf);
            }else{
                alert("Erro ao carregar informações!");
            }
        });
    }         
}

//Permite preencher somente número
function somente_numero(campo) {
    var digits = "0123456789"
    var campo_temp
    for (var i = 0; i < campo.value.length; i++) {
        campo_temp = campo.value.substring(i, i + 1)
        if (digits.indexOf(campo_temp) == -1) {
            campo.value = campo.value.substring(0, i);
        }
    }
}