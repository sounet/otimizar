<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require ("../class/config.php");
require ("../class/Helpers/Template.php");

$TplView = new Template("view/index.html");

if(isset($_GET['Secao'])) {
    $Sec = $_GET['Secao'];
    $url = explode('/', $_GET['Secao']);
}else{
    echo "<script>window.location='".URL_BASE."';</script>";
    exit();
}

$TplView->NOME_PROJETO = NOME_PROJETO;
$TplView->URL_BASE = URL_BASE;
$TplView->URL_CHECKOUT = URL_CHECKOUT;

//Verifica se tem algum item na URL
/*
if(!isset($url[1])){
    echo "<script>window.location='".URL_BASE."';</script>";
    exit();
}
*/
//Blocos a serem apresentados
$include_file = "controller/{$url[0]}.php";
if (file_exists($include_file)) {
    include($include_file);
} else {
    echo "<script>window.location='".URL_BASE."';</script>";
    exit();
}

$TplView->show();
?>