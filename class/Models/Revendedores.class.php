<?php
/**
 * <strong>Revendedores.class</strong>
 * Classe responsável por gerenciar os revendedores
 * Não é possível deletar um revendedor pois pode ter compras relacionadas, somente inativa-lo
 * @copyright (c) 2017, André Cristhian
 */
class Revendedores{
    
    private $Nome;
    private $Telefone;
    private $Cpf;
    private $Email;
    private $Senha;
    private $ConfSenha;
    private $Status;
    private $Level;
    private $CodRevendedor;
    private $IdInfo;
    private $Error;
    private $Result;
    
    public function ExeCadastro(array $UserData){
        //Limpa e retira quaisquer meios de invasões por formulários
        $UserData = array_map("strip_tags", $UserData);
        $UserData = array_map("trim", $UserData);
        
        $this->Nome = $UserData['nome'];
        $this->Telefone = $UserData['telefone'];
        $this->Cpf = $UserData['cpf'];
        $this->Email = $UserData['email'];
        $this->Senha = $UserData['senha'];
        $this->ConfSenha = $UserData['confirm_senha'];
        $this->Level = $UserData['level'];
        $this->CodRevendedor = Valida::GeraCodigo(15);
        $this->SetCadastro();
    }

    public function ExeAtualizacao(array $UserData){
        //Limpa e retira quaisquer meios de invasões por formulários
        $UserData = array_map("strip_tags", $UserData);
        $UserData = array_map("trim", $UserData);
        
        $this->Nome = $UserData['nome'];
        $this->Telefone = $UserData['telefone'];
        $this->Cpf = $UserData['cpf'];
        $this->Email = $UserData['email'];
        $this->Senha = $UserData['senha'];
        $this->ConfSenha = $UserData['confirm_senha'];
        $this->Status = $UserData['status'];
        $this->Level = $UserData['level'];
        $this->IdInfo = $UserData['idfinfo'];
        $this->SetAtualiza();
    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
    
    private function SetCadastro(){
        if(!$this->Nome || !$this->Email || !Valida::Email($this->Email) || !$this->Telefone || !$this->Cpf || !$this->Senha || !$this->ConfSenha){
            $this->Error = Mensagens::SetPreencAllCamps();
            $this->Result = false;
        }elseif($this->Senha <> $this->ConfSenha){
            $this->Error = Mensagens::SetPassConfError();
            $this->Result = false;
        }elseif($this->GetUserExist()){
            $this->Error = Mensagens::SetUserExist();
            $this->Result = false;
        }else{
            $this->RealizaCadastro();
        }
    }
    
    private function SetAtualiza(){
       if(!$this->Nome || !$this->Email || !Valida::Email($this->Email) || !$this->Telefone || !$this->Cpf){
            $this->Error = Mensagens::SetPreencAllCamps();
            $this->Result = false;
        }elseif($this->Senha && (!$this->ConfSenha || $this->Senha <> $this->ConfSenha)){
            $this->Error = Mensagens::SetPassConfError();
            $this->Result = false;
        }else{
            $this->RealizaAtualizacao();
        }
    }
    
    private function GetUserExist(){
        $Read = new Read();
        $Read->ExeRead("revendedores", "WHERE email = :email", "email={$this->Email}");
        if($Read->GetResult()){
            return true;
        }else{
            return false;
        }
    }
    
    private function RealizaCadastro(){
        //Gera senha criptgrafada
        $senhacry = Valida::GeraHash(12, Valida::GeraCodigo(22), $this->Senha);

        $Insere = new Create();
        $Insere->ExeCreate("revendedores", array("cod_revendedor" => $this->CodRevendedor, "nome" => $this->Nome, "email" => $this->Email, "telefone" => $this->Telefone, "cpf" => $this->Cpf, "status" => 2, "level" => $this->Level, "senha" => $senhacry));
        if($Insere->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
    
    private function RealizaAtualizacao(){
        //Gera senha criptgrafada se foi preenchida, caso contrário a senha continua a mesma
        if($this->ConfSenha){
            $senhacry = Valida::GeraHash(12, Valida::GeraCodigo(22), $this->Senha);
        }else{
            $Read = new Read();
            $Read->ExeRead("admin", "WHERE id = :id", "id={$this->IdInfo}");
            $senhacry = $Read->GetResult()[0]['senha'];
        }
        
        $Atualiza = new Update();
        $Atualiza->ExeUpdate("revendedores", array("nome" => $this->Nome, "email" => $this->Email, "telefone" => $this->Telefone, "cpf" => $this->Cpf, "status" => $this->Status, "level" => $this->Level, "senha" => $senhacry), "WHERE id = :id", "id={$this->IdInfo}");
        if($Atualiza->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
}