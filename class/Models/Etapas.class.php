<?php 

Class Etapas{
	private $codCompra;
	private $seo;
	private $ad;
	private $post;
	private $error;
	private $result;
	private $postData;

	public function getResult(){
		return $this->result;
	}

	public function getError(){
		return $this->error;
	}

	public function setPost($post){
		$this->postData = $post; 
	}

	//verifica qual pacote foi comprado
	private function getCompra(array $postData){
		$this->postData = $postData;
		$readCompra = new Read();
		$readCompra->ExeRead("compras", "WHERE id = :id", "id={$this->postData['IdCompra']}");
		if($readCompra->GetResult()){
			
		}else{

		}
	}

	//faz consulta, envia o e-mail e prepara para proxima etapa
	public function nextSeo(){
		try {
			$this->result = false;
			$readSeo = new Read();
			$readSeo->ExeRead("etapas", "WHERE cod_compra = :cod", "cod={$this->postData['IdCompra']}");
			$readTemplate = new Read();
			$readTemplate->ExeRead("seo", "WHERE etapa = :etapa AND tipo = 'seo'", "etapa={$this->postData['seo']}");

			if($readSeo->GetResult()){
				$updtSeo = new Update();
				$updtSeo->ExeUpdate("etapas", array("seo"=>$this->postData['seo']),"WHERE cod_compra =:cod","cod={$this->postData['IdCompra']}");
				
				if ($updtSeo->GetResult()) { $this->result = true; }

			}else{
				$createSeo = new Create();
				$createSeo->ExeCreate("etapas",array("cod_compra"=>$this->postData['IdCompra'],"seo"=>$this->postData['seo']));

				if ($createSeo->GetResult()) { $this->result = true; }

			}
			
			if ($this->result) { $this->sendMail($readTemplate->GetResult()[0]['email']); }

		} catch (Exception $e) {
			$this->result = false;
			echo "<p><strong>Erro ao mudar de etapa:</strong> {$e->getMessage()}. Codigo: <strong>{$e->getCode()}</strong></p>";
			exit();
		}
	}

	public function nextAd(){
		try {
			$this->result = false;
			$readSeo = new Read();
			$readSeo->ExeRead("etapas", "WHERE cod_compra = :cod", "cod={$this->postData['IdCompra']}");
			$readTemplate = new Read();
			$readTemplate->ExeRead("seo", "WHERE etapa = :etapa AND tipo = 'adword'", "etapa={$this->postData['adword']}");
			
			if($readSeo->GetResult()){
				$updtSeo = new Update();
				$updtSeo->ExeUpdate("etapas", array("ad"=>$this->postData['adword']),"WHERE cod_compra =:cod","cod={$this->postData['IdCompra']}");
				
				if ($updtSeo->GetResult()) { $this->result = true; }

			}else{
				$createSeo = new Create();
				$createSeo->ExeCreate("etapas",array("cod_compra"=>$this->postData['IdCompra'],"ad"=>$this->postData['adword']));
				
				if ($createSeo->GetResult()) { $this->result = true; }

			}

			if ($this->result) { $this->sendMail($readTemplate->GetResult()[0]['email']); }

		} catch (Exception $e) {
			$this->result = false;
			echo "<p><strong>Erro ao mudar de etapa:</strong> {$e->getMessage()}. Codigo: <strong>{$e->getCode()}</strong></p>";
			exit();
		}
	}

	public function sendMail($template_mail = NULL) {
		try {
			if ($template_mail){
				$ReadCompras = new Read();
				$ReadCompras->ExeRead("compras", "WHERE id = :id", "id={$this->postData['IdCompra']}");

				if ($ReadCompras->GetResult()){
					$ReadClientes = new Read();
					$ReadClientes->ExeRead("clientes", "WHERE cod_cliente = :cod", "cod={$ReadCompras->GetResult()[0]['cod_cliente']}");

					if ($ReadClientes->GetResult()) {

						//Envia e-mail da etapa para o cliente
						$ReadSistema = new Read();
						$ReadSistema->ExeRead("sistema");

						//Verifica se preencheu nome completo, se sim pega o primeiro nome
						$PrimeiroNome = explode(" ", $ReadClientes->GetResult()[0]['nome']);
						if($PrimeiroNome[0]){
							$PrimeiroNome = $PrimeiroNome[0];
						}else{
							$PrimeiroNome = $ReadClientes->GetResult()[0]['nome'];
						}

						$MsgEmail = file_get_contents(__DIR__."/../../Emails/{$template_mail}.html");
						$MsgEmail = str_replace ('%NomeUsuario', $PrimeiroNome, $MsgEmail);
						if ($this->postData['seo'] == 2)
							$MsgEmail = str_replace ('%idenAss', Valida::Base3($ReadCompras->GetResult()[0]['cod_assinatura']), $MsgEmail);

						switch ($this->postData['seo']) {
							case 1:
								$Assunto = 'Pedido realizado';
								break;
							case 2:
								$Assunto = 'Recebemos o seu pagamento';
								break;
							case 3:
								$Assunto = 'Otimização iniciada';
								break;
							case 17:
								$Assunto = 'A otimização do seu site foi finalizada!';
								break;						
							default:
								$Assunto = 'Acompanhe a otimização do seu site';
								break;
						}

						Valida::EnviarEmail($Assunto, $MsgEmail, $ReadSistema->GetResult()[0]['email_resposta'], NOME_PROJETO, $ReadClientes->GetResult()[0]['email'], $ReadClientes->GetResult()[0]['nome']);
					}
				}
			}
		} catch (Exception $ex) {
			$this->Result = null;
			echo "<p><strong>Erro ao enviar email:</strong> {$e->getMessage()}. Codigo: <strong>{$e->getCode()}</strong></p>";
			exit();
		}
	}
}
?>