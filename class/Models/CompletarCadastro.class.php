<?php
/**
 * <strong>CompletarCadastro.class</strong>
 * Classe responsável por completar cadastro dos clientes
 * @copyright (c) 2017, André Cristhian
 */
class CompletarCadastro{
    
    private $AD_Veiculacao;
    private $AD_Destaque;
    private $AD_Valor_Investido;
    private $AD_Publico_Alvo;
    private $AD_Clientes_Potencial;
    private $AD_Destaque_Inicial;
    private $AD_Orcamento;

    private $SEO_FTP_Host;
    private $SEO_FTP_Login;
    private $SEO_FTP_Senha;
    private $SEO_Site_Titulo;
    private $SEO_Plataforma_Login;
    private $SEO_Plataforma_Senha;

    private $Plano;
    private $Site_URL;
    private $Keywords;
    private $Area_Atuacao;
    private $Google_Login;
    private $Google_Senha;
    private $CodAssinatura;
    private $CodCliente;
    private $Error;
    private $Result;
    
    public function ExeAtualizacao(array $PostData){
        //CAMPOS EM COMUM
        $this->CodAssinatura            = Valida::Rebase3($PostData['identification_compra']);
        $this->Google_Login             = isset($PostData['google_login'          ])   ?   $PostData['google_login'           ]   :   ''   ;
        $this->Google_Senha             = isset($PostData['google_password'       ])   ?   $PostData['google_password'        ]   :   ''   ;
        $this->Area_Atuacao             = isset($PostData['site_atuacaoarea'      ])   ?   nl2br($PostData['site_atuacaoarea'])   :   ''   ;
        $this->Keywords                 = isset($PostData['site_keywords'         ])   ?   $PostData['site_keywords'          ]   :   ''   ;
        $this->Site_URL                 = isset($PostData['site_url'              ])   ?   $PostData['site_url'               ]   :   ''   ;
        
        //CAMPOS OTIMIZAR ADWORD
        $this->AD_Destaque              = isset($PostData['ad_destaque'           ])   ?   $PostData['ad_destaque'           ]   :   ''   ;
        $this->AD_Orcamento             = isset($PostData['ad_orcamento'          ])   ?   $PostData['ad_orcamento'          ]   :   ''   ;
        $this->AD_Veiculacao            = isset($PostData['ad_veiculacao'         ])   ?   $PostData['ad_veiculacao'         ]   :   ''   ;
        $this->AD_Publico_Alvo          = isset($PostData['ad_publico_alvo'       ])   ?   $PostData['ad_publico_alvo'       ]   :   ''   ;
        $this->AD_Valor_Investido       = isset($PostData['ad_valor_investido'    ])   ?   $PostData['ad_valor_investido'    ]   :   ''   ;
        $this->AD_Destaque_Inicial      = isset($PostData['ad_destaque_inicial'   ])   ?   $PostData['ad_destaque_inicial'   ]   :   ''   ;
        $this->AD_Clientes_Potencial    = isset($PostData['ad_clientes_potencial' ])   ?   $PostData['ad_clientes_potencial' ]   :   ''   ;

        //CAMPOS OTIMIZAR SEO
        $this->SEO_FTP_Host             = isset($PostData['seo_ftp_host'          ])   ?   $PostData['seo_ftp_host'                ]   :   ''   ;
        $this->SEO_FTP_Login            = isset($PostData['seo_ftp_login'         ])   ?   $PostData['seo_ftp_login'               ]   :   ''   ;
        $this->SEO_FTP_Senha            = isset($PostData['seo_ftp_password'      ])   ?   $PostData['seo_ftp_password'            ]   :   ''   ;
        $this->SEO_Site_Titulo          = isset($PostData['seo_site_titulo'       ])   ?   $PostData['seo_site_titulo'             ]   :   ''   ;
        $this->SEO_Plataforma_Login     = isset($PostData['seo_plat_login'        ])   ?   $PostData['seo_plat_login'              ]   :   ''   ;
        $this->SEO_Plataforma_Senha     = isset($PostData['seo_plat_password'     ])   ?   $PostData['seo_plat_password'           ]   :   ''   ;

        $this->RealizaAtualizacao();
    }
    
    private function SetValidacao() {
        //Verifica se o código da compra é válido
        $ReadCompra = new Read();
        $ReadCompra->ExeRead("compras", "WHERE cod_assinatura = :cod_assinatura", "cod_assinatura={$this->CodAssinatura}");

        if ($ReadCompra->GetResult()) {
            $this->Plano = $ReadCompra->GetResult()[0]['plano'];
            $this->CodCliente = $ReadCompra->GetResult()[0]['cod_cliente'];
            return $this->ValidarFormulario();
        } else {
            return false;
        }

    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
    
    private function RealizaAtualizacao(){
        if($this->SetValidacao()){

            //Verifica se o usuário já preencheu alguma vez, se sim somente atualiza informações, se não realiza novo cadastro
            $ReadSegmentacao = new Read();
            $ReadSegmentacao->ExeRead("segmetacao", "WHERE cod_cliente = :cod_cliente AND cod_assinatura = :cod_assinatura", "cod_cliente={$this->CodCliente}&cod_assinatura={$this->CodAssinatura}");

            $Dados_Segmentacao = $this->GetArrayData();

            if ($ReadSegmentacao->GetResult()) {

                $AtualizaSegmentacao = new Update();
                $AtualizaSegmentacao
                    ->ExeUpdate(
                        "segmetacao",

                        $Dados_Segmentacao,

                        "WHERE cod_cliente = :cod_cliente AND cod_assinatura = :cod_assinatura",

                        "cod_cliente={$this->CodCliente}&cod_assinatura={$this->CodAssinatura}" 
                    );

                $this->Result = true;
                
            } else {
                $InsereSegmentacao = new Create();
                $InsereSegmentacao
                    ->ExeCreate(
                        'segmetacao',

                        $Dados_Segmentacao
                    );

                if ($InsereSegmentacao->GetResult()) {

                    //Verifica qual cliente completou o cadastro
                    $ReadCliente = new Read();
                    $ReadCliente->ExeRead("clientes", "WHERE cod_cliente = :cod_cliente", "cod_cliente={$this->CodCliente}");

                    //Envia e-mail da etapa para o cliente
                    $ReadSistema = new Read();
                    $ReadSistema->ExeRead("sistema");

                    //Envia e-mail para administração informando que o formulário foi preenchido
                    $msg_email = '<div style="font-family:Calibri, Arial; color:#666;">';
                    $msg_email .= '<p style="color:#333; font-size:20px;">Olá <strong>administrador</strong>,</p>';
                    $msg_email .= '<br>';
                    $msg_email .= '<p style="font-size:18px;">O cadastro de <strong>'.$ReadCliente->GetResult()[0]['nome'].' - '.$this->SEO_Site_Titulo.'</strong> foi atualizado, verifique por favor no painel administrativo!</p>';
                    $msg_email .= '<br>';
                    $msg_email .= '<p style="font-size:18px;"><strong>Em:</strong> '.date("d/m/Y H:i").'</p>';
                    $msg_email .= '<br><br>';
                    $msg_email .= '</div>';

                    Valida::EnviarEmail('Cadastro atualizado - '.$this->SEO_Site_Titulo.'', $msg_email, $ReadSistema->GetResult()[0]['email_resposta'], NOME_PROJETO, 'jean@sounet.digital', NOME_PROJETO);

                    $this->Result = true;

                }else{

                    $this->Error = Mensagens::SetErrorProcess();
                    $this->Result = false;

                }

            }

        }
    }

    private function GetArrayData() {
        $Data_Array = array_merge( $this->GetArrayDataSEO(), $this->GetArrayDataAdwords() );
        $Site_URL = str_replace('https:', '', $this->Site_URL);
        $Site_URL = str_replace('http:' , '', $this->Site_URL);
        $Data_Array['site_url']         = $Site_URL;
        $Data_Array['keywords']         = $this->Keywords; 
        $Data_Array['google_login']     = $this->Google_Login; 
        $Data_Array['google_senha']     = $this->Google_Senha;
        $Data_Array['area_atuacao']     = $this->Area_Atuacao;
        $Data_Array['cod_cliente']      = $this->CodCliente; 
        $Data_Array['cod_assinatura']   = $this->CodAssinatura;
        $Data_Array['data']             = date("Y-m-d H:i:s");

        //Expiração de senha em 10 dias
        $Data_Array['exp_pass']         = time() + (((60 * 60) * 10) * 24);

        return $Data_Array;
    }

    private function GetArrayDataSEO() {
        return array(
            "site_titulo"       =>  $this->SEO_Site_Titulo, 
            "ftp_host"          =>  $this->SEO_FTP_Host, 
            "ftp_login"         =>  $this->SEO_FTP_Login, 
            "ftp_senha"         =>  $this->SEO_FTP_Senha, 
            "plataforma_login"  =>  $this->SEO_Plataforma_Login, 
            "plataforma_senha"  =>  $this->SEO_Plataforma_Senha, 
        );
    }

    private function GetArrayDataAdwords() {
        return array(
            "destaque"           =>  $this->AD_Destaque,
            "orcamento"          =>  $this->AD_Orcamento,
            "veiculacao"         =>  $this->AD_Veiculacao,
            "publico_alvo"       =>  $this->AD_Publico_Alvo,
            "valor_investido"    =>  $this->AD_Valor_Investido,
            "destaque_inicial"   =>  $this->AD_Destaque_Inicial,
            "clientes_potencial" =>  $this->AD_Clientes_Potencial
        );
    }

    private function ValidarFormulario() {
        switch ($this->Plano) {
            case 'otimizar_seo':
            case 'assinatura_seo':
                return $this->ValidarSEO();
                break;

            case 'otimizar_adwords':
            case 'assinatura_adwords':
                return $this->ValidarAdwords();
                break;

            case 'otimizar_combo':
            case 'assinatura_combo':
                return $this->ValidarCombo();
                break;

            default:
                $this->Error = Mensagens::SetPreencAllCamps();
                $this->Result = false;
                return false;
                break;
        }
    }

    private function ValidarSEO() {
        if (!$this->SEO_Site_Titulo) {
            $this->Error = Mensagens::SetErrorSiteTitulo();
            $this->Result = false;
            return false;

        } elseif (!$this->Site_URL) {
            $this->Error = Mensagens::SetErrorSiteURL();
            $this->Result = false;
            return false;

        } elseif (!$this->Area_Atuacao) {
            $this->Error = Mensagens::SetErrorAreaAtuacao();
            $this->Result = false;
            return false;

        } elseif (!$this->Keywords) {
            $this->Error = Mensagens::SetErrorKeywords();
            $this->Result = false;
            return false;

        } elseif (!$this->SEO_FTP_Host) {
            $this->Error = Mensagens::SetErrorFtpHost();
            $this->Result = false;
            return false;

        } elseif (!$this->SEO_FTP_Login) {
            $this->Error = Mensagens::SetErrorFtpLogin();
            $this->Result = false;
            return false;

        } elseif (!$this->SEO_FTP_Senha) {
            $this->Error = Mensagens::SetErrorFtpPassword();
            $this->Result = false;
            return false;

        } elseif (!$this->SEO_Plataforma_Login) {
            $this->Error = Mensagens::SetErrorPlataformaLogin();
            $this->Result = false;
            return false;

        } elseif (!$this->SEO_Plataforma_Senha) {
            $this->Error = Mensagens::SetErrorPlataformaSenha();
            $this->Result = false;
            return false;

        } elseif (!$this->Google_Login) {
            $this->Error = Mensagens::SetErrorGoogleLogin();
            $this->Result = false;
            return false;

        } elseif (!$this->Google_Senha) {
            $this->Error = Mensagens::SetErrorGoogleSenha();
            $this->Result = false;
            return false;
        } else {
            return true;
        }
    }

    private function ValidarAdwords() {
        if (!$this->Site_URL) {
            $this->Error = Mensagens::SetErrorSiteAnunciado();
            $this->Result = false;
            return false;

        } elseif (!$this->Area_Atuacao) {
            $this->Error = Mensagens::SetErrorAreaAtuacao();
            $this->Result = false;
            return false;

        } elseif (!$this->Keywords) {
            $this->Error = Mensagens::SetErrorKeywords();
            $this->Result = false;
            return false;

        } elseif (!$this->Google_Login) {
            $this->Error = Mensagens::SetErrorGoogleLogin();
            $this->Result = false;
            return false;

        } elseif (!$this->Google_Senha) {
            $this->Error = Mensagens::SetErrorGoogleSenha();
            $this->Result = false;
            return false;

        } elseif (!$this->AD_Veiculacao) {
            $this->Error = Mensagens::SetErrorFormaVeiculacao();
            $this->Result = false;
            return false;

        } elseif (!$this->AD_Destaque) {
            $this->Error = Mensagens::SetErrorFraseDestaque();
            $this->Result = false;
            return false;

        } elseif (!$this->AD_Valor_Investido) {
            $this->Error = Mensagens::SetErrorValorInvestido();
            $this->Result = false;
            return false;

        } elseif (!$this->AD_Publico_Alvo) {
            $this->Error = Mensagens::SetErrorPublicoAlvo();
            $this->Result = false;
            return false;

        } elseif (!$this->AD_Clientes_Potencial) {
            $this->Error = Mensagens::SetErrorClientePotencial();
            $this->Result = false;
            return false;

        } elseif (!$this->AD_Destaque_Inicial) {
            $this->Error = Mensagens::SetErrorDestaqueInicial();
            $this->Result = false;
            return false;

        } elseif (!$this->AD_Orcamento) {
            $this->Error = Mensagens::SetErrorOrcamentoBase();
            $this->Result = false;
            return false;

        } else {
            return true;
        }
    }

    private function ValidarCombo() {
        if (!$this->SEO_Site_Titulo) {
            $this->Error = Mensagens::SetErrorSiteTitulo();
            $this->Result = false;
            return false;

        } elseif (!$this->Site_URL) {
            $this->Error = Mensagens::SetErrorSiteURL();
            $this->Result = false;
            return false;

        } elseif (!$this->Area_Atuacao) {
            $this->Error = Mensagens::SetErrorAreaAtuacao();
            $this->Result = false;
            return false;

        } elseif (!$this->Keywords) {
            $this->Error = Mensagens::SetErrorKeywords();
            $this->Result = false;
            return false;

        } elseif (!$this->Google_Login) {
            $this->Error = Mensagens::SetErrorGoogleLogin();
            $this->Result = false;
            return false;

        } elseif (!$this->Google_Senha) {
            $this->Error = Mensagens::SetErrorGoogleSenha();
            $this->Result = false;
            return false;

        } elseif (!$this->SEO_FTP_Host) {
            $this->Error = Mensagens::SetErrorFtpHost();
            $this->Result = false;
            return false;

        } elseif (!$this->SEO_FTP_Login) {
            $this->Error = Mensagens::SetErrorFtpLogin();
            $this->Result = false;
            return false;

        } elseif (!$this->SEO_FTP_Senha) {
            $this->Error = Mensagens::SetErrorFtpPassword();
            $this->Result = false;
            return false;

        } elseif (!$this->SEO_Plataforma_Login) {
            $this->Error = Mensagens::SetErrorPlataformaLogin();
            $this->Result = false;
            return false;

        } elseif (!$this->SEO_Plataforma_Senha) {
            $this->Error = Mensagens::SetErrorPlataformaSenha();
            $this->Result = false;
            return false;

        } elseif (!$this->AD_Veiculacao) {
            $this->Error = Mensagens::SetErrorFormaVeiculacao();
            $this->Result = false;
            return false;

        } elseif (!$this->AD_Destaque) {
            $this->Error = Mensagens::SetErrorFraseDestaque();
            $this->Result = false;
            return false;

        } elseif (!$this->AD_Valor_Investido) {
            $this->Error = Mensagens::SetErrorValorInvestido();
            $this->Result = false;
            return false;

        } elseif (!$this->AD_Publico_Alvo) {
            $this->Error = Mensagens::SetErrorPublicoAlvo();
            $this->Result = false;
            return false;

        } elseif (!$this->AD_Clientes_Potencial) {
            $this->Error = Mensagens::SetErrorClientePotencial();
            $this->Result = false;
            return false;

        } elseif (!$this->AD_Destaque_Inicial) {
            $this->Error = Mensagens::SetErrorDestaqueInicial();
            $this->Result = false;
            return false;

        } elseif (!$this->AD_Orcamento) {
            $this->Error = Mensagens::SetErrorOrcamentoBase();
            $this->Result = false;
            return false;

        } else {
            return true;
        }
    }

}