<?php
/**
 * <strong>TextosPaginas.class</strong>
 * Classe responsável por gerenciar páginas com descrições
 * @copyright (c) 2017, André Cristhian
 */
class TextosPaginas{
    
    private $Descricao;
    private $PaginaAtual;
    private $Error;
    private $Result;

    public function ExeAtualizacao(array $UserData){
        $this->Descricao = $UserData['descricao'];
        $this->PaginaAtual = Valida::Rebase3($UserData['pag_form']);
        $this->SetAtualiza();
    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
    
    private function SetAtualiza(){
        if(!$this->Descricao || !$this->PaginaAtual){
            $this->Error = Mensagens::SetPreencAllCamps();
            $this->Result = false;
        }else{
            $this->RealizaAtualizacao();
        }
    }
    
    private function RealizaAtualizacao(){
        $Atualiza = new Update();
        $Atualiza->ExeUpdate("textos", array("descricao" => $this->Descricao), "WHERE pg = :pg", "pg={$this->PaginaAtual}");
        if($Atualiza->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
}