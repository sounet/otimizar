<?php
/**
 * <strong>BoletoFranquia.class</strong>
 * Classe responsável por gerenciar assinaturas por boletos
 */
class BoletoFranquia{
    
    private $CodPlano;
    private $CodCliente;
    private $NomeCliente;
    private $EmailCliente;
    private $CodVendedor;
    private $CodRevendedor;
    private $ValorPlano;
    private $ValorAssinatura;
    private $DescricaoPlano;
    private $OrigemCriacao;
    private $VendedorValido;
    private $Error;
    private $UrlBoleto;
    private $Result;
    
    public function ExeAssinatura(array $PostData){
        $this->CodPlano = $PostData['identification_plan'];
        $this->CodCliente = $PostData['clientecod'];
        $this->CodRevendedor = 0;
        $this->CodVendedor = $PostData['cod_vendedor'];
        $this->ValorPlano = $PostData['valor'];
        $this->DescricaoPlano = $PostData['nome_plano'];
        $this->ValorAssinatura = $PostData['valor_assinatura'];
        $this->OrigemCriacao = 'prospeccao';

        $this->RealizaAssinatura();
    }
    
    private function SetValidacao(){
        if (!$this->VendedorValido) {
            echo "<script>window.location='".URL_CHECKOUT."';</script>";
            exit();
        } else {
            $Gatilho = new Gatilho();
            $Gatilho->setEndpoint(ENDPOINT_API);
            
            //Verifica se o código do plano é válido
            $Gatilho->Request('POST', array('type_action' => PLANOS, 'identificador' => $this->CodPlano));
            $plano = json_decode($Gatilho->GetResult()[0]);
            
            if (empty($plano->data)) {
                echo "<script>window.location='".URL_CHECKOUT."';</script>";
                exit();
            }

            //Verifica se o código do cliente é valido
            $Gatilho->Request('post', array(
                'type_action' => CLIENTES, 
                'where' => array(
                    'cod_cliente'   => $this->CodCliente,
                    'iugu'          => 1
                )
            ));
            $cliente = json_decode($Gatilho->GetResult()[0]);
            
            if (empty($cliente->data)) {
                echo "<script>window.location='".URL_CHECKOUT."';</script>";
                exit();
            }

            $cod_plano = $plano->data->codigo;
            $nome_cliente = $cliente->data->nome;
            $email_cliente = $cliente->data->email;
        }

        //Verifica se o código do cliente e o código do plano bate com as informações vindas do IUGU
        $ClienteIugu = Iugu_Customer::fetch($this->CodCliente);
        $PlanoIugu = Iugu_Plan::fetch($cod_plano);
        if(!$ClienteIugu->id || !$PlanoIugu->id){
            echo "<script>window.location='".URL_CHECKOUT."';</script>";
            exit();
        }

        //Atribui alguns outros valores
        $this->NomeCliente = $nome_cliente;
        $this->EmailCliente = $email_cliente;

        return true;
    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
    
    public function UrlBoleto(){
        return $this->UrlBoleto;
    }
    
    private function RealizaAssinatura(){

        //Verifica código do vendedor e aplica token de envio
        if (isset($this->CodVendedor) && !empty($this->CodVendedor)) {
            $Gatilho = new Gatilho();
            $Gatilho->setEndpoint(ENDPOINT_API);
            $Gatilho->Request('POST', array('type_action' => VENDEDOR, 'cod_vendedor' => $this->CodVendedor));
            $result = json_decode($Gatilho->GetResult()[0]);
            $this->VendedorValido = (!empty($result->data)) ? true : false ;
        }

        //Interação com o Iugu
        Iugu::setApiKey(TOKEN_IUGU_SOUNET);

        if($this->SetValidacao()){

            try {
                $subitems = array(
                    array(
                        "description" => 'Desconto - Assinatura',
                        "price_cents" => number_format(($this->ValorAssinatura * -1), 2, '', '.'),
                        "quantity" => 1,
                        "recurrent" => false
                    ),
                    array(
                        "description" => "Setup - {$this->DescricaoPlano}",
                        "price_cents" => str_replace(".", "", $this->ValorPlano),
                        "quantity" => 1,
                        "recurrent" => false
                    )
                );

                $AssinaturaCliente = Iugu_Subscription::create(Array(
                    "plan_identifier" => $this->CodPlano,
                    "customer_id" => $this->CodCliente,
                    "payable_with" => "bank_slip",
                    "expires_at" => date("Y-m-d", strtotime("+1 days")),
                    "credits_based" => false,
                    "subitems" => $subitems
                ));

                $id_fatura = "";
                $status_fatura = "";
                $valor_fatura = "";
                $url_fatura = "";
                foreach($AssinaturaCliente->recent_invoices as $recent_invoices){
                    $id_fatura = $recent_invoices->id;
                    $status_fatura = $recent_invoices->status;
                    $valor_fatura = $recent_invoices->total;
                    $url_fatura = $recent_invoices->secure_url;
                }
                
                //Cadastra a assinatura no banco de dados
                $array_data = array(
                    "cod_assinatura" => $AssinaturaCliente->id,
                    "cod_cliente" => $AssinaturaCliente->customer_id,
                    "cod_fatura" => $id_fatura,
                    "plano" => $AssinaturaCliente->plan_identifier,
                    "metodo" => "bank_slip",
                    "valor" => str_replace(array(".", ",", " ", "R$"), array("", ".", "", ""), $valor_fatura),
                    "renovacao" => 1, //1 - não 2 - sim
                    "status_pgto" => $status_fatura,
                    "revendedor" => $this->CodRevendedor,
                    "cod_vendedor" => ($this->VendedorValido) ? $this->CodVendedor : 0,
                    "origem_criacao" => $this->OrigemCriacao,
                    "dias_adicionados" => 2,
                    "data" => date("Y-m-d H:i:s")
                );
                $Insere = new Create();
                $Insere->ExeCreate("compras", $array_data);

                //Verifica se cadastrou o cliente no banco de dados
                if($Insere->GetResult()){

                    //Cadastra informações no banco de dados SOUNET através do Gatilho
                    $array_data['id_compra'] = $Insere->GetResult();
                    $array_data['type_action'] = COMPRAS;
                    $array_data['origem_compra'] = ORIGEM_COMPRA;
                    $Gatilho = new Gatilho();
                    $Gatilho->Request('post', $array_data);

                    $ReadSistema = new Read();
                    $ReadSistema->ExeRead("sistema");

                    //Verifica se preencheu nome completo, se sim pega o primeiro nome
                    $PrimeiroNome = explode(" ", $this->NomeCliente);
                    if($PrimeiroNome[0]){
                        $PrimeiroNome = $PrimeiroNome[0];
                    }else{
                        $PrimeiroNome = $this->NomeCliente;
                    }

                    //Envia e-mail de pedido realizado para o cliente
                    $MsgEmail = file_get_contents(__DIR__."/../../Emails/pedido_realizado.html");
                    $MsgEmail = str_replace ('%NomeUsuario', $PrimeiroNome, $MsgEmail);
                    Valida::EnviarEmail("Pedido realizado", $MsgEmail, $ReadSistema->GetResult()[0]['email_resposta'], NOME_PROJETO, $this->EmailCliente, $this->NomeCliente);

                    $this->Result = true;
                    $this->UrlBoleto = $url_fatura;

                }else{
                    $this->Error = Mensagens::SetErrorProcess();
                    $this->Result = false;
                }
            } catch (Exception $e) {
                $this->Error = Mensagens::SetErrorProcess();
                $this->Result = false;
            }
        }
    }
}