<?php
/**
 * <strong>Planos.class</strong>
 * Classe responsável por gerenciar os planos de assinaturas
 * @copyright (c) 2017, André Cristhian
 */
class Planos{
    
    private $Valor;
    private $IdInfo;
    private $Error;
    private $Result;

    public function ExeAtualizacao(array $PostData){
        //Limpa e retira quaisquer meios de invasões por formulários
        $PostData = array_map("strip_tags", $PostData);
        $PostData = array_map("trim", $PostData);
        
        $this->Valor = $PostData['valor'];
        $this->IdInfo = $PostData['idfinfo'];
        $this->SetAtualiza();
    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
    
    private function SetAtualiza(){
        if(!$this->Valor){
            $this->Error = Mensagens::SetPreencAllCamps();
            $this->Result = false;
        }else{
            $this->RealizaAtualizacao();
        }
    }
    
    private function RealizaAtualizacao(){
        $Atualiza = new Update();
        $Atualiza->ExeUpdate("planos", array("valor" => $this->Valor), "WHERE id = :id", "id={$this->IdInfo}");
        if($Atualiza->GetResult()){

            //Interação com o Iugu
            //Realizar alteração de plano no IUGU
            $ReadPlano = new Read();
            $ReadPlano->ExeRead("planos", "WHERE id = :id", "id={$this->IdInfo}");
            if($ReadPlano->GetResult()[0]['identificador'] <> "anual_plan"){
                Iugu::setApiKey(TOKEN_IUGU);
                $InfoSet = Iugu_Plan::fetch($ReadPlano->GetResult()[0]['codigo']);
                $InfoSet->value_cents = str_replace(".", "", $this->Valor);
                $InfoSet->save();
            }

            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
}