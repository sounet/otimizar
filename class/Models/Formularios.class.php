<?php
/**
 * <strong>Formularios.class</strong>
 * Classe responsável por fazer o envio dos formulários, contato, dúvidas, etc...
 * @copyright (c) 2017, André Cristhian
 */
class Formularios{

    private $Error;
    private $Result;
    
    public function Agencias(array $PostData){

        $ReadSistema = new Read();
        $ReadSistema->ExeRead("sistema");

        if(!$PostData['nome_responsavel'] || !$PostData['nome_agencia'] || !$PostData['email'] || !$PostData['celular'] || !$PostData['cnpj_agencia'] || !$PostData['observacoes']){
            $this->Error = Mensagens::SetPreencAllCamps();
            $this->Result = false;
        }elseif(!Valida::Email($PostData['email'])){
            $this->Error = Mensagens::SetErrorEmailValido();
            $this->Result = false;
        }else{

            //Envia e-mail para administração informando que o formulário foi preenchido
            $msg_email = '<div style="font-family:Calibri, Arial; color:#666;">';
            $msg_email .= '<p style="color:#333; font-size:20px;">Olá <strong>administrador</strong>,</p>';
            $msg_email .= '<br>';
            $msg_email .= '<p style="font-size:18px;">Nova agência com interesse:</p>';
            $msg_email .= '<p style="font-size:18px;"><strong>Nome da agência:</strong> '.$PostData['nome_agencia'].'</p>';
            $msg_email .= '<p style="font-size:18px;"><strong>Nome do responsável:</strong> '.$PostData['nome_responsavel'].'</p>';
            $msg_email .= '<p style="font-size:18px;"><strong>Telefone:</strong> '.$PostData['celular'].'</p>';
            $msg_email .= '<p style="font-size:18px;"><strong>E-mail:</strong> '.$PostData['email'].'</p>';
            $msg_email .= '<p style="font-size:18px;"><strong>CNPJ:</strong> '.$PostData['cnpj_agencia'].'</p>';
            $msg_email .= '<p style="font-size:18px;"><strong>Número de clientes:</strong> '.$PostData['numero_Clientes'].'</p>';
            $msg_email .= '<br>';
            $msg_email .= '<p style="font-size:18px;"><strong>Observações:</strong> '.$PostData['observacoes'].'</p>';
            $msg_email .= '<br>';
            $msg_email .= '<p style="font-size:18px;"><strong>Em:</strong> '.date("d/m/Y H:i").'</p>';
            $msg_email .= '<br><br>';
            $msg_email .= '</div>';
            $SendMail = Valida::EnviarEmail('Agência - Interesse', $msg_email, $PostData['email'], $PostData['nome_agencia'], "administrativo3@munditoysinflaveis.com.br", NOME_PROJETO);
            if($SendMail){
                $this->Result = true;
            }else{
                $this->Error = Mensagens::SetErrorSendMail();
                $this->Result = false;
            }

        }
    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
}