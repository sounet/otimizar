<?php
/**
 * <strong>ConfiguracoesAdmin.class</strong>
 * Classe responsável por gerenciar configurações do sistema
 * @copyright (c) 2017, André Cristhian
 */
class ConfiguracoesAdmin{
    
    private $EmailPrincipal;
    private $EmailResposta;
    private $TelefoneVendas;
    private $TelefoneSuporte;
    private $DescontoRecorrencias;
    private $Error;
    private $Result;

    public function ExeAtualizacao(array $UserData){
        //Limpa e retira quaisquer meios de invasões por formulários
        $UserData = array_map("strip_tags", $UserData);
        $UserData = array_map("trim", $UserData);
        
        $this->EmailPrincipal = $UserData['email_principal'];
        $this->EmailResposta = $UserData['email_resposta'];
        $this->TelefoneVendas = $UserData['vendas'];
        $this->TelefoneSuporte = $UserData['suporte'];
        $this->SetAtualiza();
    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
    
    private function SetAtualiza(){
        if(!$this->EmailPrincipal || !$this->EmailResposta || !$this->TelefoneVendas || !$this->TelefoneSuporte){
            $this->Error = Mensagens::SetPreencAllCamps();
            $this->Result = false;
        }else{
            $this->RealizaAtualizacao();
        }
    }
    
    private function RealizaAtualizacao(){
        $Atualiza = new Update();
        $Atualiza->ExeUpdate("sistema", array("email_principal" => $this->EmailPrincipal, "email_resposta" => $this->EmailResposta, "vendas" => $this->TelefoneVendas, "suporte" => $this->TelefoneSuporte), "WHERE id = :id", "id=1");
        if($Atualiza->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
}