<?php
/**
 * <strong>Cupons.class</strong>
 * Classe responsável por gerenciar cupons de descontos
 * @copyright (c) 2017, André Cristhian
 */
class Cupons{
    
    private $Codigo;
    private $Porcentagem;
    private $IdInfo;
    private $Error;
    private $Result;
    
    public function ExeCadastro(array $PostData){
        //Limpa e retira quaisquer meios de invasões por formulários
        $PostData = array_map("strip_tags", $PostData);
        $PostData = array_map("trim", $PostData);
        
        $this->Codigo = $PostData['codigo'];
        $this->Porcentagem = $PostData['porcentagem'];
        $this->SetCadastro();
    }

    public function ExeAtualizacao(array $PostData){
        //Limpa e retira quaisquer meios de invasões por formulários
        $PostData = array_map("strip_tags", $PostData);
        $PostData = array_map("trim", $PostData);
        
        $this->Codigo = $PostData['codigo'];
        $this->Porcentagem = $PostData['porcentagem'];
        $this->IdInfo = $PostData['idfinfo'];
        $this->SetAtualiza();
    }

    public function ExeDelete(array $PostData){
        $this->IdInfo = $PostData['idfinfo'];
        $this->RealizaDelete();
    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
    
    private function SetCadastro(){
        if(!$this->Codigo || !$this->Porcentagem){
            $this->Error = Mensagens::SetPreencAllCamps();
            $this->Result = false;
        }else{
            $this->RealizaCadastro();
        }
    }
    
    private function SetAtualiza(){
        if(!$this->Codigo || !$this->Porcentagem){
            $this->Error = Mensagens::SetPreencAllCamps();
            $this->Result = false;
        }else{
            $this->RealizaAtualizacao();
        }
    }
    
    private function RealizaCadastro(){
        $Insere = new Create();
        $Insere->ExeCreate("cupons", array("codigo" => $this->Codigo, "porcentagem" => $this->Porcentagem, "utilizacoes" => 0));
        if($Insere->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
    
    private function RealizaAtualizacao(){
        $Atualiza = new Update();
        $Atualiza->ExeUpdate("cupons", array("codigo" => $this->Codigo, "porcentagem" => $this->Porcentagem), "WHERE id = :id", "id={$this->IdInfo}");
        if($Atualiza->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
    
    private function RealizaDelete(){
        $Delete = new Delete();
        $Delete->ExeDelete("cupons", "WHERE id = :id", "id={$this->IdInfo}");
        if($Delete->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
}