<?php
/**
 * <strong>OrigemCompra.class</strong>
 * Classe responsável por gerenciar as origens das compras
 * @copyright (c) 2017, André Cristhian
 */
class OrigemCompra{
    
    private $Titulo;
    private $IdInfo;
    private $Error;
    private $Result;
    
    public function ExeCadastro(array $PostData){
        //Limpa e retira quaisquer meios de invasões por formulários
        $PostData = array_map("strip_tags", $PostData);
        $PostData = array_map("trim", $PostData);
        
        $this->Titulo = $PostData['titulo'];
        $this->SetCadastro();
    }

    public function ExeAtualizacao(array $PostData){
        //Limpa e retira quaisquer meios de invasões por formulários
        $PostData = array_map("strip_tags", $PostData);
        $PostData = array_map("trim", $PostData);
        
        $this->Titulo = $PostData['titulo'];
        $this->IdInfo = $PostData['idfinfo'];
        $this->SetAtualiza();
    }

    public function ExeDelete(array $PostData){
        $this->IdInfo = $PostData['idfinfo'];

        //Verifica primeiro se está relacionado á alguma compra
        $ReadVerifica = new Read();
        $ReadVerifica->ExeRead("compras", "WHERE id_origem_compra = :id_origem_compra", "id_origem_compra={$this->IdInfo}");
        if($ReadVerifica->GetResult()){
            $this->Error = Mensagens::SetRelacionado();
            $this->Result = false;
        }else{
            $this->RealizaDelete();
        }        
    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
    
    private function SetCadastro(){
        if(!$this->Titulo){
            $this->Error = Mensagens::SetPreencAllCamps();
            $this->Result = false;
        }else{
            $this->RealizaCadastro();
        }
    }
    
    private function SetAtualiza(){
        if(!$this->Titulo){
            $this->Error = Mensagens::SetPreencAllCamps();
            $this->Result = false;
        }else{
            $this->RealizaAtualizacao();
        }
    }
    
    private function RealizaCadastro(){
        $Insere = new Create();
        $Insere->ExeCreate("origem_compra", array("titulo" => $this->Titulo));
        if($Insere->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
    
    private function RealizaAtualizacao(){
        $Atualiza = new Update();
        $Atualiza->ExeUpdate("origem_compra", array("titulo" => $this->Titulo), "WHERE id = :id", "id={$this->IdInfo}");
        if($Atualiza->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
    
    private function RealizaDelete(){
        $Delete = new Delete();
        $Delete->ExeDelete("origem_compra", "WHERE id = :id", "id={$this->IdInfo}");
        if($Delete->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
}