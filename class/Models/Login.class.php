<?php
/**
 * <strong>Login.class</strong>
 * Classe responsável por efetuar o login dos administradores e revendedores
 * @copyright (c) 2017, André Cristhian
 */
class Login{
    
    private $Login;
    private $Senha;
    private $TipoLogin;
    private $Error;
    private $Result;
    
    public function ExeLogin(array $UserData){
        //Limpa e retira quaisquer meios de invasões por formulários
        $UserData = array_map("strip_tags", $UserData);
        $UserData = array_map("trim", $UserData);
        $this->Login = $UserData['emailLogin'];
        $this->Senha = $UserData['senhaLogin'];
        $this->TipoLogin = Valida::Rebase3($UserData['tipoLogin']);
        $this->SetLogin();
    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
    
    private function SetLogin(){
        if(!$this->Login || !$this->Senha || !Valida::Email($this->Login) || !$this->TipoLogin){
            $this->Error = Mensagens::SetErrorDadosInvalidos();
            $this->Result = false;
        }elseif($this->TipoLogin <> "admin" && $this->TipoLogin <> "revendedores"){
            $this->Error = Mensagens::SetErrorDadosInvalidos();
            $this->Result = false;
        }elseif(!$this->GetLoginTable()){
            $this->Error = Mensagens::SetErrorDadosInvalidos();
            $this->Result = false;
        }elseif(!Valida::CheckHash($this->Senha, $this->Result['senha'])){
            $this->Error = Mensagens::SetErrorDadosInvalidos();
            $this->Result = false;
        }else{
            $this->ExecuteLogin();
        }
    }
    
    private function GetLoginTable(){
        $Read = new Read();
        $Read->ExeRead($this->TipoLogin, "WHERE email = :email", "email={$this->Login}");
        if($Read->GetResult()){
            $this->Result = $Read->GetResult()[0];
            return true;
        }else{
            return false;
        }
    }
    
    private function ExecuteLogin(){
        $_SESSION['LoginUser'] = Valida::GeraAes($this->Result['id']);
        $_SESSION['TipoUser'] = Valida::GeraAes($this->TipoLogin);
        $this->Result = true;
    }
    
    public function CheckLogado($LogadoUser, $TipoUserFnc){
        $LogadoUser = Valida::CheckAes($LogadoUser);
        $TipoUser = Valida::CheckAes($TipoUserFnc);

        $ParseQuery = "";
        $QueryFinsh = "";
        if($TipoUser == "vendas"){
            $ParseQuery = "AND status = :status";
            $QueryFinsh = "&status=2";
        }

        $Read = new Read();
        $Read->ExeRead($TipoUser, "WHERE id = :id {$ParseQuery}", "id={$LogadoUser}{$QueryFinsh}");
        if(!$Read->GetResult()){
            unset($_SESSION['LoginUser']);
            unset($_SESSION['TipoUser']);
            echo "<script>window.location='".URL_ADMIN."/login.php';</script>";
            exit();
        }

        return array($TipoUser, $Read->GetResult()[0]['id'], $Read->GetResult()[0]['level']);
    }
}