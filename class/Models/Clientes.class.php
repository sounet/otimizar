<?php
/**
 * <strong>Clientes.class</strong>
 * Classe responsável por gerenciar clientes
 * @copyright (c) 2017, André Cristhian
 */
class Clientes{
    
    private $Nome;
    private $CpfCnpj;
    private $Celular;
    private $Email;
    private $Confemail;
    private $Cep;
    private $Endereco;
    private $Bairro;
    private $Numero;
    private $Cidade;
    private $Estado;
    private $Origem;
    private $OrigemCriacao;
    private $CodVendedor;
    private $TermosAccpt;
    private $VendedorValido;
    private $IdInfo; //ID do cliente para atualizações e deletes
    private $IdIuguCliente; //Codigo do cliente gerado automaticamente pelo IUGU
    private $Error;
    private $Result;
    
    public function ExeCadastro(array $PostData){
        $this->Nome = $PostData['nome_completo'];
        $this->CpfCnpj = $PostData['cpf_cnpj'];
        $this->Celular = $PostData['celular'];
        $this->Email = strtolower($PostData['email']);
        $this->Confemail = $PostData['Confemail'];
        $this->Cep = $PostData['cep'];
        $this->Endereco = $PostData['endereco'];
        $this->Bairro = $PostData['bairro'];
        $this->Numero = $PostData['numero'];
        $this->Cidade = $PostData['cidade'];
        $this->Estado = $PostData['estado'];
        $this->Origem = $PostData['como_conheceu'];
        $this->OrigemCriacao = $PostData['origem_criacao'];
        $this->CodVendedor = $PostData['cod_vendedor'];
        $this->TermosAccpt = $PostData['termos_accpt'];
        $this->VendedorValido = false;
        $this->RealizaCadastro();
    }

    public function ExeAtualizacao(array $PostData){
        $this->Nome = $PostData['nome_completo'];
        $this->CpfCnpj = $PostData['cpf_cnpj'];
        $this->Celular = $PostData['celular'];
        $this->Email = strtolower($PostData['email']);
        $this->Confemail = $PostData['Confemail'];
        $this->Cep = $PostData['cep'];
        $this->Endereco = $PostData['endereco'];
        $this->Bairro = $PostData['bairro'];
        $this->Numero = $PostData['numero'];
        $this->Cidade = $PostData['cidade'];
        $this->Estado = $PostData['estado'];
        $this->CodVendedor = $PostData['cod_vendedor'];
        $this->TermosAccpt = "sim";
        $this->VendedorValido = false;
        $this->IdInfo = $PostData['idfinfo'];
        $this->RealizaAtualizacao();
    }
    
    private function SetValidacao(){
        if(!$this->Nome){
            $this->Error = Mensagens::SetErrorNomeCompleto();
            $this->Result = false;
        }elseif(!$this->CpfCnpj){
            $this->Error = Mensagens::SetErrorCPF();
            $this->Result = false;
        }elseif(!Valida::validaCPF($this->CpfCnpj) && !Valida::validaCNPJ($this->CpfCnpj)){
            $this->Error = Mensagens::SetErrorCPFCNPJValido();
            $this->Result = false;
        }elseif(!$this->Celular){
            $this->Error = Mensagens::SetErrorCelular();
            $this->Result = false;
        }elseif(!$this->Email){
            $this->Error = Mensagens::SetErrorEmail();
            $this->Result = false;
        }elseif(!Valida::Email($this->Email)){
            $this->Error = Mensagens::SetErrorEmailValido();
            $this->Result = false;
        }elseif($this->Email <> $this->Confemail){
            $this->Error = Mensagens::SetErrorEmailConfirmacao();
            $this->Result = false;
        }elseif(!$this->Cep){
            $this->Error = Mensagens::SetErrorCep();
            $this->Result = false;
        }elseif(!$this->Endereco){
            $this->Error = Mensagens::SetErrorEndereco();
            $this->Result = false;
        }elseif(!$this->Bairro){
            $this->Error = Mensagens::SetErrorBairro();
            $this->Result = false;
        }elseif(!$this->Numero){
            $this->Error = Mensagens::SetErrorNumero();
            $this->Result = false;
        }elseif(!$this->Cidade){
            $this->Error = Mensagens::SetErrorCidade();
            $this->Result = false;
        }elseif(!$this->Estado){
            $this->Error = Mensagens::SetErrorEstado();
            $this->Result = false;
        }elseif(!$this->TermosAccpt || ($this->TermosAccpt <> "sim")){
            $this->Error = Mensagens::SetErrorTermos();
            $this->Result = false;
        }else{
            if (isset($this->CodVendedor) && !empty($this->CodVendedor)) {
                //Verifica código do vendedor e aplica token de envio
                $Gatilho = new Gatilho();
                $Gatilho->setEndpoint(ENDPOINT_API);
                $Gatilho->Request('post', array('type_action' => VENDEDOR, 'cod_vendedor' => $this->CodVendedor));
                $result = json_decode($Gatilho->GetResult()[0]);

                $this->VendedorValido = (!empty($result->data)) ? true : false ;
            }
            return true;
        }
    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
    
    public function GetIdIuguCliente(){
        return $this->IdIuguCliente;
    }

    public function ExeDelete(array $PostData){
        $this->IdInfo = $PostData['idfinfo'];
        $this->RealizaDelete();
    }
    
    private function RealizaCadastro(){
        if($this->SetValidacao()){

            $TOKEN_IUGU = ($this->VendedorValido) ? TOKEN_IUGU_SOUNET : TOKEN_IUGU ;

            //Interação com o Iugu
            Iugu::setApiKey($TOKEN_IUGU);

            //Verifica primeiro se já existe um cadastro com o mesmo CPF e E-MAIL, apenas pega o código do cliente, caso contrário ele cadastra o cliente
            $ReadVerifica = new Read();
            $ReadVerifica->ExeRead("clientes", "WHERE cpf_cnpj = :CpfCnpj AND email = :email", "CpfCnpj={$this->CpfCnpj}&email={$this->Email}");
            if(!$ReadVerifica->GetResult()){

                //Cadastra o cliente no IUGU
                $InfoSet = Iugu_Customer::create(array(
                    "email" => $this->Email,
                    "name" => $this->Nome,
                    "cpf_cnpj" => $this->CpfCnpj,
                    "zip_code" => $this->Cep,
                    "number" => $this->Numero,
                    "street" => $this->Endereco,
                    "city" => $this->Cidade,
                    "state" => $this->Estado,
                    "district" => $this->Bairro
                ));

                //Cadastra o cliente no banco de dados
                $array_data = array(
                    "cod_cliente" => $InfoSet->id,
                    "nome" => $this->Nome,
                    "cpf_cnpj" => $this->CpfCnpj,
                    "celular" => $this->Celular,
                    "email" => $this->Email,
                    "cep" => $this->Cep,
                    "endereco" => $this->Endereco,
                    "bairro" => $this->Bairro,
                    "numero" => $this->Numero,
                    "cidade" => $this->Cidade,
                    "estado" => $this->Estado,
                    "origem" => Valida::Rebase3($this->Origem),
                    "iugu_destino" => (($this->VendedorValido) ? 'sounet' : 'otimizaragora'),
                    "origem_criacao" => $this->OrigemCriacao,
                    "data" => date("Y-m-d H:i:s")
                );
                $Insere = new Create();
                $Insere->ExeCreate("clientes", $array_data);

                //Verifica se cadastrou o cliente no banco de dados
                if($Insere->GetResult()){

                    //Cadastra informações no banco de dados SOUNET através do Gatilho 
                    $array_data['type_action'] = CLIENTES;
                    $array_data['origem_cliente'] = ORIGEM_COMPRA;
                    $array_data['iugu'] = ($this->VendedorValido) ? 1 : 0;
                    unset($array_data['iugu_destino']);
                    $Gatilho = new Gatilho();
                    $Gatilho->Request('post', $array_data);

                    $this->Result = true;
                    $this->IdIuguCliente = $InfoSet->id;
                }else{
                    $this->Error = Mensagens::SetErrorProcess();
                    $this->Result = false;
                }

            }elseif($ReadVerifica->GetResult()[0]['cpf_cnpj'] == $this->CpfCnpj && $ReadVerifica->GetResult()[0]['email'] == $this->Email){

                $Gatilho = new Gatilho();

                $create_cliente = false;
                $cliente_iugu_sounet = 0;
                $cod_cliente_iugu_sounet = '';

                $clientes = $ReadVerifica->GetResult();
                $cliente = '';
                
                try {
                    if ($this->VendedorValido) {
                        $Gatilho->setEndpoint(ENDPOINT_API);

                        //VERIFICA SE CLIENTE EXISTE NO BANCO DE DADOS E NO IUGU SOUNET
                        $Gatilho->Request('post', array(
                            'type_action' => CLIENTES, 
                            'where' => array(
                                'cpf_cnpj'          => $this->CpfCnpj,
                                'email'             => $this->Email,
                                'iugu'              => 1,
                                'origem_cliente'    => ORIGEM_COMPRA
                            )
                        ));

                        $result = json_decode($Gatilho->GetResult()[0]);

                        if (!empty($result->data)) {
                            $iugu_cliente = Iugu_Customer::fetch($result->data->cod_cliente);
                            $cliente_iugu_sounet = 1;
                            $cod_cliente_iugu_sounet = $result->data->cod_cliente;
                        } else {
                            $create_cliente = true;
                        }

                    } else {
                        foreach ($clientes as $c) {
                            if ($c['iugu_destino'] != 'sounet') {
                                $cliente = $c;
                                break;
                            }
                        }
                        if ($cliente) {
                            $iugu_cliente = Iugu_Customer::fetch($cliente['cod_cliente']);
                        } else {
                            $create_cliente = true;
                        }
                    }

                } catch (Exception $e) {
                    $create_cliente = true;
                }

                if ($create_cliente) {
                    $InfoSet = Iugu_Customer::create(array(
                        "email"     => $this->Email,
                        "name"      => $this->Nome,
                        "cpf_cnpj"  => $this->CpfCnpj,
                        "zip_code"  => $this->Cep,
                        "number"    => $this->Numero,
                        "street"    => $this->Endereco,
                        "city"      => $this->Cidade,
                        "state"     => $this->Estado,
                        "district"  => $this->Bairro
                    ));

                    $array_data = array(
                        "cod_cliente"       => $InfoSet->id,
                        "nome"              => $this->Nome,
                        "cpf_cnpj"          => $this->CpfCnpj,
                        "celular"           => $this->Celular,
                        "email"             => $this->Email,
                        "cep"               => $this->Cep,
                        "endereco"          => $this->Endereco,
                        "bairro"            => $this->Bairro,
                        "numero"            => $this->Numero,
                        "cidade"            => $this->Cidade,
                        "estado"            => $this->Estado,
                        "origem"            => Valida::Rebase3($this->Origem),
                        "origem_criacao"    => $this->OrigemCriacao,
                        "data"              => date("Y-m-d H:i:s")
                    );

                    if ($this->VendedorValido) {      //IUGU SOUNET
                        // Replicar cliente no OtimizarAgora
                        $array_data['iugu_destino'] = 'sounet';
                        $Insere = new Create();
                        $Insere->ExeCreate("clientes", $array_data);

                        //Verifica se cadastrou o cliente no banco de dados OtimizarAgora
                        if ($Insere->GetResult()) {
                            //Cadastra o cliente no banco de dados
                            $array_data['type_action'] = CLIENTES;
                            $array_data['origem_cliente'] = ORIGEM_COMPRA;
                            $array_data['iugu'] = 1;
                            unset($array_data['iugu_destino']);
                            $Gatilho->setEndpoint(ENDPOINT_UPDATE_PAINEL);
                            $Gatilho->Request('post', $array_data);

                            $this->Result = true;
                            $this->IdIuguCliente = $InfoSet->id;
                        } else {
                            $this->Error = Mensagens::SetErrorProcess();
                            $this->Result = false;
                        }

                    } else {            //IUGU OTIMIZARAGORA
                        $array_data['iugu_destino'] = 'otimizaragora';
                        $Insere = new Create();
                        $Insere->ExeCreate("clientes", $array_data);

                        //Verifica se cadastrou o cliente no banco de dados
                        if ($Insere->GetResult()) {

                            //Cadastra informações no banco de dados Franqueados através do Gatilho
                            $array_data['type_action'] = CLIENTES;
                            $array_data['origem_cliente'] = ORIGEM_COMPRA;
                            $array_data['iugu'] = 0;
                            unset($array_data['iugu_destino']);
                            $Gatilho->setEndpoint(ENDPOINT_UPDATE_PAINEL);
                            $Gatilho->Request('post', $array_data);

                            $this->Result = true;
                            $this->IdIuguCliente = $InfoSet->id;
                        } else {
                            $this->Error = Mensagens::SetErrorProcess();
                            $this->Result = false;
                        }
                    }

                } else {
                    if ($cliente) {
                        $cod_cliente = $cliente['cod_cliente'];
                        $array_data = $cliente;
                    } else {
                        $cod_cliente = ($cod_cliente_iugu_sounet) ? $cod_cliente_iugu_sounet : $ReadVerifica->GetResult()[0]['cod_cliente'];
                        $array_data = $ReadVerifica->GetResult()[0]; 
                    }
                    $array_data['type_action'] = CLIENTES;
                    $array_data['origem_cliente'] = ORIGEM_COMPRA;
                    $array_data['origem_criacao'] = $this->OrigemCriacao;
                    $array_data['iugu'] = $cliente_iugu_sounet;
                    $array_data['cod_cliente'] = $cod_cliente;
                    unset($array_data['id']);
                    $Gatilho->setEndpoint(ENDPOINT_UPDATE_PAINEL);
                    $Gatilho->Request('post', $array_data);
                    
                    $this->Result = true;
                    $this->IdIuguCliente = $cod_cliente;
                }              
            }
        }
    }
    
    private function RealizaAtualizacao(){
        if($this->SetValidacao()){
            $Atualiza = new Update();
            $Atualiza->ExeUpdate("compras", array("codigo" => $this->Codigo, "porcentagem" => $this->Porcentagem), "WHERE id = :id", "id={$this->IdInfo}");
            if($Atualiza->GetResult()){
                $this->Result = true;
            }else{
                $this->Error = Mensagens::SetErrorProcess();
                $this->Result = false;
            }
        }
    }
    
    private function RealizaDelete(){
        $Delete = new Delete();
        $Delete->ExeDelete("compras", "WHERE id = :id", "id={$this->IdInfo}");
        if($Delete->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
}