<?php
/**
 * <strong>Compras.class</strong>
 * Classe responsável por gerenciar as compras no painel administrativo
 * @copyright (c) 2017, André Cristhian
 */
class Compras{

    private $IdInfo;
    private $CodCliente;
    private $PostData;
    private $Error;
    private $Result;
    
    public function SendObservacao(array $PostData){
        $this->PostData = $PostData;

        //Faz a consulta se o ID enviado pelo post está correto
        $ReadCompras = new Read();
        $ReadCompras->ExeRead("compras", "WHERE id = :id", "id={$this->PostData['IdCompra']}");
        if($ReadCompras->GetResult() && $this->PostData['observacao'] <> ""){

            //Verifica quem é o admin logado que fez a observação
            $LogadoAdmin = Valida::CheckAes($_SESSION['LoginUser']);
            $ReadAdminLogado = new Read();
            $ReadAdminLogado->ExeRead("admin", "WHERE id = :id", "id={$LogadoAdmin}");

            //Insere a observação
            $InsertObservacao = new Create();
            $InsertObservacao->ExeCreate("observacoes", array("cod_assinatura" => $ReadCompras->GetResult()[0]['cod_assinatura'], "observacao" => nl2br($this->PostData['observacao']), "data" => date("Y-m-d H:i:s"), "origem" => ORIGEM_COMPRA, "nome_postador" => $ReadAdminLogado->GetResult()[0]['nome']));
            if($InsertObservacao->GetResult()){
                $this->Result = true;

                //Cadastra observação no banco de dados SOUNET através do Gatilho 
                $array_data = array(
                    'type_action'       =>  OBSERVACOES,
                    'cod_assinatura'    =>  $ReadCompras->GetResult()[0]['cod_assinatura'],
                    'observacao'        =>  nl2br($this->PostData['observacao']),
                    'nome_postador'     =>  $ReadAdminLogado->GetResult()[0]['nome'],
                    'origem'            =>  ORIGEM_COMPRA,
                    'data'              =>  date('Y-m-d H:i:s')
                );
                $Gatilho = new Gatilho();
                $Gatilho->Request('post', $array_data);
            }else{
                $this->Error = Mensagens::SetErrorProcess();
                $this->Result = false;
            }
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }        
    }
    
    public function SendRevendedores(array $PostData){
        $this->PostData = $PostData;

        //Faz a consulta se o ID enviado pelo post está correto
        $ReadCompras = new Read();
        $ReadCompras->ExeRead("compras", "WHERE id = :id", "id={$this->PostData['IdCompra']}");
        if($ReadCompras->GetResult() && $this->PostData['revendedor'] <> ""){
            //Insere a observação
            $UpdateRevendedor = new Update();
            $UpdateRevendedor->ExeUpdate("compras", array("revendedor" => $this->PostData['revendedor']), "WHERE id = :id", "id={$this->PostData['IdCompra']}");
            if($UpdateRevendedor->GetResult()){
                $this->Result = true;
            }else{
                $this->Error = Mensagens::SetErrorProcess();
                $this->Result = false;
            }
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }

    public function SendAutomacao(array $PostData){
        $this->PostData = $PostData;
        $this->IdInfo = $this->PostData['IdCompra'];

        //Faz a consulta se o ID enviado pelo post está correto
        $ReadCompras = new Read();
        $ReadCompras->ExeRead("compras", "WHERE id = :id", "id={$this->IdInfo}");
        if($ReadCompras->GetResult()){

            //Verifica se já existe uma automação para essa compra, caso haja apenas atualiza, se não registra
            $ReadAutomacoes = new Read();
            $ReadAutomacoes->ExeRead("automacoes", "WHERE cod_cliente = :cod_cliente AND cod_assinatura = :cod_assinatura", "cod_cliente={$ReadCompras->GetResult()[0]['cod_cliente']}&cod_assinatura={$ReadCompras->GetResult()[0]['cod_assinatura']}");
            if($ReadAutomacoes->GetResult()){

                //Caso haja registro apenas atualiza os registros
                $UpdateAutomacao = new Update();
                $UpdateAutomacao->ExeUpdate("automacoes", array("seguidores" => $this->PostData['seguidores'], "seguindo" => $this->PostData['seguindo'], "login" => $this->PostData['login_plataforma'], "senha" => $this->PostData['senha_plataforma'], "estrategia" => $this->PostData['estrategia'], "data" => date("Y-m-d H:i:s")), "WHERE cod_cliente = :cod_cliente AND cod_assinatura = :cod_assinatura", "cod_cliente={$ReadCompras->GetResult()[0]['cod_cliente']}&cod_assinatura={$ReadCompras->GetResult()[0]['cod_assinatura']}");
                if($UpdateAutomacao->GetResult()){

                    //Verifica se é necessário enviar o e-mail ou não
                    if($this->PostData['enviar_email'] == "2"){
                        $this->CodCliente = $ReadCompras->GetResult()[0]['cod_cliente'];
                        $this->EmailAutomacao();
                    }else{
                        $this->Result = true;
                    }

                }else{
                    $this->Error = Mensagens::SetErrorProcess();
                    $this->Result = false;
                }

            }else{

                //Caso não haja, o registro é realizado
                $InsertAutomacao = new Create();
                $InsertAutomacao->ExeCreate("automacoes", array("cod_cliente" => $ReadCompras->GetResult()[0]['cod_cliente'], "cod_assinatura" => $ReadCompras->GetResult()[0]['cod_assinatura'], "seguidores" => $this->PostData['seguidores'], "seguindo" => $this->PostData['seguindo'], "login" => $this->PostData['login_plataforma'], "senha" => $this->PostData['senha_plataforma'], "estrategia" => $this->PostData['estrategia'], "data" => date("Y-m-d H:i:s")));
                if($InsertAutomacao->GetResult()){

                    //Verifica se é necessário enviar o e-mail ou não
                    if($this->PostData['enviar_email'] == "2"){
                        $this->CodCliente = $ReadCompras->GetResult()[0]['cod_cliente'];
                        $this->EmailAutomacao();
                    }else{
                        $this->Result = true;
                    }

                }else{
                    $this->Error = Mensagens::SetErrorProcess();
                    $this->Result = false;
                }


            }

        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
        
    }
    
    private function EmailAutomacao(){
        //Pega informações do cliente
        $ReadCliente = new Read();
        $ReadCliente->ExeRead("clientes", "WHERE cod_cliente = :cod_cliente", "cod_cliente={$this->CodCliente}");

        //Pega informações do sistema
        $ReadSistema = new Read();
        $ReadSistema->ExeRead("sistema");

        //Verifica se preencheu nome completo, se sim pega o primeiro nome
        $PrimeiroNome = explode(" ", $ReadCliente->GetResult()[0]['nome']);
        if($PrimeiroNome[0]){
            $PrimeiroNome = $PrimeiroNome[0];
        }else{
            $PrimeiroNome = $ReadCliente->GetResult()[0]['nome'];
        }

        //Envia solicitando informações para o cliente
        $MsgEmail = file_get_contents(__DIR__."/../../Emails/automacao_iniciada.html");
        $MsgEmail = str_replace ('%NomeComprador', $PrimeiroNome, $MsgEmail);
        $MsgEmail = str_replace ('%seguidores_inicio', $this->PostData['seguidores'], $MsgEmail);
        $MsgEmail = str_replace ('%seguindo_inicio', $this->PostData['seguindo'], $MsgEmail);
        $MsgEmail = str_replace ('%estrategia_enviado', $this->PostData['estrategia'], $MsgEmail);
        $MsgEmail = str_replace ('%login_enviado', $this->PostData['login_plataforma'], $MsgEmail);
        $MsgEmail = str_replace ('%senha_enviado', $this->PostData['senha_plataforma'], $MsgEmail);
        $SendMail = Valida::EnviarEmail("Automação iniciada", $MsgEmail, $ReadSistema->GetResult()[0]['email_resposta'], NOME_PROJETO, $this->PostData['login_plataforma'], $ReadCliente->GetResult()[0]['nome']);
        if($SendMail){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorSendMail();
            $this->Result = false;
        }
    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
}