<?php
/**
 * <strong>Duvidas.class</strong>
 * Classe responsável por gerenciar duvidas frequentes
 * @copyright (c) 2017, André Cristhian
 */
class Duvidas{
    
    private $Pergunta;
    private $Resposta;
    private $IdInfo;
    private $Error;
    private $Result;
    
    public function ExeCadastro(array $PostData){
        //Limpa e retira quaisquer meios de invasões por formulários
        $PostData = array_map("strip_tags", $PostData);
        $PostData = array_map("trim", $PostData);
        
        $this->Pergunta = $PostData['pergunta'];
        $this->Resposta = $PostData['resposta'];
        $this->SetCadastro();
    }

    public function ExeAtualizacao(array $PostData){
        //Limpa e retira quaisquer meios de invasões por formulários
        $PostData = array_map("strip_tags", $PostData);
        $PostData = array_map("trim", $PostData);
        
        $this->Pergunta = $PostData['pergunta'];
        $this->Resposta = $PostData['resposta'];
        $this->IdInfo = $PostData['idfinfo'];
        $this->SetAtualiza();
    }

    public function ExeDelete(array $PostData){
        $this->IdInfo = $PostData['idfinfo'];
        $this->RealizaDelete();
    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
    
    private function SetCadastro(){
        if(!$this->Pergunta || !$this->Resposta){
            $this->Error = Mensagens::SetPreencAllCamps();
            $this->Result = false;
        }else{
            $this->RealizaCadastro();
        }
    }
    
    private function SetAtualiza(){
        if(!$this->Pergunta || !$this->Resposta){
            $this->Error = Mensagens::SetPreencAllCamps();
            $this->Result = false;
        }else{
            $this->RealizaAtualizacao();
        }
    }
    
    private function RealizaCadastro(){
        $Insere = new Create();
        $Insere->ExeCreate("duvidas_frequentes", array("pergunta" => $this->Pergunta, "resposta" => $this->Resposta));
        if($Insere->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
    
    private function RealizaAtualizacao(){
        $Atualiza = new Update();
        $Atualiza->ExeUpdate("duvidas_frequentes", array("pergunta" => $this->Pergunta, "resposta" => $this->Resposta), "WHERE id = :id", "id={$this->IdInfo}");
        if($Atualiza->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
    
    private function RealizaDelete(){
        $Delete = new Delete();
        $Delete->ExeDelete("duvidas_frequentes", "WHERE id = :id", "id={$this->IdInfo}");
        if($Delete->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
}