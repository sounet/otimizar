<?php
/**
 * <strong>Mensagens.class</strong>
 * Classe responsável por direcionar as mensagens ao usuário
 * @copyright (c) 2017, André Cristhian
 */
class Mensagens {

    public static function SetLogSucess() {
        return "Logado com sucesso!";
    }

    public static function SetErrorDadosInvalidos() {
        return "Dados inválidos!";
    }

    public static function SetPreencAllCamps() {
        return "Preencha todos os campos!";
    }

    public static function SetPassConfError() {
        return "Senha e confirmação não coincidem!";
    }

    public static function SetUserExist() {
        return "Usuário já cadastrado em nossa base!";
    }

    public static function SetErrorProcess() {
        return "Falha ao realizar processamento!";
    }

    public static function SetCadCliente() {
        return "Informações cadastradas com sucesso, você será redirecionado...";
    }

    public static function SetCadInfoSucess() {
        return "Informações cadastradas com sucesso, você será redirecionado para a listagem!";
    }

    public static function SetCadInfoSucessRefresh() {
        return "Informações atualizadas com sucesso, a página será atualizada!";
    }

    public static function SetUpdInfoSucess() {
        return "Informações atualizadas com sucesso, você será redirecionado para a listagem!";
    }

    public static function SetRelacionado() {
        return "Não é possível excluir, existem informações relacionadas!";
    }

    public static function SetErrorNomeCompleto() {
        return "Preencha seu nome completo!";
    }

    public static function SetErrorCPF() {
        return "Preencha seu CPF!";
    }

    public static function SetErrorCPFValido() {
        return "Preencha um CPF válido!";
    }

    public static function SetErrorCPFCNPJValido() {
        return "Preencha um CPF/CNPJ válido!";
    }

    public static function SetErrorCelular() {
        return "Preencha um número de Celular!";
    }

    public static function SetErrorEmail() {
        return "Preencha seu E-mail!";
    }

    public static function SetErrorEmailValido() {
        return "Preencha um E-mail válido!";
    }

    public static function SetErrorEmailConfirmacao() {
        return "E-mail e confirmação não coincidem!";
    }

    public static function SetErrorCep() {
        return "Preencha seu CEP!";
    }

    public static function SetErrorEndereco() {
        return "Preencha seu Endereço!";
    }

    public static function SetErrorBairro() {
        return "Preencha seu Bairro!";
    }

    public static function SetErrorNumero() {
        return "Preencha seu Número!";
    }

    public static function SetErrorCidade() {
        return "Preencha sua Cidade!";
    }

    public static function SetErrorEstado() {
        return "Preencha seu Estado!";
    }

    public static function SetErrorTermos() {
        return "Concorde com os nossos Termos de serviço!";
    }

    public static function SetBoletoGerado() {
        return "Boleto gerado com sucesso, você será redirecionado!";
    }

    public static function SetPagamentoCartao() {
        return "Pedido realizado com sucesso, estamos redirecionando você...";
    }

    public static function SetSuccessCompletar() {
        return "Obrigado por completar seu cadastro. Nossa equipe entrará em contato em até 2 dias úteis!";
    }

    public static function SetErrorLoginInstagram() {
        return "Preencha o login do seu instagram!";
    }

    public static function SetErrorSenhaInstagram() {
        return "Preencha a senha do seu instagram!";
    }

    public static function SetErrorPerfis() {
        return "Preencha os perfis de referência!";
    }

    public static function SetErrorHashtags() {
        return "Preencha suas hashtags de interesse!";
    }

    public static function SetErrorLocalizacao() {
        return "Preencha as cidades / locais que você quer atingir!";
    }

    public static function SetErrorSobrePublico() {
        return "Escreva um pouco sobre seu públic-alvo!";
    }

    public static function SetErrorGenero() {
        return "Selecione os gêneros que você deseja!";
    }

    public static function SetErrorNumeroCartao() {
        return "Preencha corretamente o número de seu cartão de crédito!";
    }

    public static function SetErrorPrimeiroNome() {
        return "Preencha o primeiro nome do titular do cartão!";
    }

    public static function SetErrorSegundoNome() {
        return "Preencha o segundo nome do titular do cartão!";
    }

    public static function SetErrorMesValidade() {
        return "Preencha o mês de válidade de seu cartão de crédito!";
    }

    public static function SetErrorAnoValidade() {
        return "Preencha o ano de válidade de seu cartão de crédito!";
    }

    public static function SetErrorCVV() {
        return "Preencha o código de segurança de seu cartão de crédito!";
    }

    public static function SetErrorTransacaoNaoAutorizada() {
        return "Transação não autorizada!";
    }

    public static function SetErrorCard06() {
        return "Verifique as informações preenchidas e tente novamente!";
    }

    public static function SetErrorCard51() {
        return "Saldo insuficiente!";
    }

    public static function SetErrorCard57() {
        return "Transação não permitida ou não autorizada!";
    }

    public static function PgtoReprovado() {
        return "Pagamento não autorizado!";
    }

    public static function SetErrorSendMail() {
        return "Erro ao enviar e-mail automático!";
    }

    public static function SetInfoSend() {
        return "Informações enviadas com sucesso!";
    }


    // Mensagens erro formulário briefing SEO
    public static function SetErrorSiteTitulo() {
        return "Preencha o título do site!";
    }

    public static function SetErrorSiteURL() {
        return "Preencha a URL do site!";
    }

    public static function SetErrorAreaAtuacao() {
        return "Preencha a área de atuação do site!";
    }

    public static function SetErrorKeywords() {
        return "Preencha as palavras-chave do site!";
    }

    public static function SetErrorFtpHost() {
        return "Preencha o host do FTP!";
    }

    public static function SetErrorFtpLogin() {
        return "Preencha o login do FTP!";
    }

    public static function SetErrorFtpPassword() {
        return "Preencha o senha do FTP!";
    }

    public static function SetErrorPlataformaLogin() {
        return "Preencha o login da sua plataforma própria!";
    }

    public static function SetErrorPlataformaSenha() {
        return "Preencha a senha da sua plataforma própria!";
    }

    public static function SetErrorGoogleLogin() {
        return "Preencha o login da sua conta Google!";
    }

    public static function SetErrorGoogleSenha() {
        return "Preencha a senha da sua conta Google!";
    }

    public static function SetErrorValor() {
        return "Preencha o valor";
    }


    // Mensagens erro formulário briefing ADWORDS
    public static function SetErrorFormaVeiculacao() {
        return "Preencha a forma de veiculação!";
    }

    public static function SetErrorFraseDestaque() {
        return "Preencha uma frase de destaque!";
    }

    public static function SetErrorValorInvestido() {
        return "Preencha a conta do AdWords/Valor investido mensalmente!";
    }

    public static function SetErrorSiteAnunciado() {
        return "Preencha o site a ser anunciado!";
    }

    public static function SetErrorPublicoAlvo() {
        return "Preencha o perfil do público alvo/consumidor!";
    }

    public static function SetErrorClientePotencial() {
        return "Preencha onde deseja atingir clientes em potencial!";
    }

    public static function SetErrorDestaqueInicial() {
        return "Preencha qual produto ou serviço deseja destacar inicialmente!";
    }

    public static function SetErrorOrcamentoBase() {
        return "Preencha um orçamento base para investimento mensal em google adwords!";
    }
}
