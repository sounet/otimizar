<?php
/**
 * <strong>AssinaturaBoleto.class</strong>
 * Classe responsável por gerenciar assinaturas por boletos
 * @copyright (c) 2017, André Cristhian
 */
class AssinaturaBoleto{
    
    private $CodPlano;
    private $CodCliente;
    private $NomeCliente;
    private $EmailCliente;
    private $CupomDesconto;
    private $CodVendedor;
    private $CodRevendedor;
    private $ValorPlano;
    private $ValorAssinatura;
    private $DescricaoPlano;
    private $VendedorValido;
    private $OrigemCriacao;
    private $IdInfo; //ID da assinatura para atualizações e deletes
    private $Error;
    private $UrlBoleto;
    private $Result;
    
    public function ExeAssinatura(array $PostData){
        $this->CodPlano = Valida::Rebase3($PostData['identification_plan']);
        $this->CodCliente = Valida::Rebase3($PostData['clientecod']);
        $this->CupomDesconto = Valida::Rebase3($PostData['cupom']);
        $this->CodRevendedor = Valida::Rebase3($PostData['cod_revendedor']);
        $this->CodVendedor = Valida::Rebase3($PostData['cod_vendedor']);
        $this->VendedorValido = false;
        $this->OrigemCriacao = $PostData['origem_criacao'];

        //Busca informações para verificar se a recorrencia será ou não com desconto
        $ReadSistema = new Read();
        $ReadSistema->ExeRead("sistema");
        $this->RealizaAssinatura();
    }
    
    private function SetValidacao(){

        if (!$this->VendedorValido) {
            //Verifica se o código do plano é válido
            $ReadPlano = new Read();
            $ReadPlano->ExeRead("planos", "WHERE identificador = :identificador", "identificador={$this->CodPlano}");
            if(!$ReadPlano->GetResult()){
                echo "<script>window.location='".URL_CHECKOUT."';</script>";
                exit();
            }

            //Verifica se o código do cliente é valido
            $ReadCliente = new Read();
            $ReadCliente->ExeRead("clientes", "WHERE cod_cliente = :cod_cliente", "cod_cliente={$this->CodCliente}");
            if(!$ReadCliente->GetResult()){
                echo "<script>window.location='".URL_CHECKOUT."';</script>";
                exit();
            }

            $cod_plano = $ReadPlano->GetResult()[0]['codigo'];
            $valor_plano = $ReadPlano->GetResult()[0]['valor'];
            $valor_assinatura = $ReadPlano->GetResult()[0]['assinatura'];
            $descricao_plano = $ReadPlano->GetResult()[0]['titulo'];
            
            $cod_cliente = $ReadCliente->GetResult()[0]['cod_cliente'];
            $nome_cliente = $ReadCliente->GetResult()[0]['nome'];
            $email_cliente = $ReadCliente->GetResult()[0]['email'];
            
        } else {
            $Gatilho = new Gatilho();
            $Gatilho->setEndpoint(ENDPOINT_API);
            
            //Verifica se o código do plano é válido
            $Gatilho->Request('post', array('type_action' => PLANOS, 'identificador' => $this->CodPlano));
            $plano = json_decode($Gatilho->GetResult()[0]);
            
            if (empty($plano->data)) {
                echo "<script>window.location='".URL_CHECKOUT."';</script>";
                exit();
            }

            //Verifica se o código do cliente é valido
            $Gatilho->Request('post', array(
                'type_action' => CLIENTES, 
                'where' => array(
                    'cod_cliente'   => $this->CodCliente,
                    'iugu'          => 1
                )
            ));
            $cliente = json_decode($Gatilho->GetResult()[0]);
            
            if (empty($cliente->data)) {
                echo "<script>window.location='".URL_CHECKOUT."';</script>";
                exit();
            }

            $cod_plano = $plano->data->codigo;
            $valor_plano = $plano->data->valor;
            $valor_assinatura = $plano->data->assinatura;
            $descricao_plano = $plano->data->titulo;

            $cod_cliente = $cliente->data->cod_cliente;
            $nome_cliente = $cliente->data->nome;
            $email_cliente = $cliente->data->email;
        }

        //Verifica se o cupom de desconto é válido
        $ReadCupom = new Read();
        $ReadCupom->ExeRead("cupons", "WHERE codigo = :codigo", "codigo={$this->CupomDesconto}");
        if($ReadCupom->GetResult()){
            $this->CupomDesconto = $ReadCupom->GetResult()[0]['porcentagem'];

            //Adiciona utilizações para o cupom
            $UpdateUtilizacoes = new Update();
            $UpdateUtilizacoes->ExeUpdate("cupons", array("utilizacoes" => ($ReadCupom->GetResult()[0]['utilizacoes'] + 1)), "WHERE id = :id", "id={$ReadCupom->GetResult()[0]['id']}");
        }else{
            $this->CupomDesconto = 0;
        }

        //Verifica se o código do cliente e o código do plano bate com as informações vindas do IUGU
        $ClienteIugu = Iugu_Customer::fetch($cod_cliente);
        $PlanoIugu = Iugu_Plan::fetch($cod_plano);
        if(!$ClienteIugu->id || !$PlanoIugu->id){
            echo "<script>window.location='".URL_CHECKOUT."';</script>";
            exit();
        }

        //Verifica se o código do revendedor é válido
        $ReadRevendedor = new Read();
        $ReadRevendedor->ExeRead("revendedores", "WHERE cod_revendedor = :cod_revendedor", "cod_revendedor={$this->CodRevendedor}");
        if($ReadRevendedor->GetResult()){
            $this->CodRevendedor = $ReadRevendedor->GetResult()[0]['id'];
        }else{
            $this->CodRevendedor = 0;
        }

        //Atribui alguns outros valores
        $this->NomeCliente = $nome_cliente;
        $this->EmailCliente = $email_cliente;
        $this->ValorPlano = $valor_plano;
        $this->ValorAssinatura = $valor_assinatura;
        $this->DescricaoPlano = $descricao_plano;
        return true;
    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
    
    public function UrlBoleto(){
        return $this->UrlBoleto;
    }
    
    private function RealizaAssinatura(){

        //Verifica código do vendedor e aplica token de envio
        if (isset($this->CodVendedor) && !empty($this->CodVendedor)) {
            $Gatilho = new Gatilho();
            $Gatilho->setEndpoint(ENDPOINT_API);
            $Gatilho->Request('post', array('type_action' => VENDEDOR, 'cod_vendedor' => $this->CodVendedor));
            $result = json_decode($Gatilho->GetResult()[0]);
            $this->VendedorValido = (!empty($result->data)) ? true : false ;
        }

        $TOKEN_IUGU = ($this->VendedorValido) ? TOKEN_IUGU_SOUNET : TOKEN_IUGU ;

        //Interação com o Iugu
        Iugu::setApiKey($TOKEN_IUGU);

        if($this->SetValidacao()){

            try {
                $subitems = array(
                    array(
                        "description" => 'Desconto - Assinatura',
                        "price_cents" => number_format(($this->ValorAssinatura * -1), 2, '', '.'),
                        "quantity" => 1,
                        "recurrent" => false
                    ),
                    array(
                        "description" => "Setup - {$this->DescricaoPlano}",
                        "price_cents" => str_replace(".", "", $this->ValorPlano),
                        "quantity" => 1,
                        "recurrent" => false
                    )
                );

                //Faz a verificação se existe o cupom de desconto e aplica
                if($this->CupomDesconto > 0){
                    $ValorDesconto = ($this->ValorPlano / 100) * $this->CupomDesconto;
                    $ValorFormatado = number_format($ValorDesconto, 2, ',', '.');
                    $ValorFinal = str_replace(array(".", ",", " "), "", $ValorFormatado);
                    $subitems[] = array(
                        "description" => "Desconto - Cupom",
                        "price_cents" => -$ValorFinal,
                        "quantity" => 1,
                        "recurrent" => false
                    );
                }

                $AssinaturaCliente = Iugu_Subscription::create(Array(
                    "plan_identifier" => $this->CodPlano,
                    "customer_id" => $this->CodCliente,
                    "payable_with" => "bank_slip",
                    "expires_at" => date("Y-m-d", strtotime("+1 days")),
                    "credits_based" => false,
                    "subitems" => $subitems
                ));

                $id_fatura = "";
                $status_fatura = "";
                $valor_fatura = "";
                $url_fatura = "";
                foreach($AssinaturaCliente->recent_invoices as $recent_invoices){
                    $id_fatura = $recent_invoices->id;
                    $status_fatura = $recent_invoices->status;
                    $valor_fatura = $recent_invoices->total;
                    $url_fatura = $recent_invoices->secure_url;
                }
                
                //Cadastra a assinatura no banco de dados
                $array_data = array(
                    "cod_assinatura" => $AssinaturaCliente->id,
                    "cod_cliente" => $AssinaturaCliente->customer_id,
                    "cod_fatura" => $id_fatura,
                    "plano" => $AssinaturaCliente->plan_identifier,
                    "metodo" => "bank_slip",
                    "valor" => str_replace(array(".", ",", " ", "R$"), array("", ".", "", ""), $valor_fatura),
                    "renovacao" => 1, //1 - não 2 - sim
                    "status_pgto" => $status_fatura,
                    "revendedor" => $this->CodRevendedor,
                    "cod_vendedor" => ($this->VendedorValido) ? $this->CodVendedor : 0,
                    "origem_criacao" => $this->OrigemCriacao,
                    "dias_adicionados" => 2,
                    "data" => date("Y-m-d H:i:s")
                );
                $Insere = new Create();
                $Insere->ExeCreate("compras", $array_data);

                //Verifica se cadastrou o cliente no banco de dados
                if($Insere->GetResult()){

                    //Cadastra informações no banco de dados SOUNET através do Gatilho
                    $array_data['id_compra'] = $Insere->GetResult();
                    $array_data['type_action'] = COMPRAS;
                    $array_data['origem_compra'] = ORIGEM_COMPRA;
                    $Gatilho = new Gatilho();
                    $Gatilho->Request('post', $array_data);

                    $ReadSistema = new Read();
                    $ReadSistema->ExeRead("sistema");

                    //Verifica se preencheu nome completo, se sim pega o primeiro nome
                    $PrimeiroNome = explode(" ", $this->NomeCliente);
                    if($PrimeiroNome[0]){
                        $PrimeiroNome = $PrimeiroNome[0];
                    }else{
                        $PrimeiroNome = $this->NomeCliente;
                    }

                    //Envia e-mail de pedido realizado para o cliente
                    $MsgEmail = file_get_contents(__DIR__."/../../Emails/pedido_realizado.html");
                    $MsgEmail = str_replace ('%NomeUsuario', $PrimeiroNome, $MsgEmail);
                    Valida::EnviarEmail("Pedido realizado", $MsgEmail, $ReadSistema->GetResult()[0]['email_resposta'], NOME_PROJETO, $this->EmailCliente, $this->NomeCliente);

                    $this->Result = true;
                    $this->UrlBoleto = $url_fatura;

                }else{
                    $this->Error = Mensagens::SetErrorProcess();
                    $this->Result = false;
                }
            } catch (Exception $e) {
                $this->Error = Mensagens::SetErrorProcess();
                $this->Result = false;
            }
        }
    }
}