<?php
/**
 * <strong>CartaoFranquia.class</strong>
 * Classe responsável por gerenciar assinaturas por cartões de crédito
 */
class CartaoFranquia{
    
    private $NumeroCartao;
    private $Nome;
    private $SobreNome;
    private $MesValidade;
    private $AnoValidade;
    private $CVVCartao;
    private $IdTokenCartao;
    private $CodPlano;
    private $CodCliente;
    private $NomeCliente;
    private $EmailCliente;
    private $CupomDesconto;
    private $CodVendedor;
    private $CodRevendedor;
    private $OrigemCriacao;
    private $ValorAssinatura;
    private $ValorPlano;
    private $VendedorValido;
    private $NomePLano;
    private $Parcelas;
    private $Error;
    private $Result;
    private $IdMetodoPagamento;
    
    public function ExeAssinatura(array $PostData){
        $this->NumeroCartao = str_replace(array(" ", "-", "."), "", $PostData['numero_cartao']);
        $this->Nome = $PostData['nome_cartao'];
        $this->SobreNome = $PostData['sobrenome_cartao'];
        $this->MesValidade = $PostData['mes_validade'];
        $this->AnoValidade = $PostData['ano_validade'];
        $this->CVVCartao = $PostData['codigo_seguranca'];
        $this->Parcelas = Valida::Rebase3($PostData['parcelas']);
        $this->CodPlano = $PostData['identification_plan'];
        $this->CodCliente = $PostData['clientecod'];
        $this->CodRevendedor = 0;
        $this->CodVendedor = $PostData['cod_vendedor'];
        $this->ValorPlano = $PostData['valor'];
        $this->NomePLano = $PostData['nome_plano'];
        $this->ValorAssinatura = $PostData['valor_assinatura'];
        $this->OrigemCriacao = 'prospeccao';      
        $this->VendedorValido = false;

        $this->RealizaAssinatura();
    }
    
    private function SetValidacao(){
        //Se for cartão faz a validação e cria a forma de pagamento
        if(!$this->NumeroCartao){
            $this->Error = Mensagens::SetErrorNumeroCartao();
            $this->Result = false;
        }elseif(!$this->Nome){
            $this->Error = Mensagens::SetErrorPrimeiroNome();
            $this->Result = false;
        }elseif(!$this->SobreNome){
            $this->Error = Mensagens::SetErrorSegundoNome();
            $this->Result = false;
        }elseif(!$this->MesValidade){
            $this->Error = Mensagens::SetErrorMesValidade();
            $this->Result = false;
        }elseif(!$this->AnoValidade){
            $this->Error = Mensagens::SetErrorAnoValidade();
            $this->Result = false;
        }elseif(!$this->CVVCartao){
            $this->Error = Mensagens::SetErrorCVV();
            $this->Result = false;
        }else{

            if (!$this->VendedorValido) {
                echo "<script>window.location='".URL_CHECKOUT."';</script>";
                exit();
            } else {
                $Gatilho = new Gatilho();
                $Gatilho->setEndpoint(ENDPOINT_API);
                
                //Verifica se o código do plano é válido
                $Gatilho->Request('post', array('type_action' => PLANOS, 'identificador' => $this->CodPlano));
                $plano = json_decode($Gatilho->GetResult()[0]);
                
                if (empty($plano->data)) {
                    echo "<script>window.location='".URL_CHECKOUT."';</script>";
                    exit();
                }

                //Verifica se o código do cliente é valido
                $Gatilho->Request('post', array(
                    'type_action' => CLIENTES, 
                    'where' => array(
                        'cod_cliente'   => $this->CodCliente,
                        'iugu'          => 1
                    )
                ));
                $cliente = json_decode($Gatilho->GetResult()[0]);
                
                if (empty($cliente->data)) {
                    echo "<script>window.location='".URL_CHECKOUT."';</script>";
                    exit();
                }

                $cod_plano = $plano->data->codigo;
                $nome_cliente = $cliente->data->nome;
                $email_cliente = $cliente->data->email;
            }

            //Verifica se o código do cliente e o código do plano bate com as informações vindas do IUGU
            $ClienteIugu = Iugu_Customer::fetch($this->CodCliente);
            $PlanoIugu = Iugu_Plan::fetch($cod_plano);
            if(!$ClienteIugu->id || !$PlanoIugu->id){
                echo "<script>window.location='".URL_CHECKOUT."';</script>";
                exit();
            }

            //Atribui alguns outros valores
            $this->NomeCliente = $nome_cliente;
            $this->EmailCliente = $email_cliente;

            return true;
        }
    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
    
    private function TokenCard_FormPayment(){
        $ID_IUGU = ID_IUGU_SOUNET;

        //Cria o token do cartão de crédito
        $TokenCard = Iugu_PaymentToken::create(array(
            "account_id" => $ID_IUGU,
            "method" => "credit_card",
            "test" => true,
            "data" => array(
                "number" => $this->NumeroCartao,
                "verification_value" => $this->CVVCartao,
                "first_name" => $this->Nome,
                "last_name" => $this->SobreNome,
                "month" => $this->MesValidade,
                "year" => $this->AnoValidade
            ),
        ));

        //Verifica se Deu algum erro na criação do token
        if($TokenCard['errors'] || $TokenCard['error']){

            return false;

        }else{

            //Cria a forma de pagamento com o token criado
            $ClienteIugu = Iugu_Customer::fetch($this->CodCliente);
            $MetodoPagamento = $ClienteIugu->payment_methods()->create(Array(
                "description" => "Cartão de crédito",
                "token" => $TokenCard['id'],
                "set_as_default" => false
            ));
           
            //Verifica se Deu algum erro na criação da forma de pagamento
            if($MetodoPagamento['errors'] || $MetodoPagamento['error']){
                return false;
            }else{
                $this->IdMetodoPagamento = $MetodoPagamento['id'];
                return true;
            }

        }
    }
    
    private function RealizaAssinatura(){

        //Verifica código do vendedor e aplica token de envio
        if (isset($this->CodVendedor) && !empty($this->CodVendedor)) {
            $Gatilho = new Gatilho();
            $Gatilho->setEndpoint(ENDPOINT_API);
            $Gatilho->Request('post', array('type_action' => VENDEDOR, 'cod_vendedor' => $this->CodVendedor));
            $result = json_decode($Gatilho->GetResult()[0]);
            $this->VendedorValido = (!empty($result->data)) ? true : false ;
        }

        $TOKEN_IUGU = TOKEN_IUGU_SOUNET;

        //Interação com o Iugu
        Iugu::setApiKey($TOKEN_IUGU);

        if($this->SetValidacao() && $this->TokenCard_FormPayment()){

            try {
                $subitems = array(
                    array(
                        "description" => 'Desconto - Assinatura',
                        "price_cents" => number_format(($this->ValorAssinatura * -1), 2, '', '.'),
                        "quantity" => 1,
                        "recurrent" => false
                    ),
                    array(
                        "description" => "Setup - {$this->NomePLano}",
                        "price_cents" => str_replace(".", "", $this->ValorPlano),
                        "quantity" => 1,
                        "recurrent" => false
                    )       
                );

                $AssinaturaCliente = Iugu_Subscription::create(array(
                    "plan_identifier" => $this->CodPlano,
                    "customer_id" => $this->CodCliente,
                    "payable_with" => "credit_card",
                    "credits_based" => false,
                    "only_on_charge_success" => false,
                    "subitems" => $subitems
                ));

                $Transacao = $AssinaturaCliente;
                $Fatura = Iugu_Invoice::fetch($Transacao->recent_invoices[0]->id);

                //Verifica número de parcelas e efetua a transação
                if (isset($Fatura->id) && $Fatura->id) {
                    $array_fatura = array(
                        "customer_payment_method_id" => $this->IdMetodoPagamento,
                        "invoice_id" => $Fatura->id
                    );
                    if ($this->Parcelas > 1) {
                        $array_fatura["months"] = $this->Parcelas;
                    }

                    $ParcelamentoFatura = Iugu_Charge::create($array_fatura);
                    $Transacao = $ParcelamentoFatura;
                    $Fatura = Iugu_Invoice::fetch($Fatura->id);
                }

                if(($Transacao['errors'] || $Transacao['error'] || !$Transacao['success']) && $Transacao['LR'] <> '00'){
                    
                    if($Transacao['LR'] == "05"){
                        $this->Error = Mensagens::SetErrorTransacaoNaoAutorizada();
                    }elseif($Transacao['LR'] == "06"){
                        $this->Error = Mensagens::SetErrorCard06();
                    }elseif($Transacao['LR'] == "51"){
                        $this->Error = Mensagens::SetErrorCard51();
                    }elseif($Transacao['LR'] == "57"){
                        $this->Error = Mensagens::SetErrorCard57();
                    }else{
                        $this->Error = "<strong>#{$Transacao['LR']}</strong> - ".Mensagens::SetErrorCard06();
                    }
                    
                    $this->Result = false;

                }else{
                    //Cadastra a assinatura no banco de dados
                    $array_data = array(
                        "cod_assinatura" => $AssinaturaCliente->id,
                        "cod_cliente" => $AssinaturaCliente->customer_id,
                        "cod_fatura" => $Fatura->id,
                        "plano" => $AssinaturaCliente->plan_identifier,
                        "metodo" => "credit_card",
                        "valor" => str_replace(array(".", ",", " ", "R$"), array("", ".", "", ""), $Fatura->total),
                        "renovacao" => 1, //1 - não 2 - sim
                        "status_pgto" => $Fatura->status,
                        "status_ass" => 2, //0 - aguardando recorrência, 1 - não ativa, 2 - ativa
                        "revendedor" => $this->CodRevendedor,
                        "cod_vendedor" => ($this->VendedorValido) ? $this->CodVendedor : 0,
                        "origem_criacao" => $this->OrigemCriacao,
                        "data" => date("Y-m-d H:i:s")
                    );
                    $Insere = new Create();
                    $Insere->ExeCreate("compras", $array_data);

                    //Verifica se cadastrou o cliente no banco de dados
                    if($Insere->GetResult()){

                        $IdCompra = $Insere->GetResult();
                        $MsgEmail = file_get_contents(__DIR__."/../../Emails/pedido_realizado.html");
                        
                        //Cadastra informações no banco de dados SOUNET através do Gatilho
                        $array_data['id_compra'] = $IdCompra;
                        $array_data['type_action'] = COMPRAS;
                        $array_data['origem_compra'] = ORIGEM_COMPRA;
                        $Gatilho = new Gatilho();
                        $Gatilho->Request('post', $array_data);
                                                
                        //Atualiza etapa da otimização 
                        $Alterar = false;
                        switch ($AssinaturaCliente->plan_identifier) {
                            case 'otimizar_seo'   :
                            case 'otimizar_combo' :
                                if ($Fatura->status == 'paid') {
                                    $Alterar = true;
                                    $MsgEmail = file_get_contents(__DIR__."/../../Emails/pagamento_realizado.html");
                                    $MsgEmail = str_replace ('%idenAss', Valida::Base3($AssinaturaCliente->id), $MsgEmail);
                                }
                                break;
                            
                            default:
                                $Alterar = false;
                                break;
                        }

                        if ($Alterar) {
                            $createSeo = new Create();
                            $createSeo
                                ->ExeCreate(
                                    "etapas", 
                                    array(
                                        "cod_compra"    =>  $IdCompra,
                                        "seo"           =>  2
                                    )
                                );
                        }

                        $ReadSistema = new Read();
                        $ReadSistema->ExeRead("sistema");

                        //Verifica se preencheu nome completo, se sim pega o primeiro nome
                        $PrimeiroNome = explode(" ", $this->NomeCliente);
                        if($PrimeiroNome[0]){
                            $PrimeiroNome = $PrimeiroNome[0];
                        }else{
                            $PrimeiroNome = $this->NomeCliente;
                        }

                        //Envia e-mail de pedido realizado para o cliente
                        $MsgEmail = str_replace ('%NomeUsuario', $PrimeiroNome, $MsgEmail);
                        Valida::EnviarEmail("Pedido realizado", $MsgEmail, $ReadSistema->GetResult()[0]['email_resposta'], NOME_PROJETO, $this->EmailCliente, $this->NomeCliente);
                        
                        $this->Result = true;
                        
                    }else{
                        $this->Result = true;
                    }
                }
            } catch(Exception $e) {
                $this->Error = Mensagens::SetErrorProcess();
                $this->Result = false;
            }
        }
    }
}