<?php
/**
 * <strong>UsuariosAdmin.class</strong>
 * Classe responsável por gerenciar usuários do painel administrativo
 * @copyright (c) 2017, André Cristhian
 */
class UsuariosAdmin{
    
    private $Nome;
    private $Email;
    private $Senha;
    private $ConfSenha;
    private $Level;
    private $IdInfo;
    private $Error;
    private $Result;
    
    public function ExeCadastro(array $UserData){
        //Limpa e retira quaisquer meios de invasões por formulários
        $UserData = array_map("strip_tags", $UserData);
        $UserData = array_map("trim", $UserData);
        
        $this->Nome = $UserData['nome'];
        $this->Email = $UserData['email'];
        $this->Senha = $UserData['senha'];
        $this->ConfSenha = $UserData['confirm_senha'];
        $this->Level = $UserData['level'];
        $this->SetCadastro();
    }

    public function ExeAtualizacao(array $UserData){
        //Limpa e retira quaisquer meios de invasões por formulários
        $UserData = array_map("strip_tags", $UserData);
        $UserData = array_map("trim", $UserData);
        
        $this->Nome = $UserData['nome'];
        $this->Email = $UserData['email'];
        $this->Senha = $UserData['senha'];
        $this->ConfSenha = $UserData['confirm_senha'];
        $this->Level = $UserData['level'];
        $this->IdInfo = $UserData['idfinfo'];
        $this->SetAtualiza();
    }

    public function ExeDelete(array $UserData){
        $this->IdInfo = $UserData['idfinfo'];
        $this->RealizaDelete();
    }
    
    public function GetResult(){
        return $this->Result;
    }
    
    public function GetError(){
        return $this->Error;
    }
    
    private function SetCadastro(){
        if(!$this->Nome || !$this->Email || !Valida::Email($this->Email) || !$this->Senha || !$this->ConfSenha){
            $this->Error = Mensagens::SetPreencAllCamps();
            $this->Result = false;
        }elseif($this->Senha <> $this->ConfSenha){
            $this->Error = Mensagens::SetPassConfError();
            $this->Result = false;
        }elseif($this->GetUserExist()){
            $this->Error = Mensagens::SetUserExist();
            $this->Result = false;
        }else{
            $this->RealizaCadastro();
        }
    }
    
    private function SetAtualiza(){
        if(!$this->Nome || !$this->Email || !Valida::Email($this->Email)){
            $this->Error = Mensagens::SetPreencAllCamps();
            $this->Result = false;
        }elseif($this->Senha && (!$this->ConfSenha || $this->Senha <> $this->ConfSenha)){
            $this->Error = Mensagens::SetPassConfError();
            $this->Result = false;
        }else{
            $this->RealizaAtualizacao();
        }
    }
    
    private function GetUserExist(){
        $Read = new Read();
        $Read->ExeRead("admin", "WHERE email = :email", "email={$this->Email}");
        if($Read->GetResult()){
            return true;
        }else{
            return false;
        }
    }
    
    private function RealizaCadastro(){
        //Gera senha criptgrafada
        $senhacry = Valida::GeraHash(12, Valida::GeraCodigo(22), $this->Senha);

        $Insere = new Create();
        $Insere->ExeCreate("admin", array("nome" => $this->Nome, "email" => $this->Email, "senha" => $senhacry, "level" => $this->Level));
        if($Insere->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
    
    private function RealizaAtualizacao(){
        //Gera senha criptgrafada se foi preenchida, caso contrário a senha continua a mesma
        if($this->ConfSenha){
            $senhacry = Valida::GeraHash(12, Valida::GeraCodigo(22), $this->Senha);
        }else{
            $Read = new Read();
            $Read->ExeRead("admin", "WHERE id = :id", "id={$this->IdInfo}");
            $senhacry = $Read->GetResult()[0]['senha'];
        }

        $Atualiza = new Update();
        $Atualiza->ExeUpdate("admin", array("nome" => $this->Nome, "email" => $this->Email, "senha" => $senhacry, "level" => $this->Level), "WHERE id = :id", "id={$this->IdInfo}");
        if($Atualiza->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
    
    private function RealizaDelete(){
        $Delete = new Delete();
        $Delete->ExeDelete("admin", "WHERE id = :id", "id={$this->IdInfo}");
        if($Delete->GetResult()){
            $this->Result = true;
        }else{
            $this->Error = Mensagens::SetErrorProcess();
            $this->Result = false;
        }
    }
}