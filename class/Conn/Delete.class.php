<?php
/**
 * <strong>Create.class</strong> [ TIPO ]
 * Classe responsável por deletar registros do banco de dados
 * @copyright (c) 2016, André Cristhian
 */
class Delete extends Conexao {

    private $Tabela;
    private $Termos;
    private $Places;
    private $Result;

    /** @var PDOStatement */
    private $Delete;

    /** @var PDO */
    private $Conn;

    public function ExeDelete($Tabela, $Termos, $ParseString) {
        $this->Tabela = (string) $Tabela;
        $this->Termos = (string) $Termos;
        
        parse_str($ParseString, $this->Places);
        $this->GetSintax();
        $this->Execute();
    }

    public function GetResult() {
        return $this->Result;
    }

    private function Connect() {
        $this->Conn = parent::getConectar();
        $this->Delete = $this->Conn->prepare($this->Delete);
    }

    private function GetSintax() {
        $this->Delete = "DELETE FROM {$this->Tabela} {$this->Termos}";
    }

    private function Execute() {
        $this->Connect();
        try {
            $this->Delete->execute($this->Places);
            $this->Result = true;
        } catch (Exception $e) {
            $this->Result = null;
            echo "<p><strong>Erro ao deletar:</strong> {$e->getMessage()}. Codigo: <strong>{$e->getCode()}</strong></p>";
            exit();
        }
    }
}
?>