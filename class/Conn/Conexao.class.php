<?php
/**
 * <strong>Conexao.class</strong> [ TIPO ]
 * Classe de conexao abstrata com o banco de dados utilizando Singleton
 * @copyright (c) year, André Cristhian
 */
abstract class Conexao {

    private static $Host = HOST;
    private static $Dbsi = DBSI;
    private static $User = USER;
    private static $Pass = PASS;
    //Só executa a conexão se o Conect estiver "null"
    private static $Connect = null;

    private static function Conectar() {
        try {
            if (self::$Connect == null) {
                $dsn = 'mysql:host=' . self::$Host . ';dbname=' . self::$Dbsi;
                $options = [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'];
                self::$Connect = new PDO($dsn, self::$User, self::$Pass, $options);
            }
        } catch (PDOException $e) {
            echo "<p><strong>Erro na linha {$e->getLine()} ::</strong> {$e->getMessage()}</p><smal>{$e->getFile()}</smal>";
            exit();
        }
        self::$Connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return self::$Connect;
    }

    //Só pode ser utilizada para que erdar
    protected static function getConectar() {
        return self::Conectar();
    }

}