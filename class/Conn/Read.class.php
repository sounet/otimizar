<?php

/**
 * <strong>Create.class</strong> [ TIPO ]
 * Classe responsável por consultar registros no banco de dados
 * @copyright (c) 2016, André Cristhian
 */
class Read extends Conexao {

    private $Select;
    private $Places;
    private $Result;

    /** @var PDOStatement */
    private $Read;

    /** @var PDO */
    private $Conn;

    public function ExeRead($Tabela, $Termos = null, $ParseString = null) {
        if (!empty($ParseString)) {
            parse_str($ParseString, $this->Places);
        }
        $this->Select = "SELECT * FROM {$Tabela} {$Termos}";
        $this->Execute();
    }

    public function GetResult() {
        return $this->Result;
    }

    public function CountLines() {
        return $this->Read->rowCount();
    }

    public function FullRead($Query, $ParseString = null) {
        if (!empty($ParseString)) {
            parse_str($ParseString, $this->Places);
        }
        $this->Select = (string) $Query;
        $this->Execute();
    }

    private function Connect() {
        $this->Conn = parent::getConectar();
        $this->Read = $this->Conn->prepare($this->Select);
        $this->Read->setFetchMode(PDO::FETCH_ASSOC);
    }

    private function GetSintax() {
        if ($this->Places) {
            foreach ($this->Places as $Vinculos => $Valores) {
                $this->Read->bindValue(":{$Vinculos}", $Valores, (is_int($Valores) ? PDO::PARAM_INT : PDO::PARAM_STR));
            }
        }
    }

    private function Execute() {
        $this->Connect();
        try {
            $this->GetSintax();
            $this->Read->execute();
            $this->Result = $this->Read->fetchAll();
        } catch (Exception $e) {
            $this->Result = null;
            echo "<p><strong>Erro ao selecionar:</strong> {$e->getMessage()}. Codigo: <strong>{$e->getCode()}</strong></p>";
            exit();
        }
    }

}

?>