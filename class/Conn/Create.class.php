<?php
/**
 * <strong>Create.class</strong> [ TIPO ]
 * Classe responsável por adicionar registros ao banco de dados
 * @copyright (c) 2016, André Cristhian
 */
class Create extends Conexao{
    
    private $Tabela;
    private $Dados;
    private $Result;
    
     /** @var PDOStatement */
    private $Create;

    /** @var PDO */
    private $Conn;
    
    public function ExeCreate($Tabela, array $CamposArray){
        $this->Tabela = (string) $Tabela;
        $this->Dados = $CamposArray;
        $this->GetSintax();
        $this->Execute();
    }

    public function GetResult(){
        return $this->Result;
    }
    
    private function Connect(){
        $this->Conn = parent::getConectar();
        $this->Create = $this->Conn->prepare($this->Create);
    }
    
    private function GetSintax(){
        $campos = implode(", ", array_keys($this->Dados));
        $values = ":". implode(", :", array_keys($this->Dados));
        $this->Create = "INSERT INTO {$this->Tabela} ({$campos}) VALUES ({$values})";
    }
    
    private function Execute(){
        $this->Connect();
        try {
            $this->Create->execute($this->Dados);
            $this->Result = $this->Conn->lastInsertId();
        }catch(Exception $e) {
            $this->Result = null;
            echo "<p><strong>Erro ao cadastrar:</strong> {$e->getMessage()}. Codigo: <strong>{$e->getCode()}</strong></p>";
            exit();
        }
    }
    
}
?>