<?php
/**
 * <strong>Create.class</strong> [ TIPO ]
 * Classe responsável por atualizar registros no banco de dados
 * @copyright (c) 2016, André Cristhian
 */
class Update extends Conexao {

    private $Tabela;
    private $Dados;
    private $Termos;
    private $Places;
    private $Result;

    /** @var PDOStatement */
    private $Update;

    /** @var PDO */
    private $Conn;

    public function ExeUpdate($Tabela, array $Dados, $Termos, $ParseString) {
        $this->Tabela = $Tabela;
        $this->Dados = $Dados;
        $this->Termos = $Termos;
        
        parse_str($ParseString, $this->Places);
        $this->GetSintax();
        $this->Execute();
    }

    public function GetResult() {
        return $this->Result;
    }

    private function Connect() {
        $this->Conn = parent::getConectar();
        $this->Update = $this->Conn->prepare($this->Update);
    }

    private function GetSintax() {
        foreach($this->Dados as $keys => $values){
            $Campos[] = $keys . ' = :' . $keys;
        }
        $Campos = implode(", ", $Campos);
        $this->Update = "UPDATE {$this->Tabela} SET {$Campos} {$this->Termos}";
    }

    private function Execute() {
        $this->Connect();
        try {
            $this->Update->execute(array_merge($this->Dados, $this->Places));
            $this->Result = true;
        } catch (Exception $e) {
            $this->Result = null;
            echo "<p><strong>Erro ao atualizar:</strong> {$e->getMessage()}. Codigo: <strong>{$e->getCode()}</strong></p>";
            exit();
        }
    }
}
?>