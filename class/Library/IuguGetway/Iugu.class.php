<?php
$baseDirectory = dirname(__FILE__);

require $baseDirectory.'/Iugu/Backward_Compatibility.class.php';

require $baseDirectory.'/Iugu/Base.class.php';
require $baseDirectory.'/Iugu/SearchResult.class.php';
require $baseDirectory.'/Iugu/Object.class.php';
require $baseDirectory.'/Iugu/Utilities.class.php';

require $baseDirectory.'/Iugu/APIRequest.class.php';
require $baseDirectory.'/Iugu/APIResource.class.php';
require $baseDirectory.'/Iugu/APIChildResource.class.php';

require $baseDirectory.'/Iugu/Customer.class.php';
require $baseDirectory.'/Iugu/PaymentMethod.class.php';
require $baseDirectory.'/Iugu/PaymentToken.class.php';
require $baseDirectory.'/Iugu/Charge.class.php';
require $baseDirectory.'/Iugu/Invoice.class.php';
require $baseDirectory.'/Iugu/Subscription.class.php';
require $baseDirectory.'/Iugu/Plan.class.php';

require $baseDirectory.'/Iugu/Factory.class.php';