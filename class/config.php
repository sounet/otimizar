<?php
//Carga automática
function __autoload($NomeClass) {
    $config_dir = ['Conn', 'Library', 'Library/Aes', 'Helpers', 'Library/IuguGetway', 'Models'];
    $include_dir = null;

    foreach ($config_dir as $diretorio_name) {
        if (!$include_dir && file_exists(__DIR__.DIRECTORY_SEPARATOR."{$diretorio_name}".DIRECTORY_SEPARATOR."{$NomeClass}.class.php")) {
            require_once(__DIR__.DIRECTORY_SEPARATOR."{$diretorio_name}".DIRECTORY_SEPARATOR."{$NomeClass}.class.php");
            $include_dir = true;
        }
    }

    if (!$include_dir) {
        die("Erro ao incluir a classe {$NomeClass}.class.php");
    }
}

//Autenticação de banco de dados
define('HOST', 'localhost');
define('DBSI', 'otimizaragoracom_bd');
define('USER', 'otimizaragoracom_adm');
define('PASS', 'na5ubEspespu@');

//Autenticação de e-mail
define('MAILHOST', 'localhost');
define('MAILUSER', 'noreply@otimizaragora.com.br');
define('MAILPASS', 'P$ldzU9$]9m}');
define('MAILPORT', '587');

//Urls de destino
define('URL_BASE', 'https://www.otimizaragora.com.br');
define('URL_ADMIN', 'https://www.otimizaragora.com.br/administrar');
define('URL_CHECKOUT', 'https://www.otimizaragora.com.br/checkout');
define('AMAZON_SERVER', 'http://18.232.205.202/Request/');
define('ENDPOINT_API', 'api');
define('ENDPOINT_UPDATE_PAINEL', 'updatePainel');

//Ações Gatilho
define('COMPRAS', 'compras');
define('CLIENTES', 'clientes');
define('VENDEDOR', 'vendedor');
define('PLANOS', 'produtos');
define('OBSERVACOES', 'observacoes');

//Nome do projeto
define('ORIGEM_COMPRA', 'otimizaragora');
define('NOME_PROJETO', 'Otimizar Agora');
define('SLOGAN_PROJETO', '- Seu site na primeira página do Google');

//Configurações IUGU
define('ID_IUGU', '74B26C89FD8740B5BB63FCBA1171A851');
define('ID_IUGU_SOUNET', 'F5F04D65472843809E3EA54D0C712AD3');

define('TOKEN_IUGU', '8be15fc5922d162c4c3f63d3f07e6708');
define('TOKEN_IUGU_TESTES', '538bc020c22d9c7a94442429a4a42f62');

define('TOKEN_IUGU_SOUNET', 'e13d1b0659a1317024714212f22c575f');
define('TOKEN_IUGU_SOUNET_TESTES', '70ce25ae7aec4ae0fa6d0c6d9cb845da');

define('MAX_PARCELAS', 10);

date_default_timezone_set('America/Sao_Paulo');
define('FIRSTKEY', 'DdYT0qFx3T2/IQidqNXd2HFl5APcapucu2OPRCRTe0s=');
define('SECONDKEY', '7u6y4ukS2/vY3tJ7VG+k48ZhJusR7hYXwr3Clj79cywN5EQupmv1SpTxRbHOBl0Q/Zf3kbNOV0nX+Y3m5s0F/w==');
?>  