<?php
$TplView->addFile("INCLUDE_PG", "view/duvidas-frequentes.html");

$ReadDuvidas = new Read();
$ReadDuvidas->ExeRead("duvidas_frequentes", "ORDER BY id DESC");
if($ReadDuvidas->GetResult()){
                        
    foreach($ReadDuvidas->GetResult() as $res_list){
        $TplView->pergunta = $res_list['pergunta'];
        $TplView->resposta = $res_list['resposta'];
        $TplView->block("FOREACH");
    }

    $TplView->block("VERIFICA_LISTAGEM");
}
?>