<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require (__DIR__.DIRECTORY_SEPARATOR."../class/config.php");
$InforVal = new Valida();

$Info_post = filter_input_array(INPUT_POST, FILTER_DEFAULT);

//Verifica se todos os campos estão preenchidos e se o tpf veio preenchido
if (empty($Info_post) || (!isset($Info_post['tpf']) || empty($Info_post['tpf']))) {
    echo "<script>window.location='".URL_BASE."';</script>";
    exit();
}

//Limpa e retira quaisquer meios de invasões por formulários
$Info_post = array_map("strip_tags", $Info_post);
$Info_post = array_map("trim", $Info_post);

$JsonResults = array();

if($InforVal->CheckAes($Info_post['tpf']) == "loginadmin"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de login
    $LoginAdmin = new Login();
    $LoginAdmin->ExeLogin($Info_post);
    if($LoginAdmin->GetResult()){
        $JsonResults['success'] = Mensagens::SetLogSucess();
        $JsonResults['urldir'] = URL_BASE;
    }else{
        $JsonResults['error'] = $LoginAdmin->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "updConfig"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de configuração do sistema
    $ConfiguracoesAdmin = new ConfiguracoesAdmin();
    $ConfiguracoesAdmin->ExeAtualizacao($Info_post);

    if($ConfiguracoesAdmin->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucess();
        $JsonResults['urldir'] = URL_BASE."/configuracoes";
    }else{
        $JsonResults['error'] = $ConfiguracoesAdmin->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "cadUsers"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de usuários administradores
    $UsuariosAdmin = new UsuariosAdmin();
    $UsuariosAdmin->ExeCadastro($Info_post);

    if($UsuariosAdmin->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucess();
        $JsonResults['urldir'] = URL_BASE."/usuarios/listar";
    }else{
        $JsonResults['error'] = $UsuariosAdmin->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "updUsers"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de usuários administradores
    $UsuariosAdmin = new UsuariosAdmin();
    $UsuariosAdmin->ExeAtualizacao($Info_post);

    if($UsuariosAdmin->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetUpdInfoSucess();
        $JsonResults['urldir'] = URL_BASE."/usuarios/listar";
    }else{
        $JsonResults['error'] = $UsuariosAdmin->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "delUsers"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de usuários administradores
    $UsuariosAdmin = new UsuariosAdmin();
    $UsuariosAdmin->ExeDelete($Info_post);

    if($UsuariosAdmin->GetResult()){
        $JsonResults['success'] = true;
    }elseif($UsuariosAdmin->GetResult()){
        $JsonResults['error'] = $UsuariosAdmin->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "cadCupons"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de cupons
    $Cupons = new Cupons();
    $Cupons->ExeCadastro($Info_post);

    if($Cupons->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetCadInfoSucess();
        $JsonResults['urldir'] = URL_BASE."/cupons/listar";
    }else{
        $JsonResults['error'] = $Cupons->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "updCupons"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de cupons
    $Cupons = new Cupons();
    $Cupons->ExeAtualizacao($Info_post);

    if($Cupons->GetResult()){
        $JsonResults['success_modal'] = Mensagens::SetUpdInfoSucess();
        $JsonResults['urldir'] = URL_BASE."/cupons/listar";
    }else{
        $JsonResults['error'] = $Cupons->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "delCupons"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de cupons
    $Cupons = new Cupons();
    $Cupons->ExeDelete($Info_post);

    if($Cupons->GetResult()){
        $JsonResults['success'] = true;
    }elseif($Cupons->GetResult()){
        $JsonResults['error'] = $Cupons->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

}elseif($InforVal->CheckAes($Info_post['tpf']) == "form_agencias"){

    //Remove o direcionamento do bloco
    unset($Info_post['tpf']);
    
    //Executa a class de cupons
    $Formularios = new Formularios();
    $Formularios->Agencias($Info_post);

    if($Formularios->GetResult()){
        $JsonResults['success'] = Mensagens::SetInfoSend();
        $JsonResults['urldir'] = URL_BASE;
    }else{
        $JsonResults['error'] = $Formularios->GetError();
    }
    
    echo json_encode($JsonResults);
    exit();

} else {
    echo "<script>window.location='".URL_BASE."';</script>";
    exit();
}
?>