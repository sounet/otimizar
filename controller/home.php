<?php
$TplView->addFile("INCLUDE_PG", "view/home.html");
$SqlPlanoSeo = new Read();
$SqlPlanoAdwords = new Read();
$SqlPlanoCombo = new Read();

$SqlPlanoSeo->ExeRead("planos", "WHERE id = :id", "id=1");
$SqlPlanoAdwords->ExeRead("planos", "WHERE id = :id", "id=2");
$SqlPlanoCombo->ExeRead("planos", "WHERE id = :id", "id=3");

//Valor dos planos diretamente do IUGU
$TplView->val_plano_seo = number_format($SqlPlanoSeo->GetResult()[0]['valor'], 2, ',', '.');
$TplView->assinatura_plano_seo = number_format($SqlPlanoSeo->GetResult()[0]['assinatura'], 2, ',', '.');

$TplView->val_plano_adwords = number_format($SqlPlanoAdwords->GetResult()[0]['valor'], 2, ',', '.');
$TplView->assinatura_plano_adwords = number_format($SqlPlanoAdwords->GetResult()[0]['assinatura'], 2, ',', '.');


$TplView->val_plano_combo = number_format($SqlPlanoCombo->GetResult()[0]['valor'], 2, ',', '.');
$TplView->assinatura_plano_combo = number_format($SqlPlanoCombo->GetResult()[0]['assinatura'], 2, ',', '.');

//Identificações dos planos diretamente do IUGU
$TplView->inden_plan_seo = Valida::Base3($SqlPlanoSeo->GetResult()[0]['identificador']);
$TplView->inden_plan_adwords = Valida::Base3($SqlPlanoAdwords->GetResult()[0]['identificador']);
$TplView->inden_plan_combo = Valida::Base3($SqlPlanoCombo->GetResult()[0]['identificador']);
?>