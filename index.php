<?php
session_start();
require ("class/config.php");
require ("class/Helpers/Template.php");
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$SqlSistema = new Read;
$SqlSistema->ExeRead("sistema");

$TplView = new Template("view/index.html");

if(isset($_GET['Secao'])) {
    $Sec = $_GET['Secao'];
    $url = explode('/', $_GET['Secao']);
}else{
    $Sec = "";
    $url[0] = "";
}

//Verifica se tem um código de revendedor na URL
if($url[0] == "revendedor" && isset($url[1])){
	$RevendedorUrl = strip_tags(trim($url[1]));
	$ReadRevendedor = new Read();
	$ReadRevendedor->ExeRead("revendedores", "WHERE cod_revendedor = :cod_revendedor", "cod_revendedor={$RevendedorUrl}");
	if($ReadRevendedor->GetResult()){
		$_SESSION['cod_revendedor'] = Valida::Base3($RevendedorUrl);
		echo "<script>window.location='".URL_BASE."';</script>";
		exit();
	}
}

if(($url[0] == "") || ($url[0] == "Inicial")) {
    require("controller/home.php");
} else {
    require("controller/secao.php");
}

$TplView->NOME_PROJETO = NOME_PROJETO;
$TplView->SLOGAN_PROJETO = SLOGAN_PROJETO;
$TplView->URL_BASE = URL_BASE;

$TplView->vendas_home = $SqlSistema->GetResult()[0]['vendas'];
//$TplView->suporte_home = $SqlSistema->GetResult()[0]['suporte'];

$TplView->show();
?>