function reply_click(e){
	if("lev_planos"==e)o="#planos";
	else if("lev_servicos"==e)o="#servicos";
	else if("lev_depoimentos"==e)var o="#depoimentos";
	var s=jQuery(o).offset().top;
	jQuery("html, body").animate(
		{scrollTop:s},"slow")
}
jQuery(document).ready(
	function(e){
		e("#form_send, #two_forms_page, #there_forms_page").submit(function(){
			var o=e(this),s=e(this).attr("action");
		if(o.find("#descCK"))
			try{
				for(instance in CKEDITOR.instances)CKEDITOR.instances[instance].updateElement()
			}catch(e){

			}
		var t=e(this).serialize();
		return e.ajax(
			{url:s,data:t,type:"POST",dataType:"json",beforeSend:function(){
				o.find(".return_form").fadeIn("fast").html("<img src='https://www.otimizaragora.com.br/images/loader.gif' width='25' height='25' />")
			},success:function(e){
				e.error?o.find(".return_form").html('<div class="error_form">'+e.error+"</div>"):e.success?(o.find(".return_form").html('<div class="success_form">'+e.success+"</div>"),setTimeout(function(){
					window.location=e.urldir},2e3)):e.success_modal?(o.find(".return_form").html(swal("Sucesso!",e.success_modal,"success")),setTimeout(function(){
						window.location=e.urldir},4e3)):alert("Houve algum problema!")
				}
			}
		),!1}
		),e(".accordionButton").click(function(){
			e(".accordionButton").removeClass("on"),
			e(".accordionContent").slideUp("normal"),
			1==e(this).next().is(":hidden")&&(e(this).addClass("on"),e(this).next().slideDown("normal"))
		}
		)
		,e(".accordionContent").hide()
	}
);

jQuery(document).ready(function() {
	//Post Form com upload
    $("#form_send_upload").on('submit', function(e) {
        e.preventDefault();
        var form = $(this);
        var url_post = $(this).attr('action');
        $.ajax({
            url: url_post,
            type: 'POST',
            dataType: 'json',      
            data: new FormData(this),
            contentType: false,       
            cache: false,           
            processData:false,
            beforeSend: function () {
                form.find('.return_form').fadeIn('fast').html("<img src='https://www.otimizaragora.com.br/images/loader.gif' width='25' height='25' />");
            },
            success: function (retorno) {
                if (retorno.error) {
                    form.find('.return_form').html('<div class="error_form">' + retorno.error + '</div>');
                } else if (retorno.success_modal) {
                    form.find('.return_form').html(swal("Sucesso!", retorno.success_modal, "success"));
                    setTimeout(function(){
                        window.location=retorno.urldir;
                    }, 4000); 
                }else{
                    alert("Houve algum problema!");
                }
            }
        });
    });
});