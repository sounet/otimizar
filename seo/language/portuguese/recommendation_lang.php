<?php 
$lang = array(

'page_title_recommendation' => 'Este é o título de sua página. O conteúdo deve estar dentro das tags (<title></title>) em seu site. Os buscadores procuram pelo titulo de sites e exibem juntamente com sua URL nas buscas. O título é o elemento mais importante tanto para SEO quanto para compartilhamento social. O título deve ter entre 50 e 60 caracteres que é quantidade que a maioria dos buscadores exibe nas buscas. Um bom título deve conter a primeira e a segunda palavras-chave e o nome da empresa.</a>',

'description_recommendation' => "A descrição é a interpretação geral do conteúdo de seu site, serviços e elementos. Deve compor um pequeno parágrafo que descreva seu site e o que oferece a seus visitantes. Tenha em mente como uma propaganda de seu site. Ele não é tão importante para os buscadores quanto para os visitantes. A descrição deve ter menos de  150 caracteres, que é o máximo que a maioria dos buscadores exibe. Cada página de seu site deve conter uma única descrição. A descrição é a definição de seu site para o usuário, então procure ser curto e preciso.",

'meta_keyword_recommendation' => "Meta keywords são palavras-chave que vão dentro Meta tags. Meta keywords não são utilizadas por todos os motores de busca no rankeamento mas podem ser úteis.",

'keyword_usage_recommendation' => "Use palavras-chave que estão dentro de sua Meta Tag em seu site. Procure por palavras-chave que descrevam propriamente seu site nas pesquisas.",

'unique_stop_words_recommendation' => "
Palavras únicas são palavras incomuns que refletem os recursos e as informações do seu site. As métricas do mecanismo de pesquisa não se destinam a usar palavras únicas como fator de classificação, mas ainda são úteis para obter uma imagem adequada do conteúdo do seu site. Usar palavras únicas positivas ainda pode ser uma boa idéia de experiência do usuário.<br/><br/>",

'heading_recommendation' => "A existência da tag h1 não é tão importante para quanto o título mas continuam sendo uma ótima forma de descrever o conteúdo de seu site nos buscadores.<br/><br/>
	A tag h2 pode ser usada para o entendimento de seu site para o visitante.",

'robot_recommendation' => 'O arquivo robots.txt é um arquivo de texto que reside no diretório raiz de seu site e contém instruções para varios robôs (principalmente os de busca) para dizer a eles como indexar seu site. O robots.txt são diretamente listados para permitir ou negar o acesso e indexação dos diretórios.<br><br>
	<br/> <a href="http://www.robotstxt.org/robotstxt.html" target="_BLANK"> <i class="fa fa-hand-o-right"></i> Saiba mais</a>',

'sitemap_recommendation' => 'O Sitemap é um aquivo xml que contem uma lsita com todas as URLs do site. É utilizado para incluir diretórios de seu site nos buscadores. Sua função é facilitar a indexação e leitura profunda de seu site nas buscas,
	You can create a sitemap.xml by various free and paid service or you can write it with proper way (read about how write a sitemap). <br><br>
	<b>Lembre-se que:</b> <br/>
	1) O Sitemap deve ter menos de 10 MB (10,485,760 bytes) e no máximo 50,000 urls. Se você tiver mais de 50.000 urls precisará utilizar mais de um sitemap e usar um arquivo de indexação dosr arquivos sitemap.<br/>
	2) Insira seu arquivo sitemap no diretótio raiz de seu site e adicione a url de seu sitemap no arquivo robots.txt.<br/>
	3) O arquivo sitemap.xml pode ser comprimido usando grip para um carregamento mais rápido.<br/><br/>
	<b>Links quebrados:</b> um link quebrado é um link inacessível de seu site e tem efeito negativo  nas buscas, fazendo seu site cair no ranking de qualidade e também tem um impacto negativo na experiência do usuário. Existem diversas razões para um lik estar quebrado. Vamos listar abaixo:<br/>
	1) Você inseriu um link errado. <br/>
	2) A página de destino não existe mais (comumente representado pelo erro 404).<br/>
	3) A página de destino foi movida de forma irreversível. (Mudnaça de domínio ou site bloqueado).<br/>
	4) O usuário pdoe estar sendo bloqueado por algum firewall ou algum software/mecanismo de defesa que está bloqueando o acesso a página de destino.<br/>
	5) Você inseriu um link que está bloqueado por um firewall ou sofware de bloqueio para acesso externo.<br/>
	<a href="http://www.sitemaps.org/protocol.html" target="_BLANK"> <i class="fa fa-hand-o-right"></i> Saiba mais</a> or <a href="http://webdesign.tutsplus.com/articles/all-you-need-to-know-about-xml-sitemaps--webdesign-9838" target="_BLANK"> <i class="fa fa-hand-o-right"></i> Saiba mais</a>',

'no_do_follow_recommendation' => '<p>
	  <strong>NoIndex : </strong>A diretiva noindex é um valor de meta tag que serve para não exibit seu site a mecanismos de busca nos resultados. Não utilize esta tag em locais de seu site que quer ser encontrado nas buscas.</p>
	<p>
	  <strong>DoFollow &amp; NoFollow : </strong>nofollow é uma diretiva de valor que serve para que não os robôs dos mecanismos de busca não sigam os links de seu website. Insira &lsquo;nofollow&rsquo;se não quiser que os bots sigam seu site.</p>
	<p>
	  <a target="_BLANK" href="http://www.launchdigitalmarketing.com/seo-tips/difference-between-noindex-and-nofollow-meta-tags/"><i class="fa fa-hand-o-right"></i> Saiba mais</a></p>',

'seo_friendly_recommendation' => 'Um link amigável deve seguir algumas regras. Precisa conter um hífen como searador de conteúdo e não pode conter parâmetros e números.<br><br>
	Para aplicar utilize estas recomendações.<br>
	1) Subistitua undeline ou outroas separados por hífen, limpe a URL deletando ou subsituind números e parâmetros. <br>
	2) Una suas urls com www e sem www<br>
	3) Não utilize urls dinâmicas. Crie um arquivo de sitemap.<br>
	4) Bloqueie urls não amigáveis ou indesejadas no robots.txt.<br>
	5) Endosse suas urls canonicalizadas nas tags.<br/>
	<a target="_BLANK" href="https://www.searchenginejournal.com/five-steps-to-seo-friendly-site-url-structure/"><i class="fa fa-hand-o-right"></i> Saiba mais</a>',

'img_alt_recommendation' => 'Um título alternativo para a imagem. O atributo alt descreve sua imagem. É necessário para notificar buscadores e melhorar a usabilidade de buscas.<br>
	<a target="_BLANK" href="https://yoast.com/image-seo-alt-tag-and-title-tag-optimization/"><i class="fa fa-hand-o-right"></i>  Saiba mais</a>',

'depreciated_html_recommendation' => "Tags antigas de HTML e seus atributo podem ser substituídas por funcionalidades mais novas de HTML e CSS por exemplo. Tags antigas foram declaradas como depreciadas no HTML4  pela W3C. Os navegadores não podem garantir o funcionamento completo das funcionalidades nas versões antigas de códigos.",

'inline_css_recommendation' => "CSS inline é feito quando declarado de forma não externa e podem aumentar consideravelmente o tempo de carregamento das páginas. Tente evitar.",

'internal_css_recommendation' => "CSS inline interno são declarados detro da tag style e não são recomendados pois podem causar uma lentidão maior no carregamento do site, muitas vezes de forma desnecessária. Sempre que possível evite.",

'html_page_size_recommendation' => 'O tamanho da página HTML é o principal fator de tempo de carregamento do site. Deve ter menos de 100 KB segundo recomendações do Google. Note que este tamanho inclui css externo, js ou tamanho de imagens. Então quanto menor seu tamanho mais rápido a página carrega..<br><br>
	Para reduzir o tamanho de sua página siga estes passos:<br>
	1) Mova todo seu css e js para um arquivo externo.<br>
	2) Certifique-se de que o conteúdo do seu texto esteja no topo da página para que ele possa ser exibido antes do carregamento completo da página.<br>
	3) Reduza ou comprima todas as imagens.<br>
	<a target="_BLANK" href="https://www.searchenginejournal.com/seo-recommended-page-size/10273/"><i class="fa fa-hand-o-right"></i>  Saiba mais</a>',

'gzip_recommendation' => "GZIP é um compressor genérico que pode ser aplicado a qualquer fluxo de bytes: ele guarda alguns dos conteúdos previamente vistos e tenta encontrar e substituir fragmentos de dados duplicados de maneira eficiente. No entanto, na prática, o GZIP funciona melhor no conteúdo baseado em texto, muitas vezes conseguindo taxas de compressão de até 70-90% para arquivos maiores, ao passo que executar o GZIP em recursos que já estão compactados através de algoritmos alternativos (por exemplo, a maioria dos formatos de imagem) produz pouco para nenhuma melhoria. Também é recomendado que o tamanho comprimido do GZIP seja <= 33 KB",

'doc_type_recommendation' => 'O doc type não é um critério de SEO mas é importante pata validar seu site. Então tenha sempre um doctype em sua página.<br> <a target="_BLANK" href="http://www.pitstopmedia.com/sem/doctype-tag-seo"><i class="fa fa-hand-o-right"></i> Saiba mais</a>',

'micro_data_recommendation' => 'Micro dados são as informações subjacentes a uma seqüência ou parágrafo html.
	<br> <a target="_BLANK" href="https://schema.org/docs/gs.html"><i class="fa fa-hand-o-right"></i> Saiba mais</a>',

'ip_canonicalization_recommendation' => '
Se o nome de domínio múltiplo estiver registrado em um único endereço IP, os bots de pesquisa podem rotular outros sites como duplicatas de um site. Esta é a canonização do IP. Um pouco como canonicalizaion url. Para resolver este uso redireciona.
	<br> <a target="_BLANK" href="http://www.phriskweb.com.au/DIY-SEO/ip-canonicalization"><i class="fa fa-hand-o-right"></i> Saiba mais</a>',

'url_canonicalization_recommendation' => 'Tags canonicas fazem suas urls convergirem para o mesmo endereço web.<br>
	<code>&lt;link rel="canonical" href="https://meusite.com/home" /&gt;</code><br>
	<code>&lt;link rel="canonical" href="https://www.meusite.com/home" /&gt;</code><br>
	Ambos fazem referência a meusite.com/home. Todas as páginas com mesmo conteúdo serão redirecionadas para meusite.com/home. Isso aumenta força para buscadores eliminando duplicação de conteúdo.
	Use tags canonias para redirecionar a mesma url.<br> <a target="_BLANK" href="https://audisto.com/insights/guides/28/"><i class="fa fa-hand-o-right"></i> Saiba mais</a>',

'plain_email_recommendation' => 'O endereço de e-mail de texto simples é vulnerável aos agentes de destruição de e-mail. Um agente de destruição de e-mail rastreia seu site e coleta todos os endereços de e-mail que estão escritos em texto simples. Portanto, a existência de um endereço de e-mail de texto simples em seu site facilita spammers. Isso pode ser um mau sinal para o mecanismo de pesquisa.<br/><br/>
	<b>Para evitar que isso aconteça siga os passos:</b> <br/>
	1) Faça o CSS em pseudo classes.<br/>
	2) Escreva seu endereço de e-mail em segundo plano.<br/>
	3) Oculte seu e-mail usando javascript.<br/>
	<a target="_BLANK" href="http://www.labnol.org/internet/hide-email-address-web-pages/28364/"><i class="fa fa-hand-o-right"></i> Saiba mais</a>',

'text_to_html_ratio_recommendation' => "A proporção ideal na relação texto HTML deve estar entre 20 to 60%.
	Se tiver menos de  20% seginifica que você deve escrever mais textos em sua página. No caso de mais de 60% pode ser considerada como spam.",

);
