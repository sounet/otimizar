<?php 
$config['default_page_url'] = 'page/blank';
$config['product_name'] = 'Analisador SEO - Otimizar Agora';
$config['product_short_name'] = 'teste Otimizar Agora';
$config['product_version'] = 'v1.4 ';

$config['institute_address1'] = 'Analisador SEO - Otimizar Agora';
$config['institute_address2'] = 'www.otimizaragora.com.br';
$config['institute_email'] = 'contato@sounet.com.br';
$config['institute_mobile'] = '+5544997754129';
$config['developed_by'] = ' SouNet ';
$config['developed_by_href'] = 'http://otimizaragora.com.br';
$config['developed_by_title'] = 'otimize seu site';
$config['developed_by_prefix'] = 'Desenvolvido por' ;
$config['support_email'] = 'contato@sounet.com.br' ;
$config['support_mobile'] = '+55 44997754129' ;
$config['time_zone'] = '' ;
$config['language'] = 'portuguese';
$config['sess_use_database'] = TRUE;
$config['sess_table_name'] = 'ci_sessions';
