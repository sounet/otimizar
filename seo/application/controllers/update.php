<?php

/**
* @category controller
* class Admin
*/

class update extends CI_Controller
{
      
    public function __construct()
    {
        parent::__construct();   
        $this->load->database();
        $this->load->model('basic');
        set_time_limit(0);
    }

    public function index()
    {
      $this->v1_2to_v1_3();
    }

    public function v1_2to_v1_3()
    {

        $lines="ALTER TABLE `site_check_report` ADD `overall_score` DOUBLE NOT NULL AFTER `email`;
                ALTER TABLE `site_check_report` ADD `alexa_rank` TEXT NULL AFTER `email`;
                ALTER TABLE `site_check_report` ADD `domain_ip_info` TEXT NULL AFTER `email`";
                      
        // Loop through each line

        $lines=explode(";", $lines);
        $count=0;
        foreach ($lines as $line) 
        {
            $count++;      
            $this->db->query($line);
        }
        echo "Item has been updated to v1.3 successfully.".$count." queries executed.";
        //$this->delete_update();        
    }
  
 
    public function v1_to_v1_1()
    {

        $lines=" ALTER TABLE `site_check_report` CHANGE `title` `title` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL";
      
        // Loop through each line

        $lines=explode(";", $lines);
        $count=0;
        foreach ($lines as $line) 
        {
            $count++;      
            $this->db->query($line);
        }
        echo "Item has been updated to v1.1 successfully.".$count." queries executed.";
        //$this->delete_update();        
    }
  

    function delete_update()
    {
        unlink(APPPATH."controllers/update.php");
    }
 


}
